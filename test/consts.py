
class Consts:
    LOGS_FOLDER="./Logs/"
    PACKAGE = "pt.up.fc.dcc.hyrax.linklayerapp"
    ACTIVITY = PACKAGE + "/.MainActivity"
    REMOTE_PATH = "/storage/emulated/0/Download/"

    # main activity constants
    WHICH_ACTIVITY = "android.hyrax.which_activity"
    START_LINK_SERVICE = "android.hyrax.start_link_service"
    BLUETOOTH = "bluetooth_activity"
    BLUETOOTH_LE = "bluetooth_le_activity"
    WIFI_P2P = "wifi_p2p_activity"
    WIFI = "wifi_activity"
    MOBILE = "mobile_activity"

    # intents
    INTENT_START = "android.hyrax.exp_start"
    INTENT_STOP = "android.hyrax.exp_stop"

    # args
    ARG_EXP = "ARG_EXP"
    ARG_LOG_FILE_NAME = "ARG_LOG_FILE_NAME"

    # values
    EXP_BLUETOOTH_SERVER = "EXP_BLUETOOTH_SERVER"
    EXP_BLUETOOTH = "EXP_BLUETOOTH"
    EXP_WIFI_SERVER = "EXP_WIFI_SERVER"
    EXP_WIFI = "EXP_WIFI"
    EXP_WIFI_DIRECT_SERVER = "EXP_WIFI_DIRECT_SERVER"
    EXP_WIFI_DIRECT = "EXP_WIFI_DIRECT"
    EXP_WIFI_DIRECT_LEGACY_SERVER = "EXP_WIFI_DIRECT_LEGACY_SERVER"
    EXP_WIFI_DIRECT_LEGACY = "EXP_WIFI_DIRECT_LEGACY"

    #barrier file keys
    BARRIER_FILE = REMOTE_PATH + "file.barrier"
    BARRIER_DONE = "done"
