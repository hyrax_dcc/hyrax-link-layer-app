#!/usr/bin/python3.5
import sys
import time
import os
from hyrax_adb.adb import Adb
from hyrax_adb.log import Log
from hyrax_adb.command import Command
from consts import Consts as C


class Cmd(Command):

    def __init__(self):
        super().__init__("Adb command line options")

    # override
    def cmd_args(self, parser):
        # exec devices command
        exec_parser = parser.add_parser('exec', help='Execute exepriment')
        exec_parser.add_argument('-e', '--experiment',
                                 choices=("bluetooth", "bluetooth_le",
                                          "wifi_direct", "wifi_direct_legacy", "wifi"),
                                 help='Select the experiment to run', required=True)
        exec_parser.add_argument('-i', '--iterations', metavar="<numver_of_repetitons>",
                                 help="Tells how many times to repeat the experiment",
                                 type=int, required=True)
        exec_parser.set_defaults(cmd='exec')

    # override
    def exec_command(self, options):
        if options.cmd:
            self.run_experiment(options.experiment, options.iterations)
        else:
            raise Exception("Undefined command " + options.cmd)

    def run_experiment(self, experiment, repeations):
        """Runs a specific experiment
        Args:
        experiment -- the experiment to run
        """
        try:
            Log.debug("Selected experiment " + experiment)

            activity = None
            server_tag = None
            client_tag = None
            if experiment == "bluetooth":
                activity = C.BLUETOOTH
                server_tag = C.EXP_BLUETOOTH_SERVER
                client_tag = C.EXP_BLUETOOTH

            elif experiment == "wifi":
                activity = C.WIFI
                server_tag = C.EXP_WIFI_SERVER
                client_tag = C.EXP_WIFI

            elif experiment == "wifi_direct":
                activity = C.WIFI_P2P
                server_tag = C.EXP_WIFI_DIRECT_SERVER
                client_tag = C.EXP_WIFI_DIRECT

            elif experiment == "wifi_direct_legacy":
                activity = C.WIFI_P2P
                server_tag = C.EXP_WIFI_DIRECT_LEGACY_SERVER
                client_tag = C.EXP_WIFI_DIRECT_LEGACY

            else:
                raise Exception("Undefined experiment " + experiment)

            folder = C.LOGS_FOLDER + experiment
            # creates logs folder of not exists
            os.makedirs(folder, 0o777, True)

            devices = Adb.get_devices(2)
            if len(devices) < 2:
                raise Exception(
                    "The experiment requires at least two connected devices")

            i = 1
            while i <= repeations:
                Log.info("\nExperiment number " + str(i))

                server_log = "%s_server_%d.log" % (experiment, i)
                client_log = "%s_client_%d.log" % (experiment, i)

                Log.debug("Starting activity ...")
                Adb.start_app(devices, C.ACTIVITY, [
                              (C.WHICH_ACTIVITY, activity)])
                time.sleep(2)

                Log.debug("Starting experiment ...")
                # send intent to server
                self.send_intent_start(devices[0], server_tag, server_log)
                # send intent to client
                self.send_intent_start(devices[1], client_tag, client_log)

                Log.debug("Waiting for conclusion ...")
                Adb.perform_barrier(devices, C.BARRIER_DONE,
                                    "*", C.BARRIER_FILE)

                if self.is_done_prop_valid(devices):
                    Log.debug("Retrieving log files ...")
                    Adb.pull_file([devices[0]], C.REMOTE_PATH + server_log, folder)
                    Adb.pull_file([devices[1]], C.REMOTE_PATH + client_log, folder)
                    i += 1
                else:
                    Log.warning("Restarting experiment number: " + str(i))

                Log.debug("Stopping experiment ...")
                Adb.send_intent(devices, C.INTENT_STOP)
                time.sleep(5)
                Adb.stop_app(devices, C.PACKAGE)
                time.sleep(2)

        except Exception as e:
            Log.error(str(e))

    def is_done_prop_valid(self, devices):
        """Verifies if the experiment execution ended with success.
        Args:
        devices -- a list of devices
        """
        for d in devices:
            v = Adb.get_property(d, C.BARRIER_DONE, C.BARRIER_FILE, 'false')
            if v == 'false':
                return False
        return True

    def send_intent_start(self, device, exp, log_file):
        """Sends a start intent to a device
        Args:
        device   -- device to send the intent start
        exp      -- the type of experiment
        log_file -- file log name
        """
        Adb.send_intent([device],  C.INTENT_START, [
            (C.ARG_EXP, exp),
            (C.ARG_LOG_FILE_NAME, log_file)
        ])


if __name__ == "__main__":
    # execute only if run as a script
    cmd = Cmd()
    cmd.execute(sys.argv[1:])
