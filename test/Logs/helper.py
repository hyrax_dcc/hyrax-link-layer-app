
class Helper:

    @staticmethod
    def get_cpu_idle_time(line):
        idle = int(line[6])
        iowait = int(line[7])
        return idle + iowait

    @staticmethod
    def get_cpu_non_idle_time(line):
        user = int(line[3])
        nice = int(line[4])
        system = int(line[5])
        irq = int(line[8])
        softirq = int(line[9])
        steal = int(line[10])
        return user + nice + system + irq + softirq + steal

    @staticmethod
    def get_cpu_total_time(line):
        guest = int(line[11])
        guest_nice = int(line[12])
        return Helper.get_cpu_idle_time() + Helper.get_cpu_non_idle_time() + guest + guest_nice

    @staticmethod
    def get_process_utime(line):
        utime = int(line[15])
        return utime
