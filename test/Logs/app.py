import os
import re
import sys
from math import *
from flask import Flask
from flask import render_template

from helper import Helper
from statistics import *

app = Flask(__name__)


dirs = ['all', 'idle', 'idle_bluetooth', 'idle_wifi', 'bluetooth', 'bluetooth_le',
        'wifi', 'wifi_direct', 'wifi_direct_legacy']
EXP_NUMBER = 16
CONF_95 = 1.96

options = ['time', 'memory', 'cpu', 'battery']
techs = ['bluetooth', 'bluetooth_le', 'wifi',
         'wifi_direct', 'wifi_direct_legacy']
tmap = ['Bluetooth', 'Bluetooth LE', 'WiFi',
        'WiFi Direct', 'Wifi Direct Legacy']


@app.route("/")
def index():
    return render_template('index.html', navs=dirs)


@app.route("/all")
def all():
    return render_template('aggr.html', navs=dirs, nav_active='all', options=options)


@app.route("/all/<opt>")
def all_opt(opt):
    # techs = ['bluetooth', 'bluetooth_le', 'wifi',
    #         'wifi_direct', 'wifi_direct_legacy']

    stats = None
    if opt == 'time':
        stats = get_aggr_action_stats(techs)
    elif opt == 'memory':
        stats = get_memory_aggr_stats(techs)
    elif opt == 'cpu':
        stats = get_cpu_aggr_stats(techs)
    elif opt == 'battery':
        stats = get_battery_aggr_stats(techs)

    return render_template('aggr.html', navs=dirs, nav_active='all',
                           options=options, opt_active=opt,
                           stats=stats
                           )


@app.route("/<tech>")
def exp(tech):
    return render_template('tech.html', navs=dirs, nav_active=tech, exp_n=EXP_NUMBER)


@app.route("/<tech>/all")
def exp_all(tech):
    (server_files, client_files) = listFiles(tech)

    server = get_stats(tech, server_files)
    client = get_stats(tech, client_files)
    return render_template('tech.html',
                           navs=dirs,
                           nav_active=tech,
                           exp_n=EXP_NUMBER,
                           exp_active=int(0),
                           all={'server': server, 'client': client},
                           )


@app.route("/<tech>/<n>")
def exp_n(tech, n):
    # server lines
    fs = "%s_server_%s.log" % (tech, n)
    s_lines = split_file_lines("%s/%s" % (tech, fs))
    # client lines
    fc = "%s_client_%s.log" % (tech, n)
    c_lines = split_file_lines("%s/%s" % (tech, fc))

    # action readings
    s_act = get_action_readings(s_lines, "Actions Server")
    c_act = get_action_readings(c_lines, "Action Client")

    # battery readings
    s_batt = get_battery_readings(s_lines, "Battery Server")
    c_batt = get_battery_readings(c_lines, "Battery Client")

    # battery charge readings
    s_batt_charge = get_battery_charge_readings(
        s_lines, "Battery Charge Server")
    c_batt_charge = get_battery_charge_readings(
        c_lines, "Battery Charge  Client")

    # memory readings
    s_mem = get_memory_readings(s_lines, "Memory Server")
    c_mem = get_memory_readings(c_lines, "Memory Client")

    # cpu readings
    s_cpu = get_cpu_readings(s_lines, "Cpu Server")
    c_cpu = get_cpu_readings(c_lines, "Cpu Client")

    return render_template('tech.html',
                           navs=dirs,
                           nav_active=tech,
                           exp_n=EXP_NUMBER,
                           exp_active=int(n),
                           act={'server': s_act, 'client': c_act},
                           batt={'server': s_batt, 'client': c_batt},
                           batt_charge={'server': s_batt_charge,
                                        'client': c_batt_charge},
                           mem={'server': s_mem, 'client': c_mem},
                           cpu={'server': s_cpu, 'client': c_cpu},
                           )


def get_stats(tech, files):
    (act_key_order, act_key_values) = get_action_stats(tech, files)
    (time_key_order, time_key_values) = get_time_stats(tech, files)
    (batt_key_order, batt_key_values, _) = get_battery_stats(tech, files)
    (mem_key_order, mem_key_values) = get_memory_stats(tech, files)
    (cpu_key_order, cpu_key_values, _) = get_cpu_stats(tech, files)
    (comb_key_order, comb_key_values) = get_action_comb_stats(tech, files)

    act_key_values['keys'] = act_key_order
    time_key_values['keys'] = time_key_order
    batt_key_values['keys'] = batt_key_order
    mem_key_values['keys'] = mem_key_order
    cpu_key_values['keys'] = cpu_key_order
    comb_key_values['keys'] = comb_key_order

    return {
        'actions': act_key_values,
        'time': time_key_values,
        'battery': batt_key_values,
        'memory': mem_key_values,
        'cpu': cpu_key_values,
        'comb': comb_key_values,
    }


def get_action_stats(tech, files):
    key_order = ['HARDWARE_CALL_END', 'HARDWARE_TRIGGER', 'HARDWARE', 'DISCOVERY_CALL_END', 'DISCOVERING_TRIGGER', 'DISCOVERING',
                 'DISCOVERY_DONE_TRIGGER', 'DISCOVERY_DONE', 'VISIBILITY_CALL_END', 'VISIBLE_TRIGGER', 'VISIBLE', 'VISIBILITY_DONE_TRIGGER', 'VISIBILITY_DONE',
                 'CONNECTION_CALL_END', 'CONNECTED_TRIGGER', 'CONNECTED', 'ACCEPT_CALL_END', 'ACCEPTING_TRIGGER', 'ACCEPTING',
                 'PING_RESP', 'TRANSFER',
                 ]
    keys = dict(map(lambda k: (k, []), key_order))
    sm = 0
    for f in files:
        lines = split_file_lines("%s/%s" % (tech, f))
        series = get_action_readings(lines, "Actions")['series']
        for s in series:
            data = s['data']
            for d in data:
                if d['y'] is None:
                    continue
                lst = keys.get(d['action'], None)
                if lst is not None:
                    if d['action'] == 'PING_RESP' and d['y'] == 2:
                        keys.get('TRANSFER').append(d['diff'])
                    else:
                        lst.append(d['diff'])

                if d['action'] == 'HARDWARE' and d['diff_trg'] != None:
                    keys['HARDWARE_TRIGGER'].append(d['diff_trg'])
                    #print("HW_TRG " + str(d['diff_trg']))
                elif d['action'] == 'DISCOVERING' and d['diff_trg'] != None:
                    keys['DISCOVERING_TRIGGER'].append(d['diff_trg'])
                    #print("DIS_TRG " + str(d['diff_trg']))
                    #sm += d['diff_trg']
                elif d['action'] == 'DISCOVERY_DONE' and d['diff_trg'] != None:
                    keys['DISCOVERY_DONE_TRIGGER'].append(d['diff_trg'])
                    #print("DIS_DONE_TRG " + str(d['diff_trg']))
                elif d['action'] == 'VISIBLE' and d['diff_trg'] != None:
                    keys['VISIBLE_TRIGGER'].append(d['diff_trg'])
                    #print("VIS_TRG " + str(d['diff_trg']))
                elif d['action'] == 'VISIBILITY_DONE' and d['diff_trg'] != None:
                    keys['VISIBILITY_DONE_TRIGGER'].append(d['diff_trg'])
                    # print("VIS_DONE_TRG " + str(d['diff_trg']))
                elif d['action'] == 'CONNECTED' and d['diff_trg'] != None:
                    keys['CONNECTED_TRIGGER'].append(d['diff_trg'])
                    #print("CONN_TRG " + str(d['diff_trg']))
                elif d['action'] == 'ACCEPTING' and d['diff_trg'] != None:
                    keys['ACCEPTING_TRIGGER'].append(d['diff_trg'])
                    #print("ACCEPT_TRG " + str(d['diff_trg']))
    print("SUM " + str(sm))
    key_values = {}
    for k, lst in keys.items():
        #key_values[k] = calc_stats(lst)
        st = calc_stats(lst)
        irq = st['q3'] - st['q1'];
        ll = st['q1'] - (1.5 * irq)
        ul = st['q3'] + (1.5 * irq)
        nlst = list(filter(lambda x : (x >= ll and x <= ul), lst))
        key_values[k] = calc_stats(nlst)
    return (key_order, key_values)


def get_action_comb_stats(tech, files):
    def transform_cpu(raw_values):
        lst_total = []
        lst_process = []
        for i in range(1, len(raw_values)):
            (prev_total, prev_idled, prev_utime) = raw_values[i - 1]
            (total, idle, utime) = raw_values[i]
            total_diff = total - prev_total
            idle_diff = idle - prev_idled
            utime_diff = utime - prev_utime
            lst_total.append(((total_diff - idle_diff) /
                              total_diff) if total_diff > 0 else 0)
            lst_process.append((utime_diff / total_diff)
                               if total_diff > 0 else 0)

        return (lst_total, lst_process)

    def transform_mem(raw_values):
        rss = list(map(lambda x: x[0], raw_values))
        shared = list(map(lambda x: x[1], raw_values))
        return (rss, shared)

    def transform_batt(raw_values):
        batt = []
        e = 0
        if len(raw_values) > 0:
            prev_ts = raw_values[0][0] - 200
            for (ts, mA, v) in raw_values:
                # microamperes to amperes - I
                ampere = (-1 * mA) / 1000000
                volt = v / 1000  # millivolts to volts - V
                power = ampere * volt  # P = V * I
                print("P -> " + str(power))
                # milliseconds to seconds
                t_delta = (ts - prev_ts) / 1000
                print(t_delta)
                print(power * t_delta)
                e += (power * t_delta)  # E = P * T
                print(e)
                prev_ts = ts
        batt.append(e)
        return batt

    key_order = ['HARDWARE', 'DISCOVERY_DONE',
                 'VISIBILITY_DONE', 'CONNECTED', 'ACCEPTING', 'COMM']
    cols = ['time', 'cpu_total', 'cpu_process',
            'mem_rss', 'mem_shared']
    keys = dict(
        map(lambda k: (k, dict(map(lambda c: (c, []), cols))), key_order))

    for f in files:
        lines = split_file_lines("%s/%s" % (tech, f))
        res_map = get_action_readings(lines, "Actions")['res_map']

        for k, v in res_map.items():
            if k in keys:
                keys[k]['time'].extend(v['time'])
                (lst_total, lst_process) = transform_cpu(v['cpu'])
                keys[k]['cpu_total'].extend(lst_total)
                keys[k]['cpu_process'].extend(lst_process)

                (rss, shared) = transform_mem(v['mem'])
                keys[k]['mem_rss'].extend(rss)
                keys[k]['mem_shared'].extend(shared)

    key_values = dict(
        map(lambda k: (k, dict(map(lambda c: (c, -1), cols))), key_order))
    for k, v in keys.items():
        time_stats = calc_stats(v['time'])
        cpu_total_stats = calc_stats(v['cpu_total'])
        cpu_process_stats = calc_stats(v['cpu_process'])
        mem_rss_stats = calc_stats(v['mem_rss'])
        mem_shared_stats = calc_stats(v['mem_shared'])

        key_values[k] = {
            'time': round(time_stats['mean'], 3),
            'cpu_total': round(cpu_total_stats['mean'], 3),
            'cpu_process': round(cpu_process_stats['mean'], 3),
            'mem_rss': round(mem_rss_stats['mean'] / 1000000, 3),
            'mem_shared': round(mem_shared_stats['mean'] / 1000000, 3)
        }

    return (key_order, key_values)


def get_time_stats(tech, files):
    key_order = ['Time']
    keys = dict(map(lambda k: (k, []), key_order))

    for f in files:
        lines = split_file_lines("%s/%s" % (tech, f))
        lst = keys.get('Time')
        time_int = int(lines[-1][0]) - int(lines[0][0])
        lst.append(time_int)

    key_values = {}
    for k, lst in keys.items():
        key_values[k] = calc_stats(lst)
    return (key_order, key_values)




# extra = {
#     'form_init_ts' : lines[0][0],
#     'form_end_ts'  : None,
#     'ping_init_ts' : None,
#     'ping_end_ts'  : None,
#     'transfer_init_ts' : None,
#     'transfer_end_ts'  : lines[-1][0],
# }


def get_battery_stats(tech, files):
    key_order = ['Battery All', 'Battery During Comm',
                 'Battery During Transfer']
    keys = dict(map(lambda k: (k, []), key_order))
    per_sec = {
        'during_all' : [],
        'during_form' : [],
        'during_transfer' : []}
    for f in files:
        lines = split_file_lines("%s/%s" % (tech, f))
        readings = get_battery_readings(lines, "Battery")
        extra = readings['extra']
        series = readings['series']
        for s in series:
            data = s['data']
            energy_all = 0
            energy_transfer = 0
            prev_ts = data[0]['x'] - 200
            for d in data:
                # microamperes to amperes - I
                ampere = -1 * d['y'] / 1000000
                volt = d['v'] / 1000  # millivolts to volts - V
                power = ampere * volt  # P = V * I
                # milliseconds to seconds
                t_delta = (d['x'] - prev_ts) / 1000
                e = (power * t_delta)
                energy_all += e  # E = P * T
                if not d['before_transfer']:
                    energy_transfer += e
                prev_ts = d['x']

            keys.get('Battery All').append(energy_all)
            keys.get('Battery During Comm').append(0)
            keys.get('Battery During Transfer').append(energy_transfer)
            if extra['form_end_ts'] is None:
                ts = (extra['transfer_end_ts'] - extra['form_init_ts']) / 1000 # to seconds
                per_sec.get('during_all').append(energy_all / ts)
            else:
                ts = (extra['transfer_end_ts'] - extra['form_init_ts']) / 1000 # to seconds
                per_sec.get('during_all').append(energy_all / ts)
                ts = (extra['form_end_ts'] - extra['form_init_ts']) / 1000
                per_sec.get('during_form').append( (energy_all - energy_transfer) / ts)
                ts = (extra['transfer_end_ts'] - extra['transfer_init_ts']) / 1000
                per_sec.get('during_transfer').append( energy_transfer / ts)

    key_values = {}
    for k, lst in keys.items():
        key_values[k] = calc_stats(lst)
    per_sec_values = {}
    for k, lst in per_sec.items():
        per_sec_values[k] = calc_stats(lst)
    return (key_order, key_values, per_sec_values)


def get_memory_stats(tech, files):
    key_order = ['Rss', 'Shared']
    keys = dict(map(lambda k: (k, []), key_order))
    for f in files:
        lines = split_file_lines("%s/%s" % (tech, f))
        series = get_memory_readings(lines, "Memory")['series']
        for s in series:
            data = s['data']
            lst = keys.get(s['name'], None)
            if lst != None:
                for d in data:
                    lst.append(d['y'])
    key_values = {}
    for k, lst in keys.items():
        key_values[k] = calc_stats(lst)

    return (key_order, key_values)


def get_cpu_stats(tech, files):
    key_order = ['Total', 'Process']
    keys = dict(map(lambda k: (k, []), key_order))
    phases_keys = {
        'total_form': [],
        'process_form': [],
        'total_ping': [],
        'process_ping': [],
    }

    def get_list(phases_keys, d, name):
        if name == 'Total':
            if d['during_ping']: return phases_keys['total_ping']
            return phases_keys['total_form']
        elif name == 'Process':
            if d['during_ping']: return phases_keys['process_ping']
            return phases_keys['process_form']
        else:
            raise Exception("What!!!")

    for f in files:
        lines = split_file_lines("%s/%s" % (tech, f))
        series = get_cpu_readings(lines, "Cpu")['series']
        for s in series:
            data = s['data']
            lst = keys.get(s['name'], None)
            if lst != None:
                for d in data:
                    lst.append(d['y'])
                    other = get_list(phases_keys, d, s['name'])
                    other.append(d['y'])

    key_values = {}
    for k, lst in keys.items():
        key_values[k] = calc_stats(lst)
    phases_values = {}
    for k, lst in phases_keys.items():
        phases_values[k] = calc_stats(lst)

    #print(phases_values)
    return (key_order, key_values, phases_values)


def get_battery_charge_readings(lines, title):
    x_min = int(lines[0][0])
    x_max = int(lines[len(lines) - 1][0])
    y_min = sys.maxsize
    y_max = -sys.maxsize - 1
    data = []
    for l in lines:
        if l[1] != "BATTERY":
            continue
        ts = int(l[0])  # timestamp
        mAh = int(l[3])  # charge in microamperes-hour
        data.append({'x': ts, 'y': mAh})
        y_min = min(y_min, mAh)
        y_max = max(y_max, mAh)

    series = [{'name': "Battery Charge", 'data': data, }]
    return {
        'title': title,
        'x': {'title': "Ts", 'min': x_min, 'max': x_max},
        'y': {'title': "microAmp-Hour", 'min': y_min, 'max': y_max},
        'series': series,
    }


def get_battery_readings(lines, title):
    x_min = int(lines[0][0])
    x_max = int(lines[len(lines) - 1][0])
    y_min = sys.maxsize
    y_max = -sys.maxsize - 1
    data = []

    extra = {
        'form_init_ts' : int(lines[0][0]),
        'form_end_ts'  : None,
        'transfer_init_ts' : None,
        'transfer_end_ts'  : int(lines[-1][0]),
    }

    before_transfer = True
    for l in lines:
        #print(l)
        if before_transfer and l[1] == 'PING_REQ':
            extra['form_end_ts'] = int(l[0])
            extra['transfer_init_ts'] = int(l[0])
            before_transfer = False
            continue
        if l[1] != "BATTERY":
            continue
        ts = int(l[0])  # timestamp
        mA = int(l[2])  # current in microamperes
        mVolt = int(l[5])  # milli volts
        data.append({'x': ts, 'y': mA, 'v': mVolt, 'before_transfer': before_transfer})
        y_min = min(y_min, mA)
        y_max = max(y_max, mA)

    series = [{'name': "Battery", 'data': data}]
    return {
        'title': title,
        'x': {'title': "Ts", 'min': x_min, 'max': x_max},
        'y': {'title': "microAmp", 'min': y_min, 'max': y_max},
        'extra' : extra,
        'series': series,
    }

def get_cpu_readings(lines, title):
    """
    see: http://wiki.linuxwall.info/doku.php/en:ressources:astuces:cpu_process_usage
    """
    x_min = int(lines[0][0])
    x_max = int(lines[len(lines) - 1][0])
    y_min = 0
    y_max = 0

    data_total, data_process = [], []

    prev_total, prev_idled, prev_utime, utime = None, None, None, None
    during_ping = False
    during_transfer = False;

    for l in lines:
        ts = int(l[0])     # timestamp
        if not during_ping and l[1] == 'PING_REQ':
            during_ping = True
            continue
        if not during_transfer and l[1] == 'PING_REQ' and int(l[2]) > 0:
            during_transfer = True
            continue
        if l[1] == "CPU_PID_STAT":  # activity process log
            utime = Helper.get_process_utime(l)

        elif l[1] == "CPU_STAT":  # whole cpu log
            idle = Helper.get_cpu_idle_time(l)
            non_idle = Helper.get_cpu_non_idle_time(l)
            total = idle + non_idle
            if prev_total is not None:
                total_diff = total - prev_total
                idle_diff = idle - prev_idled
                utime_diff = utime - prev_utime
                data_total.append(
                    {'x': ts, 'y': ((total_diff - idle_diff) / total_diff), 'during_ping': during_ping, 'during_transfer': during_transfer})
                data_process.append(
                    {'x': ts, 'y': (utime_diff / total_diff), 'during_ping': during_ping, 'during_transfer': during_transfer})

            prev_total = total
            prev_idled = idle
            prev_utime = utime

    series = [{'name': "Total", 'data': data_total, },
              {'name': "Process", 'data': data_process, }
              ]
    return {
        'title': title,
        'x': {'title': "Ts", 'min': x_min, 'max': x_max},
        'y': {'title': "Percentage", 'min': y_min, 'max': y_max},
        'series': series,
    }

def get_memory_readings(lines, title):
    x_min = int(lines[0][0])
    x_max = int(lines[len(lines) - 1][0])
    y_min = sys.maxsize
    y_max = -sys.maxsize - 1

    data_total, data_rss, data_shared = [], [], []
    page_size = 4096  # 4KB

    for l in lines:
        if l[1] != "MEMORY":
            continue
        ts = int(l[0])      # timestamp
        total = int(l[2]) * page_size  # total memory
        rss = int(l[3]) * page_size   # Resident set size
        shared = int(l[4]) * page_size  # shared memory

        data_total.append({'x': ts, 'y': total})
        data_rss.append({'x': ts, 'y': rss})
        data_shared.append({'x': ts, 'y': shared})
        y_min = min(y_min, total)
        y_max = max(y_max, total)

    #{'name': "Total", 'data': data_total, }
    series = [{'name': "Rss", 'data': data_rss, },
              {'name': "Shared", 'data': data_shared, },
              ]
    return {
        'title': title,
        'x': {'title': "Ts", 'min': x_min, 'max': x_max},
        'y': {'title': "KB", 'min': y_min, 'max': y_max},
        'series': series,
    }


def get_action_readings(lines, title):
    x_min = int(lines[0][0])
    x_max = int(lines[len(lines) - 1][0])
    y_min, y_max = 0, 0

    def append(lst, trg_ts, last_ts, ts, y, ac, ins_none=[]):
        diff_trg = None if trg_ts is None else (ts - trg_ts)
        lst.append({'x': ts, 'y': y, 'action': ac, 'diff': (ts - last_ts), 'diff_trg': diff_trg})
        if ac in ins_none:
            lst.append({'x': ts, 'y': None, 'action': ac,
                        'diff': (ts - last_ts)})

    res_map = {}
    data = {'cpu': [], 'mem': [], 'batt': []}
    page_size = 4096  # 4KB

    def add_to_map(res_map, data, ac, last_ts, ts):
        close = ['HARDWARE', 'DISCOVERY_DONE',
                 'VISIBILITY_DONE', 'CONNECTED', 'ACCEPTING']
        if(ac.endswith('_CALL_BEGIN') or ac == "PING_REQ"):
            data['cpu'], data['mem'], data['batt'] = [], [], []
            if ac == "PING_REQ" and "COMM" not in res_map:
                res_map["COMM"] = {'cpu': [], 'mem': [],
                                   'batt': [], 'time': [], 'first_ts': last_ts}

        elif ac in close:
            if ac not in res_map:
                res_map[ac] = {'cpu': [], 'mem': [], 'batt': [], 'time': []}

            for k, v in data.items():
                res_map[ac][k].extend(data[k])
            res_map[ac]['time'].append(ts - last_ts)

        elif ac == "PING_RESP":
            for k, v in data.items():
                res_map["COMM"][k].extend(data[k])
            res_map["COMM"]['time'] = [ts - res_map["COMM"]['first_ts']]

    hw, dis, vis, conn, acc, ping, transf, evt = [], [], [], [], [], [], [], []
    hw_ts, dis_ts, vis_ts, conn_ts, acc_ts, ping_ts = 0, 0, 0, 0, 0, 0
    utime = 0
    hw_trg_ts, vis_trg_ts, dis_trg_ts, conn_trg_ts, acc_trg_ts = None, None, None, None, None
    for l in lines:
        ts = int(l[0])
        ac = l[1].strip()

        # get cpu stats
        if ac == "CPU_PID_STAT":
            utime = Helper.get_process_utime(l)
        elif ac == "CPU_STAT":
            idle = Helper.get_cpu_idle_time(l)
            non_idle = Helper.get_cpu_non_idle_time(l)
            total = idle + non_idle
            data['cpu'].append((total, idle, utime))

        # get memory stats
        if l[1] == "MEMORY":
            rss = int(l[3]) * page_size   # Resident set size
            shared = int(l[4]) * page_size  # shared memory
            data['mem'].append((rss, shared))

        # process actions
        if ac in ['HARDWARE_CALL_BEGIN', 'HARDWARE_CALL_END', 'HARDWARE']:
            if ac == 'HARDWARE_CALL_BEGIN':
                hw_ts = ts
            append(hw, hw_trg_ts, hw_ts, ts, 1, ac, ['HARDWARE'])
            add_to_map(res_map, data, ac, hw_ts, ts)
            hw_trg_ts = None

        elif ac in ['DISCOVERY_CALL_BEGIN', 'DISCOVERY_CALL_END', 'DISCOVERING', 'DISCOVERY_DONE']:
            if ac == 'DISCOVERY_CALL_BEGIN':
                dis_ts = ts
            append(dis, dis_trg_ts, dis_ts, ts, 1, ac, ['DISCOVERY_DONE'])
            add_to_map(res_map, data, ac, dis_ts, ts)
            dis_trg_ts = None

        elif ac in ['VISIBILITY_CALL_BEGIN', 'VISIBILITY_CALL_END', 'VISIBLE', 'VISIBILITY_DONE']:
            if ac == 'VISIBILITY_CALL_BEGIN':
                vis_ts = ts
            append(vis, vis_trg_ts, vis_ts, ts, 1, ac, ['VISIBILITY_DONE'])
            add_to_map(res_map, data, ac, vis_ts, ts)
            vis_trg_ts = None

        elif ac in ['CONNECTION_CALL_BEGIN', 'CONNECTION_CALL_END', 'CONNECTED']:
            if ac == 'CONNECTION_CALL_BEGIN':
                conn_ts = ts
            append(conn, conn_trg_ts, conn_ts, ts, 1, ac, ['CONNECTED'])
            add_to_map(res_map, data, ac, conn_ts, ts)
            conn_trg_ts = None

        elif ac in ['ACCEPT_CALL_BEGIN', 'ACCEPT_CALL_END', 'ACCEPTING']:
            if ac == 'ACCEPT_CALL_BEGIN':
                acc_ts = ts
            append(acc, acc_trg_ts, acc_ts, ts, 1, ac, ['ACCEPTING'])
            add_to_map(res_map, data, ac, acc_ts, ts)
            acc_trg_ts = None

        elif ac in ['PING_REQ', 'PING_RESP']:
            size = int(l[2])
            if ac == 'PING_REQ':
                ping_ts = ts
            if size == 0:
                append(ping, None, ping_ts, ts, 1, ac, ['PING_RESP'])
            else:
                append(ping, None, ping_ts, ts, 2, ac, ['PING_RESP'])
            add_to_map(res_map, data, ac, ping_ts, ts)

        elif ac in ['EVENT_TRIGGER']:
            a = l[2].strip()
            if a in ['LINK_HARDWARE_ON']:
                hw_trg_ts = ts
            elif a in ['LINK_VISIBILITY_ON', 'LINK_VISIBILITY_DONE']:
                vis_trg_ts = ts
            elif a in ['LINK_DISCOVERY_ON', 'LINK_DISCOVERY_DONE']:
                dis_trg_ts = ts
            elif a in ['LINK_CONNECTION_NEW']:
                conn_trg_ts = ts
            elif a in ['LINK_CONNECTION_SERVER_ON']:
                acc_trg_ts = ts

            append(evt, None, ts, ts, 3, l[2].strip(), [a])

    series = [{'name': "Hardware", 'data': hw, },
              {'name': "Discover", 'data': dis, },
              {'name': "Visible", 'data': vis, },
              {'name': "Connect", 'data': conn, },
              {'name': "Accept", 'data': acc, },
              {'name': "Ping", 'data': ping,
               'marker': {'enabled': True, 'lineWidth': 3}},
              {'name': "Events", 'data': evt,
               'marker': {'enabled': True, 'lineWidth': 3}}
              ]
    return {
        'title': title,
        'x': {'title': "Ts", 'min': x_min, 'max': x_max},
        'y': {'title': "Total", 'min': y_min, 'max': y_max},
        'series': series,
        'res_map': res_map,
    }


def get_aggr_action_stats(techs):
    categories = ["Hardware", "Discovery", "Visibility",
                  "Connection", "Accept", "Ping", "Transfer"]
    values = ["HARDWARE", "DISCOVERY_DONE", "VISIBLE", "CONNECTED",
              "ACCEPTING", "PING_RESP", "TRANSFER"]  # , "TRANSFER"
    series = []
    table = []
    # colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
    #           '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
    #           '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
    #           '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
    #           '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C',  # purple + deep purple
    #           '#FFE0B2', '#FF8A65', '#FFA726', '#FF5722', '#F57C00', '#D84315', '#E65100',  # orange + deep orange
    #           '#D7CCC8', '#BCAAA4', '#BCAAA4', '#8D6E63', '#5D4037', '#4E342E', '#3E2723',  # brown
    #           ]
    #colors = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9",
    #          "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"]
    colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
            '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
            '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
            '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
            '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C', # purple + deep purple
            ]
    for i in range(len(techs)):
        t = techs[i]
        (server_files, client_files) = listFiles(t)
        files = server_files + client_files
        (_, act_key_values) = get_action_stats(t, files)

        chart_data = []
        chart_data_error = []
        table_data = {}

        for k in range(len(values)):
            stat = act_key_values[values[k]]
            if values[k] != "TRANSFER":
                mean, std, n = stat['mean'] / \
                    1000, stat['std'] / 1000, stat['n']
                chart_data.append(mean)
                conf = 0 if n == 0 else CONF_95 * (std / sqrt(n))
                chart_data_error.append([mean - conf, mean + conf])
            table_data[categories[k]] = stat

        series.append({
            'name': tmap[i],
            'color': colors[(i * 7) + 5],
            'data': chart_data
        })
        series.append({
            'name': t + " Error",
            'type': 'errorbar',
            #'color': colors[i],
            'data': chart_data_error
        })

        table_data['keys'] = categories
        table.append({
            'title': t,
            'color': colors[(i*7) + 5],
            'data': table_data
        })

    return {
        'type': 'time',
        'chart': [{
            'title': "Aggregate time by Technology",
            'x': {'title': ""},
            'y': {'title': "Time (s)", 'unit': 's'},
            'categories': categories,
            'series': series,
            'plot': 'plotColumn',
        }],
        'table': table
    }


def get_memory_aggr_stats(techs):
    categories = ["Server", "Client"]
    series = []
    table = []

    colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
            '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
            '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
            '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
            '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C', # purple + deep purple
            ]
    for i in range(len(techs)):
        tech = techs[i]
        (server_files, client_files) = listFiles(tech)
        (_, mem_server_values) = get_memory_stats(tech, server_files)
        (_, mem_client_values) = get_memory_stats(tech, client_files)

        server_rss_mean = mem_server_values['Rss']['mean']
        server_rss_std = mem_server_values['Rss']['std']
        server_rss_n = mem_server_values['Rss']['n']
        server_diff = server_rss_mean - mem_server_values['Shared']['mean']

        client_rss_mean = mem_client_values['Rss']['mean']
        client_rss_std = mem_client_values['Rss']['std']
        client_rss_n = mem_client_values['Rss']['n']
        client_diff = client_rss_mean - mem_client_values['Shared']['mean']

        series.append({
            'name': tmap[i],  # + " By Process"
            'color': colors[(i * 7) + 1],
            'data': [server_diff, client_diff],
            'stack': tech
        })

        series.append({
            'name': tmap[i] + " Shared",
            'color': colors[(i * 7) + 6],
            'data': [mem_server_values['Shared']['mean'],
                     mem_client_values['Shared']['mean']],
            'stack': tech,
            'showInLegend': False,
        })

        server_conf = CONF_95 * (server_rss_std / sqrt(server_rss_n))
        client_conf = CONF_95 * (client_rss_std / sqrt(client_rss_n))

        series.append({
            'name': tech + " Error",
            "type": "errorbar",
            "stacking": False,
            'data': [
                [server_rss_mean - server_conf, server_rss_mean + server_conf],
                [client_rss_mean - client_conf, client_rss_mean + client_conf]],
            'stack': tech
        })

        cats = ["Server RSS", "Server Shared", "Server By Process",
                "Client RSS", "Client Shared", "Client By Process", ]
        table_data = {
            "Server RSS": mem_server_values['Rss'],
            "Server Shared": mem_server_values['Shared'],
            "Server By Process": {'mean': server_diff, 'std': 0, 'q1': 0,
                                  'q2': 0, 'q3': 0, 'min': 0, 'max': 0},
            "Client RSS":  mem_client_values['Rss'],
            "Client Shared": mem_client_values['Rss'],
            "Client By Process": {'mean': client_diff, 'std': 0, 'q1': 0,
                                  'q2': 0, 'q3': 0, 'min': 0, 'max': 0},
        }

        table_data['keys'] = cats
        table.append({
            'title': tech,
            'color': colors[(i * 7) + 5],
            'data': table_data
        })

    return {
        'type': 'memory',
        'chart': [{
            'title': "Aggregate memory by Technology",
            'x': {'title': ""},
            'y': {'title': "Length (MB)", 'unit': 'MB'},
            'categories': categories,
            'series': series,
            'plot': 'plotStackedGroup',
            'stacking': 'normal',
        }],
        'table': table
    }
    #  {
    #     'title': "Aggregate memory by Technology",
    #     'x': {'title': ""},
    #     'y': {'title': "Percentage", 'unit': 'MB'},
    #     'categories': categories,
    #     'series': series,
    #     'plot': 'plotStackedGroup',
    #     'stacking': 'percent',
    # }

def get_cpu_aggr_stats(techs):
    categories = ["Server", "Client"] # "Client Formation", "Client Transfer"
    series = []
    table = []

    colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
            '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
            '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
            '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
            '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C', # purple + deep purple
            ]
    for i in range(len(techs)):
        tech = techs[i]
        (server_files, client_files) = listFiles(tech)
        (_, cpu_server_values, cpu_server_phases) = get_cpu_stats(tech, server_files)
        (_, cpu_client_values, cpu_client_phases) = get_cpu_stats(tech, client_files)

        server_mean = cpu_server_values['Process']['mean'] * 100
        server_total_mean = cpu_server_values['Total']['mean'] * 100
        server_std = cpu_server_values['Total']['std'] * 100
        server_n = cpu_server_values['Total']['n']

        client_mean = cpu_client_values['Process']['mean'] * 100
        client_total_mean = cpu_client_values['Total']['mean'] * 100
        client_std = cpu_client_values['Total']['std'] * 100
        client_n = cpu_client_values['Total']['n']

        process_form_mean = cpu_client_phases['process_form']['mean'] * 100
        total_form_mean = cpu_client_phases['total_form']['mean'] * 100 - process_form_mean
        process_ping_mean = cpu_client_phases['process_ping']['mean'] * 100
        total_ping_mean = cpu_client_phases['total_ping']['mean'] * 100 - process_ping_mean

        series.append({
            'name': tmap[i],  # + " Process"
            'color': colors[(i*7) + 1],
            'data': [server_mean, client_mean], # process_form_mean, process_ping_mean]
            'stack': tech
        })
        series.append({
            'name': tmap[i] + " Total",
            'color': colors[(i*7) + 6],
            'data': [server_total_mean - server_mean, client_total_mean - client_mean], # total_form_mean, total_ping_mean
            'stack': tech,
            'showInLegend': False,
        })
        server_conf = CONF_95 * (server_std / sqrt(server_n))
        client_conf = CONF_95 * (client_std / sqrt(client_n))

        form_conf = CONF_95 * ((cpu_client_phases['total_form']['std'] * 100) / sqrt(cpu_client_phases['total_form']['n']))
        ping_conf = CONF_95 * ((cpu_client_phases['total_ping']['std'] * 100) / sqrt(cpu_client_phases['total_ping']['n']))

        server_total = server_total_mean
        client_total = client_total_mean
        form_total = process_form_mean + total_form_mean
        ping_total = process_ping_mean + total_ping_mean
        series.append({
            'name': tmap[i] + " Error",
            "type": "errorbar",
            "stacking": False,
            'data': [
                [server_total - server_conf, server_total + server_conf],
                [client_total - client_conf, client_total + client_conf],
                # [form_total - form_conf, form_total + form_conf],
                # [ping_total - ping_conf, ping_total + ping_conf]
                ],
            'stack': tech
        })
        cats = ["Server Total", "Server Process",
                "Client Total", "Client Process"]
        table_data = {
            "Server Total": cpu_server_values['Total'],
            "Server Process": cpu_server_values['Process'],
            "Client Total":  cpu_client_values['Total'],
            "Client Process": cpu_client_values['Process'],
        }

        table_data['keys'] = cats
        table.append({
            'title': tech,
            'color': colors[(i*7) + 6],
            'data': table_data
        })

    return {
        'type': 'cpu',
        'chart': [{
            'title': "Aggregate cpu by Technology",
            'x': {'title': ""},
            'y': {'title': "Percentage", 'unit': '%'},
            'categories': categories,
            'series': series,
            'plot': 'plotStackedGroup',
            'stacking': 'normal',
        }],
        'table': table
    }


def get_battery_aggr_stats(techs):
    categories = ["Server", "Client"] #["Client"]
    sec_cat = ["Server", "Client", "Client Formation", "Client Transfer"] #  "Client Formation", "Client Transfer"
    series = []
    sec_series = []
    table = []

    colors = ['#C5CAE9', '#64B5F6', '#5C6BC0', '#1E88E5', '#303F9F', '#1565C0', '#1A237E',  # indigo + blue
            '#B2DFDB', '#81C784', '#26A69A', '#43A047', '#00796B', '#2E7D32', '#004D40',  # teal + green
            '#F8BBD0', '#E57373', '#EC407A', '#E53935', '#C2185B', '#C62828', '#880E4F',  # pink + red
            '#F0F4C3', '#FFF176', '#D4E157', '#FDD835', '#AFB42B', '#F9A825', '#827717',  # lime + yellow
            '#E1BEE7', '#9575CD', '#AB47BC', '#673AB7', '#7B1FA2', '#512DA8', '#4A148C', # purple + deep purple
            ]
    for i in range(len(techs)):
        tech = techs[i]
        key_order = ['Battery All', 'Battery During Comm',
                     'Battery During Transfer']
        (server_files, client_files) = listFiles(tech)
        (_, batt_server_values, sec_server_values) = get_battery_stats(tech, server_files)
        (_, batt_client_values, sec_client_values) = get_battery_stats(tech, client_files)

        server_mean_all = batt_server_values['Battery All']['mean']
        server_std_all = batt_server_values['Battery All']['std']
        server_n_all = batt_server_values['Battery All']['n']
        server_form = server_mean_all - \
            batt_server_values['Battery During Comm']['mean']

        client_mean_all = batt_client_values['Battery All']['mean']
        client_std_all = batt_client_values['Battery All']['std']
        client_n_all = batt_client_values['Battery All']['n']
        client_form = client_mean_all - \
            batt_client_values['Battery During Comm']['mean']
        series.append({
            'name': tmap[i],  # + " On Formation"
            'color': colors[(i*7) + 1],
            'data': [server_form, client_form], #[client_form],
            'stack': tech
        })
        server_ping = batt_server_values['Battery During Comm']['mean'] - \
            batt_server_values['Battery During Transfer']['mean']
        client_ping = batt_client_values['Battery During Comm']['mean'] - \
            batt_client_values['Battery During Transfer']['mean']
        series.append({
            'name': tmap[i] + " On Ping",
            'color': colors[(i*7) + 6],
            'data': [server_ping, client_ping], #[client_ping],
            'stack': tech,
            'showInLegend': False,
        })
        server_transf = batt_server_values['Battery During Transfer']['mean']
        client_transf = batt_client_values['Battery During Transfer']['mean']
        series.append({
            'name': tmap[i] + " On Transfer",
            'color': colors[(i*7) + 4],
            'data': [server_transf, client_transf],#[client_transf],
            'stack': tech,
            'showInLegend': False,
        })
        server_conf = CONF_95 * (server_std_all / sqrt(server_n_all))
        client_conf = CONF_95 * (client_std_all / sqrt(client_n_all))

        server_total = server_mean_all
        client_total = client_mean_all
        # commentar para fazer plot to gráfico da percentagem
        series.append({
            'name': tmap[i] + " Error",
            "type": "errorbar",
            "stacking": False,
            'data': [
                [server_total - server_conf, server_total + server_conf],
                [client_total - client_conf, client_total + client_conf]],
            'stack': tech
        })

        cats = ["Server Total", "Client Total", "Client Comm", "Client Transfer",
                "Client Ping", "Client Formation"]
        table_data = {
            "Server Total": batt_server_values['Battery All'],
            "Client Total":  batt_client_values['Battery All'],
            "Client Comm": batt_client_values['Battery During Comm'],
            "Client Transfer": batt_client_values['Battery During Transfer'],
            "Client Ping": {'mean': client_ping, 'std': 0, 'q1': 0,
                            'q2': 0, 'q3': 0, 'min': 0, 'max': 0},
            "Client Formation": {'mean': client_form, 'std': 0, 'q1': 0,
                                 'q2': 0, 'q3': 0, 'min': 0, 'max': 0},
        }
        table_data['keys'] = cats
        table.append({
            'title': tech,
            'color': colors[(i*7) + 6],
            'data': table_data
        })

        s_mean = sec_server_values['during_all']['mean']
        c_mean = sec_client_values['during_all']['mean']
        cform_mean = sec_client_values['during_form']['mean']
        ctransfer_mean = sec_client_values['during_transfer']['mean']

        sec_series.append({
            'name': tmap[i],
            'color': colors[(i*7) + 1],
            'data' : [s_mean, c_mean, cform_mean, ctransfer_mean] # cform_mean, ctransfer_mean
        })
        s_conf = CONF_95 * (sec_server_values['during_all']['std'] / sqrt(16))
        c_conf = CONF_95 * (sec_client_values['during_all']['std'] / sqrt(16))
        cform_conf = CONF_95 * (sec_client_values['during_form']['std']/ sqrt(16))
        ctransfer_conf =  CONF_95 * (sec_client_values['during_transfer']['std'] / sqrt(16))
        sec_series.append({
            'name': tmap[i] + " Error",
            "type": "errorbar",
            "stacking": False,
            'data': [
                [s_mean - s_conf, s_mean + s_conf],
                [c_mean - c_conf, c_mean + c_conf],
                [cform_mean - cform_conf, cform_mean + cform_conf],
                [ctransfer_mean - ctransfer_conf, ctransfer_mean + ctransfer_conf],
                ]
        })


    return {
        'type': 'battery',
        'chart': [
            {
                'title': "Aggregate battery by Technology",
                'x': {'title': ""},
                'y': {'title': "Joules", 'unit': 'J'},
                'categories': categories,
                'series': series,
                'plot': 'plotStackedGroup',
                'stacking': 'normal',
            }
            ,
            {
                'title': "Battery per second",
                'x': {'title': ""},
                'y': {'title': "Power (Watt)", 'unit': 'W'},
                'categories': sec_cat,
                'series': sec_series,
                'plot': 'plotColumn',
            }
        ],
        'table': table
    }


def calc_stats(lst):
    avg, std, q1, q2, q3, mi, ma = 0, 0, 0, 0, 0, 0, 0
    if(len(lst) > 1):
        lst.sort()
        avg, std = mean(lst), stdev(lst)
        mi, ma = min(lst), max(lst)
        q2 = median(lst)

        mid = len(lst) // 2
        if(len(lst) % 2 == 0):
            q1 = median(lst[:mid])
            q3 = median(lst[mid:])
        else:
            q1 = median(lst[:mid])
            q3 = median(lst[(mid + 1):])

    return {'mean': avg, 'std': std, 'q1': q1,
            'q2': q2, 'q3': q3, 'min': mi, 'max': ma, 'n': len(lst)}


def split_file_lines(path):
    """Reads a file, splits each line and retuns a list with all lines.
    Args:
    path -- file path
    Return: [ [line 1], [line 2] ... [line n] ]. Each line is a list.
    """
    lines = []
    with open(path) as f:
        for line in f:
            s = re.split('  | |, ', line)
            lines.append(s)
    return lines


def listFiles(pathDir):
    serverList = EXP_NUMBER * [None]
    clientList = EXP_NUMBER * [None]
    files = os.listdir(pathDir)
    for f in files:
        s = re.split('_|\.', f)
        idx = int(s[-2]) - 1
        if s[-3] == 'server':
            serverList[idx] = f
        else:
            clientList[idx] = f
    return (serverList, clientList)
