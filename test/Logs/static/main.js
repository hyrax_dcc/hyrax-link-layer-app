(function(H) {
    function symbolWrap(proceed, symbol, x, y, w, h, options) {
        if (symbol.indexOf('text:') === 0) {
            var text = symbol.split(':')[1],
                svgElem = this.text(text, x, y + h)
                .css({
                    fontFamily: 'FontAwesome',
                    fontSize: h * 2
                });

            if (svgElem.renderer.isVML) {
                svgElem.fillSetter = function(value, key, element) {
                    element.style.color = H.Color(value).get('rgb');
                };
            }
            return svgElem;
        }
        return proceed.apply(this, [].slice.call(arguments, 1));
    }
    H.wrap(H.SVGRenderer.prototype, 'symbol', symbolWrap);
    if (H.VMLRenderer) {
        H.wrap(H.VMLRenderer.prototype, 'symbol', symbolWrap);
    }

    // Load the font for SVG files also
    H.wrap(H.Chart.prototype, 'getSVG', function(proceed) {
        var svg = proceed.call(this);
        svg = '<?xml-stylesheet type="text/css" ' +
            'href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" ?>' +
            svg;
        return svg;
    });

    // Pass error messages
    H.Axis.prototype.allowNegativeLog = true;

    // Override conversions
    H.Axis.prototype.log2lin = function (num) {
        var isNegative = num < 0,
            adjustedNum = Math.abs(num),
            result;
        if (adjustedNum < 10) {
            adjustedNum += (10 - adjustedNum) / 10;
        }
        result = Math.log(adjustedNum) / Math.LN10;
        return isNegative ? -result : result;
    };
    H.Axis.prototype.lin2log = function (num) {
        var isNegative = num < 0,
            absNum = Math.abs(num),
            result = Math.pow(10, absNum);
        if (result < 10) {
            result = (10 * (result - 1)) / (10 - 1);
        }
        return isNegative ? -result : result;
    };
}(Highcharts));

/**
Arguments style:
{
  title: "<value>",
  x: {
    min: <value>
    max: <value>
    title: "<value>"
  },
  y: {
    min: <value>
    max: <value>
    title: "<value>"
  },
  series: [
    {name: "<serie_name>", data: {x <val>: , y: <val> ...}},
    ...
  ]
}
**/

function plotSpline(divId, json) {
    Highcharts.chart(divId, {
        chart: {
            type: "spline",
            zoomType: 'x',
        },
        title: {
            text: json.title,
        },
        xAxis: {
            title: {
                text: json.x.title
            },
            type: 'datetime',
            min: json.x.min,
            max: json.x.max,
        },
        yAxis: {
            title: {
                text: json.y.title
            },
        },
        legend: {
            enabled: true,
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>({point.y})<br/>',
        },
        plotOptions: {
            line: { // <--- Chart type here, check the API reference first!
                marker: {
                    enabled: false
                }
            },
            series: {
                turboThreshold: 0,
                marker: {
                    enabled: false,
                },
            }
        },
        credits: {
            enabled: false
        },
        series: json.series,
    });
}

function plotSplineActions(divId, json) {
    Highcharts.chart(divId, {
        chart: {
            type: "spline",
            zoomType: 'x',
        },
        title: {
            text: json.title,
        },
        xAxis: {
            title: {
                text: json.x.title
            },
            type: 'datetime',
            min: json.x.min,
            max: json.x.max,
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: json.y.title
            },
        },
        legend: {
            enabled: true,
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span> -> <p>{point.action}: ({point.y:,.3f}) - RRT: {point.diff} </p>',
            split: true
        },
        plotOptions: {
            line: { // <--- Chart type here, check the API reference first!

            },
            series: {
                turboThreshold: 0,
            }
        },
        credits: {
            enabled: false
        },
        series: json.series,
    });
}

function plotStackedArea(divId, json) {
    Highcharts.chart(divId, {
        chart: {
            type: "area",
            zoomType: 'x',
        },
        title: {
            text: json.title
        },
        xAxis: {
            title: {
                text: json.x.title
            },
            type: 'datetime',
            min: json.x.min,
            max: json.x.max,
        },
        yAxis: {
            title: {
                text: json.y.title
            },
        },
        legend: {
            enabled: true,
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>({point.y:,.3f})<br/>',
            split: true
        },
        plotOptions: {
            area: {
                marker: {
                    enabled: false
                }
            },
            series: {
                turboThreshold: 0,
                marker: {
                    enabled: false,
                },
            }
        },
        credits: {
            enabled: false
        },
        series: json.series,
    });
}

function plotColumn(divId, json) {
    console.log(json);
    Highcharts.chart(divId, {
        chart: {
            type: 'column',
            /*style: {
                fontSize: "14px",
            },*/
            backgroundColor:"rgba(255, 255, 255, 0)",
        },
        title: {
            text: ""//json.title
        },
        xAxis: {
            categories: json.categories,
            crosshair: true,
            labels: {
            /*  style: {
                  fontSize: "14px",
                  //color: "#000000",
              },*/
            }
        },
        yAxis: {
            min: 0,
            //minorTickInterval: 'auto',
            tickInterval:1,
            title: {
                text: json.y.title,
                /*style: {
                		fontSize: "14px",
                    //color: "#000000",
                },*/
            },
            labels: {
                /*style: {
                		fontSize: "14px",
                    //color: "#000000",
                }*/
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} ' + json.y.unit + '</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
        },
        /*legend: {
              itemDistance: 50,
              itemStyle: {
                fontSize: "14px",
                //color: '#616161',
                //font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
              },
        },*/
        credits: {
            enabled: false
        },
        series: json.series,
        /*/exporting: {
            enabled: true,
            sourceWidth: 1920,
            sourceHeight: 800,
            scale: 1,
        },*/
    });
}

function plotStackedGroup(divId, json) {
  console.log("gwww");
    Highcharts.chart(divId, {
        chart: {
            type: 'column',
            style: {
                fontSize: "20px",
            },
            backgroundColor:"rgba(255, 255, 255, 0)",
        },

        title: {
            text: ""//json.title
        },

        xAxis: {
            categories: json.categories,
            labels: {
              style: {
                  fontSize: "20px",
                  color: "#000000",
              },
            }
        },

        yAxis: {
           //type: 'logarithmic',
            allowDecimals: false,
            min: 0,
            title: {
                text: json.y.title,
                style: {
                    fontSize: "20px",
                    color: "#000000",
                },
            },
            labels: {
              style: {
                  fontSize: "20px",
                  color: "#000000",
              },
            }
        },

        tooltip: {
            formatter: function() {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        plotOptions: {
            column: {
                stacking: json.stacking
            }
        },
        credits: {
            enabled: false
        },
        legend: {
              itemDistance: 50,
              itemStyle: {
                fontSize: "20px",
              },
        },
        series: json.series,
        exporting: {
            enabled: true,
            sourceWidth: 1920,
            sourceHeight: 800,
            scale: 1,
        },
    });
}

function plotBoxPlot(divId, json) {
  console.log(json);
  Highcharts.chart(divId, {

    chart: {
        type: 'boxplot'
    },

    title: {
        text: 'Highcharts Box Plot Example'
    },

    legend: {
        enabled: false
    },

    xAxis: {
        categories: ['1', '2', '3', '4', '5'],
        title: {
            text: json.x.title
        }
    },

    yAxis: {
        title: {
            text: json.y.title
        },
        /*plotLines: [{
            value: 932,
            color: 'red',
            width: 1,
            label: {
                text: 'Theoretical mean: 932',
                align: 'center',
                style: {
                    color: 'gray'
                }
            }
        }]*/
    },

    series: [{
        name: 'Observations',
        data: json.series.data,/*/[
            [760, 801, 848, 895, 965],
            [733, 853, 939, 980, 1080],
            [714, 762, 817, 870, 918],
            [724, 802, 806, 871, 950],
            [834, 836, 864, 882, 910]
        ],*/
        tooltip: {
            headerFormat: '<em>Experiment No {point.key}</em><br/>'
        }
    }, /*{
        name: 'Outlier',
        color: Highcharts.getOptions().colors[0],
        type: 'scatter',
        data: [ // x, y positions where 0 is the first category
            [0, 644],
            [4, 718],https://www.jornaldenegocios.pt/
            [4, 951],
            [4, 969]
        ],
        marker: {
            fillColor: 'white',
            lineWidth: 1,
            lineColor: Highcharts.getOptions().colors[0]
        },
        tooltip: {
            pointFormat: 'Observation: {point.y}'
        }
    }*/]

});
    /*Highcharts.chart(divId, {
      chart: {
          type: 'boxplot',
          zoomType: 'y',
      },

      title: {
          text: "" //json.title
      },

      xAxis: {
          categories: json.x.categories,
          crosshair: true,
          title: {
              text: json.x.title
          }
      },
      yAxis: {
          min: 0,
          type: json.y.type,
          minorTickInterval: 'auto',
          title: {
              text: json.y.title
          },
      },
      credits: {
          enabled: false,
      },
      exporting: {
          enabled: true,
          width: 1920,
          height: 720,
      },
      series: json.series
    });*/
}
