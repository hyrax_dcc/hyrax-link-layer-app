# About

Hyrax Middleware: a middleware for mobile edge clouds.

Copyright (C) 2019 INESC TEC. 

This software is authored by João Filipe Rodrigues. Its development was financially supported by HYRAX project (Hyrax: Crowd-Sourcing Mobile Devices to Develop Edge Clouds - Ref: CMUP-ERI/FIA/0048/2013) and was part of João's PhD work at the Computer Science Department, Faculty of Sciences, University of Porto,  supervised by Luís Lopes and Fernando Silva, and with contributions by Eduardo Marques, Rolando Martins, and Joaquim Silva. 

If you use the Hyrax middleware in a work that leads to a scientific publication, we would appreciate if you would kindly cite the following document:

João Rodrigues, A Middleware for Mobile Edge-Cloud Applications, PhD Thesis, Computer Science Department, Faculty of Sciences, University of Porto, 2019

The document can be found at https://hdl.handle.net/10216/118307

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

This work makes use and is distributed with the following libraries:
- Jdeferred (http://jdeferred.org)
- Little Proxy (https://github.com/adamfisk/LittleProxy)
- Google Protocol Buffers (https://developers.google.com/protocol-buffers/)
- Google Protocol Buffers - Java Format (https://github.com/bivas/protobuf-java-format)

You can reach INESC TEC at info@inesctec.pt, or

Campus da Faculdade de Engenharia da Universidade do Porto
Rua Dr. Roberto Frias
4200-465 Porto
Portugal

A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.


## Hyrax Link Layer App Test

This app means to test the [Hyrax Link Layer](https://bitbucket.org/hyrax_dcc/android-link-layer-library/)
and serve, also, as developer guide. This App has several activities and one
controller where's the developer may see the similarities of using distinct
technologies. In fact they will be able to confirm that the code used for all the
technologies is mostly common.

## Install
This project has a reference to another project so let's clone the project
recursively.
```
git clone --recursive git@bitbucket.org:hyrax_dcc/hyrax-link-layer-app.git
```
