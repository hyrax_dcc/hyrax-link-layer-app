#!/bin/bash

LOCATION="/home/joao/AndroidStudioProjects/LinkLayerApp/app/build/outputs/apk/app-debug.apk";
DEVICES_NUMBER="20";

#Get the devices list
getDevices() {
	number=$1 #the number of devices that you want to select
	echo `adb devices | grep -v List | cut -f 1 | head -n $number`;
}

ADBDEVICES=$(getDevices "$DEVICES_NUMBER");
for SERIAL in $ADBDEVICES; do
	printf "Installing on %s\n" "$SERIAL"
	adb -s $SERIAL install -r $LOCATION
done

