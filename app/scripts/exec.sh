
#Get the devices list
getDevices() {
	number=$1 #the number of devices that you want to select
	echo `adb devices | grep -v List | cut -f 1 | head -n $number`;
}

#starts activity and blocks until the activity has started
startActivity() {
	ADBDEVICES=$1;
	for SERIAL in $ADBDEVICES; do 
		(adb -s $SERIAL shell am start -W -n pt.up.fc.dcc.hyrax.linklayerapp/.MainActivity \
			 > /dev/null) &
	done
	wait
}

stopActivity() {
	ADBDEVICES=$1;
	for SERIAL in $ADBDEVICES; do 
		(adb -s $SERIAL shell am broadcast -a android.intent.endSetup > /dev/null) &
	done
	wait
}

NUMBER=20;
devices=$(getDevices "$NUMBER");

START="start"
STOP="stop"

if [ "$1" == "$START" ]; then
	 startActivity "$devices"
else
	stopActivity "$devices"
fi




