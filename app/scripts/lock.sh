#Get the devices list
getDevices() {
	number=$1 #the number of devices that you want to select
	echo `adb devices | grep -v List | cut -f 1 | head -n $number`;
}

NUMBER=20;
devices=$(getDevices "$NUMBER");

for SERIAL in $devices; do 
	(adb -s $SERIAL shell input keyevent 26) &
done
wait


