/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.experiment;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.bluetooth.BluetoothLink;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothComm;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.util.Objects;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.BluetoothSocketComm;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.SocketComm;

/**
 * Bluetooth experiment.
 */
public class BluetoothExperiment extends Experiment<BluetoothLink> {

    /**
     * Constructor.
     *
     * @param linkHelper link helper object
     * @param server     <tt>true</tt> if server, <tt>false</tt> otherwise
     */
    public BluetoothExperiment(@NonNull LinkHelper<BluetoothLink> linkHelper, boolean server) {
        super(linkHelper, server);
    }


    @Override
    ServerProperties getServerProperties() {
        return linkHelper.getLink().getFeatures().newServerBuilder().build();
    }

    @Override
    VisibilityProperties getVisibilityProperties() {
        return linkHelper.getLink().getFeatures().newVisibilityBuilder().build();
    }

    @Override
    DiscoveryProperties getDiscoveryProperties() {
        return linkHelper.getLink().getFeatures().newDiscoveryBuilder()
                .setScannerFilter(device -> "Nexus 9".equals(device.getName()))
                .setStopRule(devices -> devices.size() > 0)
                .build();
    }

    @Override
    ConnectionProperties getConnectionProperties(@NonNull Device device) {
        return linkHelper.getLink().getFeatures().newConnectionBuilder(device).build();
    }

    @Override
    SocketComm getSocketObject(@NonNull Device device) {
        BluetoothComm comm = linkHelper.getLink().getFeatures().getCommLink(device);
        Objects.requireNonNull(comm);
        return BluetoothSocketComm.getInstance(linkHelper.getActivityComm(), comm);
    }

    @Override
    void executeServer(@NonNull DoneCallback doneCallback) {
        //listens for new bluetooth connections
        linkHelper.getLink().listen(LinkEvent.LINK_CONNECTION_NEW,
                (LinkListener<Device>) (eventType, outcome) -> {
                    BluetoothComm comm = linkHelper.getLink().getFeatures().getCommLink(outcome.getOutcome());
                    Objects.requireNonNull(comm);
                    BluetoothSocketComm.getInstance(linkHelper.getActivityComm(), comm);
                });
        super.executeServer(doneCallback);
    }


    @Override
    public void finish() {

    }
}
