/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.socket;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.bluetooth.BluetoothLeLink;
import org.hyrax.link.misc.Observer;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityComm;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;

/**
 * Bluetooth low energy client manager.
 */
public class BluetoothLeClient extends BluetoothGattCallback implements SocketComm {
    private final LinkHelper<BluetoothLeLink> linkHelper;
    private final ActivityComm comm;
    private Sender sender;
    private int msgId;
    private final ExecutorService executorService;
    //connection ready object
    private final Object READY_LOCK = new Object();
    @Nullable
    private Observer<Boolean> readyListener;
    private boolean isReady;

    /**
     * Constructor.
     *
     * @param linkHelper link helper controller
     */
    public BluetoothLeClient(@NonNull LinkHelper<BluetoothLeLink> linkHelper) {
        this.linkHelper = linkHelper;
        this.comm = linkHelper.getActivityComm();
        this.msgId = 0;
        this.sender = null;
        this.executorService = Executors.newSingleThreadExecutor();
    }

    /**
     * Returns a gatt client object
     *
     * @return bluetooth gatt client object
     */
    @Nullable
    private BluetoothGatt getGattClient() {
        Collection<Device> devices = linkHelper.getLink().getConnected();
        if (devices.size() > 0) {
            Device device = devices.iterator().next();
            return linkHelper.getLink().getFeatures().getBluetoothGatt(device);
        }
        return null;
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        comm.debug("Client: state changed " + newState);
        if (newState == STATE_CONNECTED)
            gatt.discoverServices();
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        comm.debug("Client: Services discovered " + status);
        for (BluetoothGattService service : gatt.getServices()) {
            comm.debug(service.getUuid().toString());
            for (BluetoothGattCharacteristic characteristic : service.getCharacteristics())
                comm.debug("---------> " + characteristic.getUuid().toString());
        }
        BluetoothGattService service = gatt.getService(BluetoothLeServer.SERVICE_ID);
        if (service != null) {
            comm.info("Service valid " + service.getUuid());
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(BluetoothLeServer.MESSAGE_CHARACTERISTIC);
            characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
            boolean not = gatt.setCharacteristicNotification(characteristic, true);
            comm.info("Notification enabled " + not);
        } else
            comm.warning("Service NULL");

        synchronized (READY_LOCK) {
            isReady = true;
            if (readyListener != null) {
                readyListener.onEvent(true);
                readyListener = null;
            }
        }
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        comm.debug(String.format(Locale.ENGLISH, "Client: characteristic read %s -> %d ",
                characteristic.getUuid().toString(),
                status));
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        /*comm.debug(String.format(Locale.ENGLISH, "Client: characteristic write %s -> %d ",
                characteristic.getUuid().toString(),
                status));*/
        if (BluetoothLeServer.MESSAGE_CHARACTERISTIC.equals(characteristic.getUuid()) ||
                BluetoothLeServer.BODY_CHARACTERISTIC.equals(characteristic.getUuid())) {
            if (sender != null)
                sender.notifyWrite();
        }
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        comm.debug(String.format(Locale.ENGLISH, "Client: characteristic changed %s",
                characteristic.getUuid().toString()));

        if (BluetoothLeServer.MESSAGE_CHARACTERISTIC.equals(characteristic.getUuid())) {
            byte[] value = characteristic.getValue();
            if (value.length == BluetoothLeServer.HEADER_SIZE) {
                int msgId = ByteBuffer.wrap(value, 0, 4).getInt();
                byte msgType = value[4];
                int msgSize = ByteBuffer.wrap(value, 5, 4).getInt();
                comm.info(String.format(Locale.ENGLISH,
                        "Message received: %d -> ID: %d Len: %d", msgType, msgId, msgSize));
                /*if (sender != null)
                    sender.notifyWrite();*/
            }
        }
    }

    @Override
    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor,
                                 int status) {
        comm.debug(String.format(Locale.ENGLISH, "Client: descriptor read %s -> %d ",
                descriptor.getUuid().toString(),
                status));
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor,
                                  int status) {
        comm.debug(String.format(Locale.ENGLISH, "Client: descriptor write %s -> %d ",
                descriptor.getUuid().toString(),
                status));
    }

    @Override
    public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
        comm.debug("Client: Reliable write complete " + status);
    }

    @Override
    public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
        comm.debug(String.format(Locale.ENGLISH, "Client: Remote RSSI %d, %d ", rssi, status));
    }

    @Override
    public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
        comm.debug(String.format(Locale.ENGLISH, "Client: Mtu %d, %d ", mtu, status));
        if (sender != null)
            sender.notifyWrite();
    }

    @Override
    public void connect(@NonNull Observer<Boolean> listener) {
        synchronized (READY_LOCK) {
            if (isReady)
                listener.onEvent(true);
            else {
                readyListener = listener;
            }
        }
    }

    @Override
    public void disconnect() {

    }

    @Override
    public void sendPingMultiple(int number) {
        if (number == 0)
            return;
        final long start_ts = System.currentTimeMillis();
        sendMessage(Type.PING, 0, tp -> {
            sender = null;
            long t = System.currentTimeMillis() - start_ts;
            comm.info(String.format(Locale.ENGLISH,
                    "Msg of type: %s RTT: %s Speed: %s",
                    tp,
                    SocketComm.timeToString(t),
                    SocketComm.speedToString(SocketComm.getSpeedPerSecond(0, t))));
            sendPingMultiple(number - 1);
        });
    }

    @Override
    public void sendMessage(@NonNull Type type, int payloadSize) {
        final long start_ts = System.currentTimeMillis();
        sendMessage(type, payloadSize, tp -> {
            long t = System.currentTimeMillis() - start_ts;
            comm.info(String.format(Locale.ENGLISH,
                    "Msg of type: %s RTT: %s Speed: %s",
                    tp,
                    SocketComm.timeToString(t),
                    SocketComm.speedToString(SocketComm.getSpeedPerSecond(payloadSize, t))));
        });
    }

    @Override
    public void sendMessage(@NonNull Type type, int payloadSize, @NonNull Observer<Type> listener) {
        BluetoothGatt gatt = getGattClient();
        Objects.requireNonNull(gatt);
        BluetoothGattService service = gatt.getService(BluetoothLeServer.SERVICE_ID);

        if (service != null) {
            this.sender = new Sender(gatt, service, msgId, type, payloadSize, listener);
            executorService.submit(sender);
            msgId += 1;
        } else {
            comm.error("Send Message: Service INVALID " + BluetoothLeServer.SERVICE_ID);
        }
    }

    /**
     * Wrapper sender class.
     */
    class Sender implements Runnable {
        private static final int READ_TIMEOUT = 2000;//2 seconds
        private final BluetoothGatt gatt;
        private final BluetoothGattService gattService;
        private final int msgId;
        private final SocketComm.Type type;
        private final int payloadSize;
        private final Lock lock = new ReentrantLock();
        private final Condition canWrite = lock.newCondition();
        private final Observer<Type> callback;

        /**
         * Constructor.
         *
         * @param gatt        bluetooth gatt device object
         * @param gattService gatt server object
         * @param msgId       message id to send
         * @param type        type of message
         * @param payloadSize message payload size
         * @param callback    callback triggered when all message has been sent
         */
        private Sender(@NonNull BluetoothGatt gatt,
                       @NonNull BluetoothGattService gattService,
                       int msgId, SocketComm.Type type, int payloadSize,
                       @NonNull Observer<Type> callback) {
            this.gatt = gatt;
            this.gattService = gattService;
            this.msgId = msgId;
            this.type = type;
            this.payloadSize = payloadSize;
            this.callback = callback;
        }

        @Override
        public void run() {
            lock.lock();
            try {
                //if payload is greater than 0 lets change the mtu and increase the connection priority
                if (payloadSize > 0) {
                    if (!gatt.requestMtu(BluetoothLeServer.MTU)) //mtu 512 bytes. Characteristics not support more.
                        throw new Exception("Request MTU failed");
                    if (!gatt.requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_HIGH))
                        throw new Exception("Cannot change connection priority");
                    if (!canWrite.await(READ_TIMEOUT, TimeUnit.MILLISECONDS))
                        throw new Exception("Mtu: Waiting expired");
                }
                //builds the message header
                ByteBuffer buffer = ByteBuffer.allocate(BluetoothLeServer.HEADER_SIZE);
                buffer.putInt(0, msgId); //message id
                buffer.put(4, type.getId()); //message type
                buffer.putInt(5, payloadSize); //message payload size

                //gets message characteristic
                BluetoothGattCharacteristic msgCharacteristic = gattService
                        .getCharacteristic(BluetoothLeServer.MESSAGE_CHARACTERISTIC);
                Objects.requireNonNull(msgCharacteristic);
                //sends characteristic changes
                msgCharacteristic.setValue(buffer.array());
                if (!gatt.writeCharacteristic(msgCharacteristic))
                    throw new Exception("Error setting message characteristic");

                //waits for confirmation so that can writes the body
                if (!canWrite.await(READ_TIMEOUT, TimeUnit.MILLISECONDS))
                    throw new Exception("Message: Waiting expired");
                //gets the message body characteristic
                BluetoothGattCharacteristic bodyCharacteristic = gattService
                        .getCharacteristic(BluetoothLeServer.BODY_CHARACTERISTIC);
                Objects.requireNonNull(bodyCharacteristic);

                //sends random payload
                Random r = new Random();
                int remain = payloadSize;
                while (remain > 0) {
                    //buffer len
                    int len = Math.min(remain, BluetoothLeServer.MTU - 3);
                    //buffer
                    byte[] b = new byte[len];
                    //generating random buffer bytes
                    r.nextBytes(b);
                    //write buffer
                    bodyCharacteristic.setValue(b);
                    if (!gatt.writeCharacteristic(bodyCharacteristic))
                        throw new Exception("Error sending message payload");
                    remain -= len;
                    if (!canWrite.await(READ_TIMEOUT, TimeUnit.MILLISECONDS))
                        throw new Exception("Payload: Waiting expired");
                }
                //triggers the message event. Message sent.
                callback.onEvent(type);

            } catch (Exception e) {//InterruptedException
                e.printStackTrace();
                comm.error(e.getMessage());
            } finally {
                /*if (payloadSize > 0) {
                    if (!gatt.requestMtu(20)) //go default
                        comm.warning("Request MTU failed");
                    if (!gatt.requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_BALANCED))//go default
                        comm.warning("cannot changed priority");
                }*/
                lock.unlock();
            }
        }

        /**
         * Notifies when the thread can continue send bytes.
         */
        private void notifyWrite() {
            lock.lock();
            try {
                canWrite.signal();
            } finally {
                lock.unlock();
            }
        }
    }
}
