/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.socket;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.Observer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityComm;

/**
 * Wifi server socket.
 */
public class WifiSocketServer implements Runnable {
    public static final int DEFAULT_PORT = 8080;
    private final ActivityComm activityComm;
    private final int userListenPort;
    private final AtomicBoolean running;
    private ServerSocket serverSocket;
    private Observer<Boolean> readyListener = (Observer<Boolean>) object -> {
    };

    /**
     * Constructor.
     *
     * @param activityComm activity communication object
     */
    public WifiSocketServer(@NonNull ActivityComm activityComm) {
        this(activityComm, 0);
    }

    /**
     * Constructor.
     *
     * @param activityComm activity communication object
     * @param listenPort   listen port
     */
    public WifiSocketServer(@NonNull ActivityComm activityComm, int listenPort) {
        this.activityComm = activityComm;
        this.userListenPort = listenPort;
        this.running = new AtomicBoolean(false);
    }

    /**
     * Changes the ready listener object
     *
     * @param readyListener observer object
     */
    public void setReadyListener(@NonNull Observer<Boolean> readyListener) {
        this.readyListener = readyListener;
    }

    /**
     * Starts the socket server.
     */
    public void start() {
        if (running.compareAndSet(false, true)) {
            new Thread(this).start();
            return;
        }
        throw new RuntimeException("Server: already running");
    }

    /**
     * Stops the socket server
     */
    public void stop() {
        if (running.compareAndSet(true, false)) {
            try {
                serverSocket.close();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns the the server listen port.
     *
     * @return port as Integer
     */
    public int getListenPort() {
        return serverSocket.getLocalPort();
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(userListenPort);
            activityComm.info("Socket server listening on port " + getListenPort());
            readyListener.onEvent(true);

            while (running.get()) {
                Socket client = serverSocket.accept();
                WifiSocketComm socketComm = new WifiSocketComm(activityComm, client);
                socketComm.connect(success -> {
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (running.get())
                activityComm.error("Socket Server: " + e.getMessage());
        } finally {
            stop();
            running.set(false);
            activityComm.warning("Server has stopped");
        }
    }
}
