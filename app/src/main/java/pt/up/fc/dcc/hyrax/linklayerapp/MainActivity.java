/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.Locale;

import pt.up.fc.dcc.hyrax.linklayerapp.experiment.AdbBroadcast;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityHelper;


public class MainActivity extends AppCompatActivity {
    private static final String WHICH_ACTIVITY = "android.hyrax.which_activity";
    public static final String START_LINK_SERVICE = "android.hyrax.start_link_service";
    public static final String EXPERIMENT = "android.hyrax.experiment";
    public static final String LOG_FILE_NAME = "android.hyrax.log_file_name";

    //available activities
    private static final String BLUETOOTH = "bluetooth_activity";
    private static final String BLUETOOTH_LE = "bluetooth_le_activity";
    private static final String WIFI_P2P = "wifi_p2p_activity";
    private static final String WIFI = "wifi_activity";
    private static final String MOBILE = "mobile_activity";
    private boolean started = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onStart() {
        super.onStart();

        String activity = getIntent().getStringExtra(WHICH_ACTIVITY);
        if (!started && activity != null) {
            switch (activity) {
                case BLUETOOTH:
                    startActivity(BluetoothActivity.class, false);
                    break;
                case BLUETOOTH_LE:
                    startActivity(BluetoothLeActivity.class, false);
                    break;
                case WIFI_P2P:
                    startActivity(WifiDirectActivity.class, false);
                    break;
                case WIFI:
                    startActivity(WifiActivity.class, false);
                    break;
                case MOBILE:
                    startActivity(MobileActivity.class, false);
                    break;
                default:
                    throw new RuntimeException("Undefined activity " + activity);
            }
        }
        started = true;
    }

    public void runBluetooth(View v) {
        String[] options = {"None",
                AdbBroadcast.EXP_IDLE_SERVER, AdbBroadcast.EXP_IDLE,
                AdbBroadcast.EXP_IDLE_BLUETOOTH_SERVER, AdbBroadcast.EXP_IDLE_BLUETOOTH,
                AdbBroadcast.EXP_BLUETOOTH_SERVER, AdbBroadcast.EXP_BLUETOOTH};
        String[] logs = {"None",
                "idle_server", "idle_client",
                "idle_bluetooth_server", "idle_bluetooth_client",
                "bluetooth_server", "bluetooth_client"};
        startActivityExperiment(BluetoothActivity.class, options, logs);
        //startActivity(BluetoothActivity.class, true);
    }

    public void runBluetoothLe(View v) {
        String[] options = {"None",
                AdbBroadcast.EXP_BLUETOOTH_LE_SERVER, AdbBroadcast.EXP_BLUETOOTH_LE};
        String[] logs = {"None",
                "bluetooth_le_server", "bluetooth_le_client"};
        startActivityExperiment(BluetoothLeActivity.class, options, logs);
        //startActivity(BluetoothLeActivity.class, true);
    }

    public void runWifiP2p(View v) {
        String[] options = {"None",
                AdbBroadcast.EXP_WIFI_DIRECT_SERVER, AdbBroadcast.EXP_WIFI_DIRECT,
                AdbBroadcast.EXP_WIFI_DIRECT_LEGACY_SERVER, AdbBroadcast.EXP_WIFI_DIRECT_LEGACY};
        String[] logs = {"None",
                "wifi_direct_server", "wifi_direct_client",
                "wifi_direct_legacy_server", "wifi_direct_legacy_client"};
        startActivityExperiment(WifiDirectActivity.class, options, logs);
        //startActivity(WifiDirectActivity.class, true);
    }

    public void runWifi(View v) {
        String[] options = {"None",
                AdbBroadcast.EXP_IDLE_WIFI_SERVER, AdbBroadcast.EXP_IDLE_WIFI,
                AdbBroadcast.EXP_WIFI_SERVER, AdbBroadcast.EXP_WIFI};
        String[] logs = {"None",
                "idle_wifi_server", "idle_wifi_client",
                "wifi_server", "wifi_client"};
        startActivityExperiment(WifiActivity.class, options, logs);
        //startActivity(WifiActivity.class, true);
    }

    public void runMobile(View v) {
        startActivity(MobileActivity.class, true);
    }

    /**
     * Starts an arbitrary activity.
     *
     * @param act        activity object
     * @param start_link <tt>true</tt> if to start link, <tt>false</tt> otherwise
     */
    private void startActivity(@NonNull Class act, boolean start_link) {
        Intent intent = new Intent(this, act);
        intent.putExtra(START_LINK_SERVICE, start_link);
        startActivity(intent);
    }

    /**
     * Starts an arbitrary activity in order to run a manual experiment.
     *
     * @param act     activity object
     * @param options experiment options
     * @param logs    array of logs name files
     */
    private void startActivityExperiment(@NonNull Class act,
                                         @NonNull String[] options,
                                         @NonNull String[] logs) {
        String[] exps = new String[16];
        for (int i = 0; i < 16; i++)
            exps[i] = "Repetition " + (i + 1);
        //Selects the experiment
        ActivityHelper.showOptionsDialog(this, "Select the experiment", options,
                whichExp -> {
                    if (whichExp == 0) {
                        startActivity(act, true);
                        return;
                    }
                    //Selects the repetition number
                    ActivityHelper.showOptionsDialog(this, "Select the repetition", exps,
                            whichRep -> {
                                Intent intent = new Intent(MainActivity.this, act);
                                intent.putExtra(START_LINK_SERVICE, false);
                                intent.putExtra(EXPERIMENT, options[whichExp]);
                                intent.putExtra(LOG_FILE_NAME, String.format(Locale.ENGLISH,
                                        "%s_%d.log", logs[whichExp], whichRep + 1));
                                startActivity(intent);
                            });
                });
    }
}
