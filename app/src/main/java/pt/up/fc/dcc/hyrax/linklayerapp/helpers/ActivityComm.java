/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.support.annotation.NonNull;

/**
 * Activity Ui interaction interface.
 */
public interface ActivityComm {
    /**
     * Execution link modes
     */
    enum Mode {
        SYNC,
        ASYNC,
        PROMISE
    }

    /**
     * Enum color definition.
     */
    enum Color {
        BLUE_GREY_600("#546E7A"),
        GREY_800("#424242"),
        RED_300("#E57373"),
        GREEN_300("#81C784"),
        YELLOW_300("#FFF176");

        public final String hex;

        Color(String hex) {
            this.hex = hex;
        }
    }

    /**
     * Button interaction definition.
     */
    enum Button {
        //hardware button ON/OFF
        HARDWARE,
        //discovery button ON/OFF and number of active scanners
        DISCOVERY,
        //visibility button ON/OFF and number of active advertisers
        VISIBILITY,
        //connection button ON/OFF and number of active connections
        CONNECTION,
        //acceptance button ON/OFF and number of active clients
        ACCEPTANCE;

        /**
         * Buttons states.
         */
        enum State {
            ON,
            OFF
        }
    }

    /**
     * Sends a info message.
     *
     * @param message string message.
     */
    default void info(@NonNull String message) {
        message(message, Color.BLUE_GREY_600);
    }

    /**
     * Sends a debug message.
     *
     * @param message string message
     */
    default void debug(@NonNull String message) {
        message(message, Color.GREY_800);
    }

    /**
     * Sends a warning message.
     *
     * @param message string message
     */
    default void warning(@NonNull String message) {
        message(message, Color.YELLOW_300);
    }

    /**
     * Sends an error message.
     *
     * @param message string message
     */
    default void error(@NonNull String message) {
        message(message, Color.RED_300);
    }

    /**
     * Sends a success message.
     *
     * @param message string message
     */
    default void success(@NonNull String message) {
        message(message, Color.GREEN_300);
    }

    /**
     * Sends a message to activity screen.
     *
     * @param message string message
     * @param color   color object
     */
    void message(@NonNull String message, @NonNull Color color);

    /**
     * Sends a toast message to screen. Long duration.
     *
     * @param message message to be shown
     */
    void toastLong(@NonNull String message);

    /**
     * Sends a toast message to screen. Short duration.
     *
     * @param message message to be shown
     */
    void toastShort(@NonNull String message);

    /**
     * Toggles the ui button state
     *
     * @param button enum button type
     * @param state  new button enum state
     * @param count  an integer counter
     */
    void toggleButton(@NonNull Button button, @NonNull Button.State state, int count);

    /**
     * Toggles the ui execution mode button state.
     *
     * @param mode enum mode type
     */
    void toggleButtonExec(@NonNull Mode mode);
}
