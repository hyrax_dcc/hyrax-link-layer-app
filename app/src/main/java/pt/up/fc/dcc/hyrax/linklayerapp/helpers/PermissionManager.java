/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Application permissions manager.
 */
public class PermissionManager {
    //permission to scan/advertise to nearby devices
    public static final int PERMISSION_LOCATION = 1;
    //permission to write on disk
    public static final int PERMISSION_WRITE_STORAGE = 2;

    @NonNull
    private final Activity activity;
    @NonNull
    private SparseArray<ActivityHelper.Result<Boolean>> callbackMap;

    /**
     * Constructor.
     *
     * @param activity app activity
     */
    PermissionManager(@NonNull Activity activity) {
        this.activity = activity;
        this.callbackMap = new SparseArray<>();
    }

    /**
     * Request for necessary permission.
     *
     * @param localPerm group permission to request
     * @param callback  callback boolean result (granted or not)
     */
    public void requestPermission(int localPerm, @NonNull ActivityHelper.Result<Boolean> callback) {
        List<String> permList = new ArrayList<>();
        switch (localPerm) {
            case PERMISSION_LOCATION:
                permList.add(Manifest.permission.ACCESS_FINE_LOCATION);
                permList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
                break;
            case PERMISSION_WRITE_STORAGE:
                permList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                break;
            default:
                throw new RuntimeException("No permission defined " + localPerm);
        }

        String[] perms = permList.toArray(new String[permList.size()]);

        callbackMap.put(localPerm, callback);
        //requests permission through SO
        ActivityCompat.requestPermissions(activity, perms, localPerm);
    }

    /**
     * Notify permissions result. This method must be called on activity in result of onRequestPermissionsResult.
     *
     * @param localPerm    local permission identifier.
     * @param permissions  an array of requested permissions
     * @param grantResults an array of grant results
     */
    @MainThread
    public void onPermissionResult(int localPerm, String[] permissions, int[] grantResults) {
        ActivityHelper.Result<Boolean> callback = callbackMap.get(localPerm);
        if (callback == null)
            return;
        callbackMap.delete(localPerm);

        for (int g : grantResults)
            if (g == PackageManager.PERMISSION_DENIED) {
                callback.onResult(false);
                return;
            }
        callback.onResult(true);
    }
}
