/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.experiment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.misc.ScanData;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.outcome.Error;
import org.hyrax.link.misc.outcome.Success;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.wifi.WifiLink;
import org.hyrax.link.wifi.types.ScanDataWifi;
import org.hyrax.link.wifi.types.WifiScanFilter;

import java.util.Collection;
import java.util.Objects;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkController;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.SocketComm;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketComm;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketServer;

/**
 * Wifi experiment.
 */
public class WifiExperiment extends Experiment<WifiLink> {
    private static final String WIFI_SERVICE = "wifi_service";
    @Nullable
    private WifiSocketServer socketServer;
    @Nullable
    private ScanDataWifi scanDataWifi;
    @Nullable
    private WifiSocketComm socketComm;

    /**
     * Constructor.
     *
     * @param linkHelper link helper object
     * @param server     <tt>true</tt> if server, <tt>false</tt> otherwise
     */
    public WifiExperiment(@NonNull LinkHelper<WifiLink> linkHelper, boolean server) {
        super(linkHelper, server);
        if (server)
            this.socketServer = new WifiSocketServer(linkHelper.getActivityComm());
    }

    @Override
    ServerProperties getServerProperties() {
        return linkHelper.getLink().getFeatures().newServerBuilder().build();
    }

    @Override
    VisibilityProperties getVisibilityProperties() {
        Objects.requireNonNull(socketServer);
        return linkHelper.getLink().getFeatures()
                .newVisibilityBuilder(WIFI_SERVICE, socketServer.getListenPort()).build();
    }

    @Override
    DiscoveryProperties getDiscoveryProperties() {
        return linkHelper.getLink().getFeatures()
                .newDiscoveryBuilder()
                .setScannerFilter(
                        WifiScanFilter.newBuilder()
                                .setFilter(device -> "Tomato".equals(device.getName()))
                                .build())
                .build();
    }

    @Override
    ConnectionProperties getConnectionProperties(@NonNull Device device) {
        return linkHelper.getLink()
                .getFeatures()
                .newConnectionBuilder(device)
                .setPassword("hyraxCmu2014")
                .removeWhenDisconnect(true)
                .build();
    }

    @Override
    SocketComm getSocketObject(@NonNull Device device) {
        Objects.requireNonNull(scanDataWifi);
        String host = scanDataWifi.getOriginalObject().getHost();
        int port = scanDataWifi.getOriginalObject().getPort();
        this.socketComm = new WifiSocketComm(linkHelper.getActivityComm(), host, port);
        return this.socketComm;
    }

    @Override
    public void finish() {
        if (socketComm != null)
            socketComm.disconnect();
        if (socketServer != null)
            socketServer.stop();

        Collection<Device> devices = linkHelper.getLink().getConnected();
        for (Device device : devices)
            linkHelper.toggleConnection(device, getConnectionProperties(device));
    }

    @Override
    void connect(@NonNull Device device, @NonNull ConnectionProperties connectionProperties,
                 @NonNull LinkController.ActionResult result) {
        DiscoveryProperties<Void, WifiScanFilter> dis = linkHelper.getLink().getFeatures()
                .newDiscoveryBuilder(WIFI_SERVICE)
                .setStopRule(devices -> devices.size() > 0)
                .build();
        linkHelper.toggleConnection(device, connectionProperties, connOut -> {
            //server side
            if (server) {
                result.onResult(connOut);
                return;
            }
            //client side
            //if connection fail - ERROR
            connOut.ifError(error -> result.onResult(connOut));
            connOut.ifSuccess(connDevice -> {
                //going discovery for services
                linkHelper.toggleDiscovery(dis, disOut -> {
                    //if discovery error fail - ERROR
                    disOut.ifError(error -> result.onResult(disOut));
                    disOut.ifSuccess(rawDevices -> {
                        Collection<Device> devices = (Collection<Device>) rawDevices;
                        //if no devices discovered - ERROR
                        if (devices.size() == 0) {
                            result.onResult(new Error<>(
                                    LinkException.build(new Exception("No devices discovered")), ""));
                            return;
                        }
                        //get the first device
                        Device deviceService = devices.iterator().next();
                        Collection<ScanData> scanData = deviceService.getScanData();
                        if (scanData.size() == 0) {
                            result.onResult(new Error<>(
                                    LinkException.build(new Exception("Empty scan data")), ""));
                            return;
                        }
                        this.scanDataWifi = (ScanDataWifi) scanData.iterator().next();
                        //success!!
                        result.onResult(connOut);
                    });//closes service discovery success
                });//closes discovery
            });//closes connection success
        });//closes toggle connection
    }

    @Override
    void executeServer(@NonNull DoneCallback doneCallback) {
        discoverAndConnect(connOut -> {
            //if wifi connection fails - ERROR
            connOut.ifError(error -> doneCallback.onDone(connOut));
            connOut.ifSuccess(device -> {
                Link link = linkHelper.getLink();
                Objects.requireNonNull(socketServer);

                socketServer.setReadyListener(ready -> {
                    VisibilityProperties visibilityProperties = getVisibilityProperties();
                    //if the hardware is visible - ERROR
                    if (link.isVisible(visibilityProperties.getIdentifier())) {
                        doneCallback.onDone(new Error<>(
                                LinkException.build(new Exception("Already visible")), ""));
                        return;
                    }
                    linkHelper.toggleVisibility(visibilityProperties, visOut -> {
                        //if visibility fails - ERROR
                        visOut.ifError(error -> doneCallback.onDone(visOut));
                        visOut.ifSuccess(id -> doneCallback.onDone(new Success<>("Success!")));
                    });//closes visibility
                });
                socketServer.start();
            });
        });
    }
}
