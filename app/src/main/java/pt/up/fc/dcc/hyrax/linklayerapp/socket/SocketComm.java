/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.socket;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Observer;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;
import java.util.Random;

import pt.up.fc.dcc.hyrax.linklayerapp.experiment.Experiment;

/**
 * Interface that defined general communication patterns.
 */
public interface SocketComm {
    int CHUNK_SIZE = 1024;

    /**
     * Message type identifier.
     */
    enum Type {
        PING((byte) 1),
        PONG((byte) 2);

        private byte id;

        /**
         * Constructor.
         *
         * @param id byte identifier
         */
        Type(byte id) {
            this.id = id;
        }

        /**
         * Returns the byte identifier
         *
         * @return byte identifier
         */
        public byte getId() {
            return id;
        }

        /**
         * Returns a type from given a byte
         *
         * @param b byte id
         * @return the corresponding type or NULL of byte not exists
         */
        @Nullable
        public static Type toType(byte b) {
            for (Type t : Type.values()) {
                if (t.id == b)
                    return t;
            }
            return null;
        }
    }

    /**
     * Establish a communication link with remote device.
     *
     * @param listener result listener
     */
    void connect(@NonNull Observer<Boolean> listener);

    /**
     * Disconnects the communication link.
     */
    void disconnect();

    /**
     * Sends a ping message with payload.
     * The payload is randomly generated
     *
     * @param payloadSize payload size
     */
    default void sendPing(int payloadSize) {
        sendMessage(Type.PING, payloadSize);
    }

    /**
     * Sends multiple pings.
     *
     * @param number number of pings
     */
    void sendPingMultiple(int number);
    /**
     * Sends a message.
     * The payload is randomly generated
     *
     * @param type        type of message.
     * @param payloadSize payload size
     */
    void sendMessage(@NonNull Type type, int payloadSize);

    /**
     * Sends a message.
     * The payload is randomly generated
     *
     * @param type        type of message.
     * @param payloadSize payload size
     * @param listener    response listener
     */
    void sendMessage(@NonNull Type type, int payloadSize, @NonNull Observer<Type> listener);

    /**
     * Writes an action and it's payload to an output stream
     *
     * @param outputStream stream to be written
     * @param msgId        message unique identifier
     * @param action       byte action
     * @param payloadSize  action payload
     * @throws IOException if the write fails
     */
    default void write(@NonNull OutputStream outputStream, int msgId, byte action, int payloadSize) throws IOException {
        DataOutputStream out = new DataOutputStream(outputStream);
        //writes message unique identifier
        out.writeInt(msgId);
        //writes action
        out.writeByte(action);
        //writes payload size
        out.writeInt(payloadSize);

        //sends random payload
        int remain = payloadSize;
        Random r = new Random();
        while (remain > 0) {
            //buffer len
            int len = Math.min(remain, CHUNK_SIZE);
            //buffer
            byte[] b = new byte[len];
            //generating random buffer bytes
            r.nextBytes(b);
            //write buffer
            outputStream.write(b);
            remain -= len;
        }
    }

    /**
     * Reads from input stream
     *
     * @param inputStream stream to read
     * @param payloadSize among of payload to be read
     * @throws IOException if the read fails
     */
    default void read(@NonNull InputStream inputStream, int payloadSize) throws IOException {
        int read;
        int remain = payloadSize;
        byte[] buffer = new byte[CHUNK_SIZE];
        while (remain > 0 && (read = inputStream.read(buffer, 0, CHUNK_SIZE)) != -1) {
            remain -= read;
        }
    }

    /**
     * Converts time in milliseconds to a more friendly string representation.
     *
     * @param milliseconds time in milliseconds
     * @return a formatted time string
     */
    static String timeToString(long milliseconds) {
        long second = 1000; //one second is 1000 ms
        long minute = second * 60; //one minute is 60s
        if (milliseconds >= minute) {
            long m = milliseconds / minute;
            long s = (milliseconds - (m * minute)) / second;
            long ms = milliseconds - (m * minute) - (s * second);
            return String.format(Locale.ENGLISH, "%dm:%ds:%dms", m, s, ms);

        } else if (milliseconds >= second) {
            long s = milliseconds / second;
            long ms = milliseconds - (s * second);
            return String.format(Locale.ENGLISH, "%ds:%dms", s, ms);
        }
        return String.format(Locale.ENGLISH, "%dms", milliseconds);
    }

    /**
     * Converts size in bytes to a more friend string representation.
     *
     * @param lenBytes size in bytes
     * @return a formatted size string
     */
    static String sizeToString(int lenBytes) {
        int kb = 1000; //1KB is 1000 bytes
        int mb = kb * kb; //1MB is 1000KB and 1000000 bytes
        if (lenBytes >= mb)
            return String.format(Locale.ENGLISH, "%.3f MB", (float) lenBytes / mb);
        else if (lenBytes >= kb)
            return String.format(Locale.ENGLISH, "%.3f KB", (float) lenBytes / kb);
        return String.format(Locale.ENGLISH, "%d bytes", lenBytes);
    }

    /**
     * Calculates the speed given the amount of bytes transferred and the time it took.
     *
     * @param lenBytes number of bytes transferred
     * @param time     time taken
     * @return the speed per second
     */
    static int getSpeedPerSecond(int lenBytes, long time) {
        long total = lenBytes * 1000;
        return (int) (total / (int) time);
    }

    /**
     * Returns the speed into a string format.
     *
     * @param bytesPerSecond bytes transferred per second
     * @return speed string representation
     */
    static String speedToString(int bytesPerSecond) {
        return sizeToString(bytesPerSecond) + "/s";
    }
}
