/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import org.hyrax.link.Device;
import java.util.Locale;

/**
 * General activity actions helper
 */
public class ActivityHelper {
    /**
     * General result callback interface for asynchronous actions.
     *
     * @param <A> type of argument result
     */
    public interface Result<A> {
        void onResult(A result);
    }

    /**
     * Shows a dialog with a view to the screen.
     *
     * @param activity app activity
     * @param layout   layout resource
     */
    public static void showDialogWithView(@NonNull Activity activity, @LayoutRes int layout) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater layoutInflater = activity.getLayoutInflater();

        View customView = layoutInflater.inflate(layout, null);

        builder.setView(customView)
                .create()
                .show();
    }

    /**
     * Shows a dialog with a view to the screen.
     *
     * @param activity app activity
     * @param layout   layout resource
     * @param title    dialog title
     * @param result   callback result
     */
    public static void showDialogWithView(@NonNull Activity activity, @LayoutRes int layout,
                                          @NonNull String title,
                                          @NonNull Result<View> result) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater layoutInflater = activity.getLayoutInflater();

        View customView = layoutInflater.inflate(layout, null);

        builder.setView(customView)
                .setTitle(title)
                .setPositiveButton("Ok", (dialog, which) -> result.onResult(customView))
                .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                .create()
                .show();
    }

    /**
     * Shows an option dialog to the screen.
     *
     * @param activity app activity
     * @param title    dialog title
     * @param options  dialog options
     * @param result   callback result
     */
    public static void showOptionsDialog(@NonNull Activity activity, @NonNull String title,
                                         @NonNull String[] options, @NonNull Result<Integer> result) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title)
                .setItems(options, (dialog, which) -> {
                    // The 'which' argument contains the index position
                    // of the selected item
                    result.onResult(which);
                });
        builder.create().show();
    }

    /**
     * Transforms a collection of devices to string array representation.
     *
     * @param devices collection of devices
     * @return string array
     */
    public static String[] devicesToStr(@NonNull Device[] devices) {
        String[] str = new String[devices.length];

        for (int i = 0; i < devices.length; i++)
            str[i] = String.format(Locale.ENGLISH, "%s (%s)",
                    devices[i].getUniqueAddress(), devices[i].getName());
        return str;
    }
}
