/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.socket;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.bluetooth.BluetoothLeLink;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityComm;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;

/**
 * Bluetooth low energy server manager.
 */
public class BluetoothLeServer extends BluetoothGattServerCallback {
    public static final int HEADER_SIZE = 9; // 9 bytes
    public static final int MTU = 512;
    public static final UUID SERVICE_ID = BluetoothLeServer.convertFromInteger(0x2109);
    public static final UUID MESSAGE_CHARACTERISTIC = BluetoothLeServer.convertFromInteger(0x24F1);
    public static final UUID BODY_CHARACTERISTIC = BluetoothLeServer.convertFromInteger(0x254E);

    private final LinkHelper<BluetoothLeLink> linkHelper;
    private final ActivityComm comm;

    private final BluetoothGattService aService;
    private Receiver dataReceiver;

    /**
     * Constructor.
     *
     * @param linkHelper link helper controller
     */
    public BluetoothLeServer(@NonNull LinkHelper<BluetoothLeLink> linkHelper) {
        this.linkHelper = linkHelper;
        this.comm = linkHelper.getActivityComm();
        this.aService = new BluetoothGattService(
                SERVICE_ID,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        BluetoothGattCharacteristic msgCharacteristic = new BluetoothGattCharacteristic(
                MESSAGE_CHARACTERISTIC,
                BluetoothGattCharacteristic.PROPERTY_WRITE,
                BluetoothGattCharacteristic.PERMISSION_WRITE);
        BluetoothGattCharacteristic bodyDescriptor = new BluetoothGattCharacteristic(
                BODY_CHARACTERISTIC,
                BluetoothGattCharacteristic.PROPERTY_WRITE,
                BluetoothGattCharacteristic.PERMISSION_WRITE);

        aService.addCharacteristic(msgCharacteristic);
        aService.addCharacteristic(bodyDescriptor);
    }

    /**
     * Return a bluetooth gatt service.
     *
     * @return gatt service object
     */
    @NonNull
    public BluetoothGattService getGattService() {
        return aService;
    }

    /**
     * Returns a gatt server
     *
     * @return bluetooth gatt server object
     */
    @Nullable
    private BluetoothGattServer getGattServer() {
        Collection<String> accepted = linkHelper.getLink().getAccepted();
        if (accepted.size() > 0) {
            String id = accepted.iterator().next();
            return linkHelper.getLink().getFeatures().getBluetoothGattServer(id);
        }
        return null;
    }

    @Override
    public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
        comm.debug(String.format(Locale.ENGLISH, "Server: server state %d", newState));
    }

    @Override
    public void onServiceAdded(int status, BluetoothGattService s) {
        comm.debug(String.format(Locale.ENGLISH, "Server: service added %s -> %d",
                s.getUuid().toString(), status));
        BluetoothGattServer server = getGattServer();
        Objects.requireNonNull(server);
        for (BluetoothGattService service : getGattServer().getServices()) {
            comm.debug(service.getUuid().toString());
            for (BluetoothGattCharacteristic characteristic : service.getCharacteristics())
                comm.debug("---------> " + characteristic.getUuid().toString());
        }
    }

    @Override
    public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset,
                                            BluetoothGattCharacteristic characteristic) {
        comm.debug(String.format(Locale.ENGLISH,
                "Server: characteristic read req %s -> Id: %d Off: %d",
                characteristic.getUuid().toString(), requestId, offset));
    }

    @Override
    public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId,
                                             BluetoothGattCharacteristic characteristic,
                                             boolean preparedWrite, boolean responseNeeded,
                                             int offset, byte[] value) {
        /*comm.debug(String.format(Locale.ENGLISH,
                "Server: characteristic write req %s -> Id: %d Off: %d || pW: %s resp: %s",
                characteristic.getUuid().toString(), requestId, offset, Boolean.toString(preparedWrite),
                Boolean.toString(responseNeeded)));*/

        BluetoothGattServer gattServer = getGattServer();
        Objects.requireNonNull(gattServer);
        gattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, null);
        if (MESSAGE_CHARACTERISTIC.equals(characteristic.getUuid())) {

            if (value.length == HEADER_SIZE) {
                int msgId = ByteBuffer.wrap(value, 0, 4).getInt();
                //byte msgType = value[4];
                int msgSize = ByteBuffer.wrap(value, 5, 4).getInt();

                dataReceiver = new Receiver(msgId, msgSize);
                if (msgSize == 0) {
                    characteristic.setValue(dataReceiver.buildResponse(SocketComm.Type.PONG));
                    gattServer.notifyCharacteristicChanged(device, characteristic, false);
                    dataReceiver = null;
                }

            } else {
                comm.error("Message must have an header of 9 bytes");
            }
        } else if (BODY_CHARACTERISTIC.equals(characteristic.getUuid())) {
            if (dataReceiver != null) {
                dataReceiver.onRead(value);
                if (dataReceiver.isComplete()) {
                    characteristic.setValue(dataReceiver.buildResponse(SocketComm.Type.PONG));
                    gattServer.notifyCharacteristicChanged(device, characteristic, false);
                    dataReceiver = null;
                }
            }
        }
    }

    @Override
    public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset,
                                        BluetoothGattDescriptor descriptor) {
        comm.debug(String.format(Locale.ENGLISH,
                "Server: descriptor read req %s -> Id: %d Off: %d",
                descriptor.getUuid().toString(), requestId, offset));
    }

    @Override
    public void onDescriptorWriteRequest(BluetoothDevice device, int requestId,
                                         BluetoothGattDescriptor descriptor, boolean preparedWrite,
                                         boolean responseNeeded, int offset, byte[] value) {
        comm.debug(String.format(Locale.ENGLISH,
                "Server: descriptor write req %s -> Id: %d Off: %d || pW: %s resp: %s",
                descriptor.getUuid().toString(), requestId, offset, Boolean.toString(preparedWrite),
                Boolean.toString(responseNeeded)));
    }

    @Override
    public void onExecuteWrite(BluetoothDevice device, int requestId, boolean execute) {
        comm.debug(String.format(Locale.ENGLISH,
                "Server: Execute Write Id: %d Exec: %s", requestId, Boolean.toString(execute)));
    }

    @Override
    public void onNotificationSent(BluetoothDevice device, int status) {
        comm.debug(String.format(Locale.ENGLISH,
                "Server: Notification Sent %d ", status));
    }

    @Override
    public void onMtuChanged(BluetoothDevice device, int mtu) {
        comm.debug(String.format(Locale.ENGLISH, "Server: Mtu %d", mtu));
    }

    /**
     * Converts an Integer to UUID object.
     *
     * @param i integer to convert to
     * @return an UUID object
     */
    private static UUID convertFromInteger(int i) {
        final long MSB = 0x0000000000001000L;
        final long LSB = 0x800000805f9b34fbL;
        return new UUID(MSB | ((long) i << 32), LSB);
    }


    /**
     * Wrapper data receiver class
     */
    private class Receiver {
        private final int msgId;
        private final int msgSize;
        private int read;

        /**
         * Constructor.
         *
         * @param msgId   message identifier
         * @param msgSize message remaining length
         */
        private Receiver(int msgId, int msgSize) {
            this.msgId = msgId;
            this.msgSize = msgSize;
            this.read = 0;
        }

        /**
         * Reads the bytes
         *
         * @param bytes bytes to be read
         */
        private void onRead(byte[] bytes) {
            read += bytes.length;
        }

        /**
         * Verify if the message is completed. All read.
         *
         * @return <tt>true</tt> if completed, <tt>false</tt> otherwise
         */
        private boolean isComplete() {
            return read >= msgSize;
        }

        /**
         * Build a pong response.
         *
         * @return byte data array
         */
        private byte[] buildResponse(@NonNull SocketComm.Type type) {
            ByteBuffer buffer = ByteBuffer.allocate(HEADER_SIZE);
            buffer.putInt(0, msgId);
            buffer.put(4, type.getId());
            buffer.putInt(5, 0);
            return buffer.array();
        }
    }
}
