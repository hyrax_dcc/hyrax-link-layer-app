/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp;

import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.hyrax.link.bluetooth.types.BluetoothLeServerSettings;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.Device;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.bluetooth.BluetoothLeLink;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;

import pt.up.fc.dcc.hyrax.linklayerapp.experiment.AdbBroadcast;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.AdbBroadcastListener;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.BluetoothLeExperiment;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.Experiment;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.HandlerHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkUiActionsListener;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.PermissionManager;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.BluetoothLeClient;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.BluetoothLeServer;

public class BluetoothLeActivity extends AppCompatActivity implements LinkUiActionsListener, AdbBroadcastListener {
    //activity helper
    private HandlerHelper helper;
    //Bluetooth link controller
    private LinkHelper<BluetoothLeLink> controller;
    //adb commands receiver
    private AdbBroadcast adbBroadcast;
    @Nullable
    private BluetoothLeClient leClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_le);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.helper = new HandlerHelper(this);
        this.controller = new LinkHelper<>(this, helper, Technology.BLUETOOTH_LE);
        this.adbBroadcast = new AdbBroadcast(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bluetooth_le, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_WRITE_STORAGE,
                granted -> {
                    if (!granted) {
                        helper.error("Need write permissions to log");
                        helper.toastLong("Permissions are needed in order to log traces");
                    }
                });

        boolean toStart = getIntent().getBooleanExtra(
                MainActivity.START_LINK_SERVICE, false);
        if (toStart) {
            //enables logger
            controller.enableLog(AdbBroadcast.BASE_PATH + "app.log");
            //starts the controller
            controller.start();
        }
        //TODO - remove
        String exp = getIntent().getStringExtra(MainActivity.EXPERIMENT);
        String log = getIntent().getStringExtra(MainActivity.LOG_FILE_NAME);
        if (exp != null && log != null) {
            onExperimentStart(exp, log);
        }
        //register broadcast receiver
        registerReceiver(adbBroadcast, adbBroadcast.getIntentFilters());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        helper.getPermissionManager().onPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onStop() {
        super.onStop();
        controller.stop();
        //unregisters broadcast receiver
        unregisterReceiver(adbBroadcast);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller.stop();
    }

    @Override
    public void onActionAbout(MenuItem menuItem) {
        ActivityHelper.showDialogWithView(this, R.layout.about_bluetooth);
    }

    @Override
    public void onActionStop(MenuItem menuItem) {
        //TODO - to remove if
        if (experiment != null) {
            onExperimentStop();
            return;
        }
        finishAndRemoveTask();
    }

    @Override
    public void onActionAuto(View view) {
        ArrayList<Device> devices = new ArrayList<>(controller.getLink().getConnected());
        if (devices.isEmpty() || leClient == null) {
            helper.warning("No devices connected or is not client");
            return;
        }
        Objects.requireNonNull(leClient);
        String[] options = {"Ping", "Ping - Multiple"};
        ActivityHelper.showOptionsDialog(this, "Select Action", options,
                result -> {
                    switch (result) {
                        case 0:
                            ActivityHelper.showDialogWithView(BluetoothLeActivity.this,
                                    R.layout.ping_select, "Enter ping payload size",
                                    dialogView -> {
                                        EditText text = dialogView.findViewById(R.id.payload_size);
                                        String value = text.getText().toString();
                                        leClient.sendPing(Integer.valueOf(value));
                                    });
                            break;
                        case 1:
                            ActivityHelper.showDialogWithView(BluetoothLeActivity.this,
                                    R.layout.ping_select, "Enter the number of pings",
                                    dialogView -> {
                                        EditText text = dialogView.findViewById(R.id.payload_size);
                                        String value = text.getText().toString();
                                        leClient.sendPingMultiple(Integer.valueOf(value));
                                    });
                            break;
                        default:
                            throw new RuntimeException("Undefined test " + result);
                    }
                });
    }

    @Override
    public void onActionExecMode(View view) {
        controller.toggleMode();
    }

    @Override
    public void onActionHardware(View view) {
        controller.toggleHardware();
    }

    @Override
    public void onActionDiscovery(View view) {
        String[] options = {"Default", "Scanner Low Power", "Scanner High Power"};
        ActivityHelper.showOptionsDialog(this, "Discover Options", options,
                opt -> {
                    DiscoveryProperties<ScanSettings, ArrayList<ScanFilter>> properties;
                    switch (opt) {
                        case 0:
                            properties = controller.getLink().getFeatures()
                                    .newDiscoveryBuilder().build();
                            break;
                        case 1:
                            ScanSettings settings = new ScanSettings.Builder()
                                    .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                                    .build();
                            ArrayList<ScanFilter> filter = new ArrayList<ScanFilter>() {{
                                add(new ScanFilter.Builder()
                                        .setDeviceName("Nexus 9")
                                        .build());
                            }};
                            properties = controller.getLink().getFeatures()
                                    .newDiscoveryBuilder(settings, filter)
                                    .stopAfterTimeoutExpiration(true)
                                    .setStopRule(devices -> devices.size() > 0)
                                    .build();
                            break;
                        case 2:
                            settings = new ScanSettings.Builder()
                                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                                    .build();
                            filter = new ArrayList<ScanFilter>() {{
                                add(new ScanFilter.Builder().build());
                            }};
                            properties = controller.getLink().getFeatures()
                                    .newDiscoveryBuilder(settings, filter)
                                    .stopAfterTimeoutExpiration(true)
                                    .setStopRule(devices -> devices.size() > 0)
                                    .build();
                            break;
                        default:
                            throw new RuntimeException("Undefined option " + opt);
                    }
                    controller.toggleDiscovery(properties);
                }
        );
    }

    @Override
    public void onActionVisibility(View view) {
        String[] options = {"Default", "Advertiser Low Power", "Advertiser High Power"};
        ActivityHelper.showOptionsDialog(this, "Visible Options", options,
                opt -> {
                    VisibilityProperties<AdvertiseSettings, AdvertiseData> properties;
                    switch (opt) {
                        case 0:
                            properties = controller.getLink().getFeatures()
                                    .newVisibilityBuilder().build();
                            break;
                        case 1:
                            AdvertiseSettings settings = new AdvertiseSettings.Builder()
                                    .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER)
                                    .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_LOW)
                                    .build();
                            AdvertiseData data = new AdvertiseData.Builder()
                                    .setIncludeDeviceName(true)
                                    .setIncludeTxPowerLevel(false)
                                    .addServiceUuid(new ParcelUuid(BluetoothLeServer.SERVICE_ID))
                                    .build();
                            properties = controller.getLink().getFeatures()
                                    .newVisibilityBuilder(settings, data).build();
                            break;
                        case 2:
                            settings = new AdvertiseSettings.Builder()
                                    .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
                                    .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
                                    .build();
                            data = new AdvertiseData.Builder()
                                    .setIncludeDeviceName(true)
                                    .setIncludeTxPowerLevel(false)
                                    .addServiceUuid(new ParcelUuid(BluetoothLeServer.SERVICE_ID))
                                    .build();
                            properties = controller.getLink().getFeatures()
                                    .newVisibilityBuilder(settings, data).build();
                            break;
                        default:
                            throw new RuntimeException("Undefined option " + opt);
                    }
                    controller.toggleVisibility(properties);
                }
        );
    }

    @Override
    public void onActionConnection(View view) {
        //merge discovered devices with connected
        HashSet<Device> devicesHash = new HashSet<>(controller.getDevicesDiscovered());
        devicesHash.addAll(controller.getLink().getConnected());

        Device[] devices = devicesHash.toArray(new Device[devicesHash.size()]);
        if (devices.length == 0) {
            helper.warning("No devices discovered. Empty list devices!!");
            return;
        }
        final String[] devicesOptions = ActivityHelper.devicesToStr(devices);
        ActivityHelper.showOptionsDialog(this, "Select a device", devicesOptions,
                result -> {
                    Device dev = devices[result];
                    leClient = new BluetoothLeClient(controller);
                    ConnectionProperties<BluetoothGattCallback> properties =
                            controller.getLink().getFeatures()
                                    .newConnectionBuilder(dev)
                                    .setSettings(leClient)
                                    .build();
                    controller.toggleConnection(dev, properties);
                });
    }

    @Override
    public void onActionAcceptance(View view) {
        BluetoothLeServer leServer = new BluetoothLeServer(controller);
        ServerProperties<BluetoothLeServerSettings> properties = controller.getLink()
                .getFeatures().newServerBuilder()
                .setSettings(BluetoothLeServerSettings.newBuilder()
                        .setGattService(leServer.getGattService())
                        .setServerCallback(leServer)
                        .build()
                )
                .build();
        controller.toggleAccept(properties);
    }

    @Nullable
    private Experiment experiment;

    @Override
    public void onExperimentStart(@NonNull String exp, @NonNull String logFileName) {
        helper.info(String.format(Locale.ENGLISH, "Starting experiment %s and logging to %s", exp, logFileName));

        //startup
        controller.enableLog(AdbBroadcast.BASE_PATH + logFileName);
        controller.start();

        switch (exp) {
            case AdbBroadcast.EXP_BLUETOOTH_LE_SERVER:
                experiment = new BluetoothLeExperiment(controller, true);
                break;
            case AdbBroadcast.EXP_BLUETOOTH_LE:
                experiment = new BluetoothLeExperiment(controller, false);
                break;
            default:
                throw new RuntimeException("Undefined experiment " + exp);
        }
        experiment.perform(outcome -> {
            outcome.ifError(error -> {
                helper.error(outcome.getError().getMessage());
                adbBroadcast.propertiesManager.setProperties(
                        AdbBroadcast.BARRIER_DONE, Boolean.toString(false));
            });
            outcome.ifSuccess(arg -> {
                helper.success("Experiment successfully done!!");
                adbBroadcast.propertiesManager.setProperties(
                        AdbBroadcast.BARRIER_DONE, Boolean.toString(true));
                //TODO - to remove
                if (AdbBroadcast.EXP_BLUETOOTH_LE.equals(exp))
                    onExperimentStop();
            });
        });
    }

    @Override
    public void onExperimentStop() {
        if (experiment != null)
            experiment.finish();

        controller.toggleHardware(result -> {
            controller.stop();
            adbBroadcast.propertiesManager.close();
            //closes activity
            helper.post(this::finishAndRemoveTask);
        });
    }
}
