/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.hyrax.link.Device;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Technology;
import org.hyrax.link.bluetooth.BluetoothLink;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothComm;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.Filter;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;

import pt.up.fc.dcc.hyrax.linklayerapp.experiment.AdbBroadcast;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.AdbBroadcastListener;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.BluetoothExperiment;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.Experiment;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.HandlerHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkUiActionsListener;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.PermissionManager;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.BluetoothSocketComm;

public class BluetoothActivity extends AppCompatActivity implements LinkUiActionsListener, AdbBroadcastListener {
    //activity helper
    private HandlerHelper helper;
    //Bluetooth link controller
    private LinkHelper<BluetoothLink> controller;
    //adb commands receiver
    private AdbBroadcast adbBroadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.helper = new HandlerHelper(this);
        this.controller = new LinkHelper<>(this, helper, Technology.BLUETOOTH);
        this.adbBroadcast = new AdbBroadcast(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bluetooth, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_WRITE_STORAGE,
                granted -> {
                    if (!granted) {
                        helper.error("Need write permissions to log");
                        helper.toastLong("Permissions are needed in order to log traces");
                    }
                });

        boolean toStart = getIntent().getBooleanExtra(
                MainActivity.START_LINK_SERVICE, false);
        if (toStart) {
            //enables logger
            controller.enableLog(AdbBroadcast.BASE_PATH + "app.log");
            //starts the controller
            controller.start();
            //listen for new connections and instantiates BluetoothSocketComm
            controller.getLink().listen(LinkEvent.LINK_CONNECTION_NEW,
                    (LinkListener<Device>) (eventType, outcome) -> {
                        BluetoothComm comm = controller.getLink().getFeatures().getCommLink(outcome.getOutcome());
                        Objects.requireNonNull(comm);
                        BluetoothSocketComm.getInstance(helper, comm);
                    });
        }
        //TODO - remove
        String exp = getIntent().getStringExtra(MainActivity.EXPERIMENT);
        String log = getIntent().getStringExtra(MainActivity.LOG_FILE_NAME);
        if (exp != null && log != null) {
            onExperimentStart(exp, log);
        }
        //register broadcast receiver
        registerReceiver(adbBroadcast, adbBroadcast.getIntentFilters());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        helper.getPermissionManager().onPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onStop() {
        super.onStop();
        controller.stop();
        //unregisters broadcast receiver
        unregisterReceiver(adbBroadcast);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller.stop();
    }

    @Override
    public void onActionAbout(MenuItem menuItem) {
        ActivityHelper.showDialogWithView(this, R.layout.about_bluetooth);
    }

    @Override
    public void onActionStop(MenuItem menuItem) {
        //TODO - to remove if
        if (experiment != null) {
            onExperimentStop();
            return;
        }

        finishAndRemoveTask();
    }

    @Override
    public void onActionAuto(View view) {
        ArrayList<Device> devices = new ArrayList<>(controller.getLink().getConnected());
        if (devices.isEmpty()) {
            helper.warning("No devices connected");
            return;
        }
        String[] options = {"Ping", "Ping - Multiple"};
        ActivityHelper.showOptionsDialog(this, "Select Action", options,
                result -> {
                    switch (result) {
                        case 0:
                            Device device = devices.get(0);
                            BluetoothComm comm = controller.getLink().getFeatures().getCommLink(device);
                            Objects.requireNonNull(comm);
                            BluetoothSocketComm socketComm = BluetoothSocketComm.getInstance(helper, comm);

                            ActivityHelper.showDialogWithView(BluetoothActivity.this,
                                    R.layout.ping_select, "Enter ping payload size",
                                    dialogView -> {
                                        EditText text = dialogView.findViewById(R.id.payload_size);
                                        String value = text.getText().toString();
                                        socketComm.sendPing(Integer.valueOf(value));
                                    });
                            break;
                        case 1:
                            device = devices.get(0);
                            comm = controller.getLink().getFeatures().getCommLink(device);
                            Objects.requireNonNull(comm);
                            socketComm = BluetoothSocketComm.getInstance(helper, comm);

                            ActivityHelper.showDialogWithView(BluetoothActivity.this,
                                    R.layout.ping_select, "Enter the number of pings",
                                    dialogView -> {
                                        EditText text = dialogView.findViewById(R.id.payload_size);
                                        String value = text.getText().toString();
                                        socketComm.sendPingMultiple(Integer.valueOf(value));
                                    });
                            break;
                        default:
                            throw new RuntimeException("Undefined test " + result);
                    }
                });
    }

    @Override
    public void onActionExecMode(View view) {
        controller.toggleMode();
    }

    @Override
    public void onActionHardware(View view) {
        controller.toggleHardware();
    }

    @Override
    public void onActionDiscovery(View view) {
        helper.info("Only default discovery supported. NO DATA.");
        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_LOCATION,
                granted -> {
                    if (!granted) {
                        helper.error("No enough permissions to continue");
                        return;
                    }
                    String[] options = {"Default", "Filtered+Stop"};
                    ActivityHelper.showOptionsDialog(this, "Select Type", options,
                            result -> {
                                DiscoveryProperties<Void, Filter<Device>> properties;
                                switch (result) {
                                    case 0:
                                        properties = controller.getLink().getFeatures()
                                                .newDiscoveryBuilder().build();
                                        break;
                                    case 1:
                                        properties = controller.getLink().getFeatures()
                                                .newDiscoveryBuilder()
                                                .setScannerFilter(device ->
                                                        !Constants.UNDEFINED.equals(device.getName()))
                                                .setStopRule(devices -> devices.size() > 0)
                                                .build();
                                        break;
                                    default:
                                        throw new RuntimeException("Undefined option " + result);
                                }
                                controller.toggleDiscovery(properties);
                            });
                });
    }

    @Override
    public void onActionVisibility(View view) {
        helper.info("Only default visibility supported. NO DATA.");
        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_LOCATION,
                granted -> {
                    if (!granted) {
                        helper.error("No enough permissions to continue");
                        return;
                    }
                    VisibilityProperties<Void, Void> properties = controller.getLink()
                            .getFeatures().newVisibilityBuilder().build();
                    controller.toggleVisibility(properties);
                });
    }

    @Override
    public void onActionConnection(View view) {
        //merge discovered devices with connected
        HashSet<Device> devicesHash = new HashSet<>(controller.getDevicesDiscovered());
        devicesHash.addAll(controller.getLink().getConnected());

        Device[] devices = devicesHash.toArray(new Device[devicesHash.size()]);
        if (devices.length == 0) {
            helper.warning("No devices discovered. Empty list devices!!");
            return;
        }
        final String[] devicesOptions = ActivityHelper.devicesToStr(devices);
        ActivityHelper.showOptionsDialog(this, "Select a device", devicesOptions,
                result -> {
                    Device dev = devices[result];
                    ConnectionProperties<Void> properties = controller.getLink().getFeatures()
                            .newConnectionBuilder(dev).build();
                    controller.toggleConnection(dev, properties);
                });
    }

    @Override
    public void onActionAcceptance(View view) {
        ServerProperties<Void> properties = controller.getLink().getFeatures().newServerBuilder().build();
        controller.toggleAccept(properties);
    }

    @Nullable
    private Experiment experiment;
    //TODO - remove
    @Nullable
    private String expType;

    @Override
    public void onExperimentStart(@NonNull String exp, @NonNull String logFileName) {
        this.expType = exp;
        helper.info(String.format(Locale.ENGLISH, "Starting experiment %s and logging to %s", exp, logFileName));
        //startup
        controller.enableLog(AdbBroadcast.BASE_PATH + logFileName);
        controller.start();
        //TODO - remove
        boolean perform = false;

        switch (exp) {
            case AdbBroadcast.EXP_IDLE_SERVER:
                experiment = new BluetoothExperiment(controller, true);
                break;
            case AdbBroadcast.EXP_IDLE:
                experiment = new BluetoothExperiment(controller, false);
                break;
            case AdbBroadcast.EXP_IDLE_BLUETOOTH_SERVER:
                experiment = new BluetoothExperiment(controller, true);
                controller.toggleHardware();
                break;
            case AdbBroadcast.EXP_IDLE_BLUETOOTH:
                experiment = new BluetoothExperiment(controller, false);
                controller.toggleHardware();
                break;
            case AdbBroadcast.EXP_BLUETOOTH_SERVER:
                experiment = new BluetoothExperiment(controller, true);
                perform = true;
                break;
            case AdbBroadcast.EXP_BLUETOOTH:
                experiment = new BluetoothExperiment(controller, false);
                perform = true;
                break;
            default:
                throw new RuntimeException("Undefined experiment " + exp);
        }
        if (perform) {
            experiment.perform(outcome -> {
                outcome.ifError(error -> {
                    helper.error(outcome.getError().getMessage());
                    adbBroadcast.propertiesManager.setProperties(
                            AdbBroadcast.BARRIER_DONE, Boolean.toString(false));
                });
                outcome.ifSuccess(arg -> {
                    helper.success("Experiment successfully done!!");
                    adbBroadcast.propertiesManager.setProperties(
                            AdbBroadcast.BARRIER_DONE, Boolean.toString(true));
                    //TODO - to remove
                    if (AdbBroadcast.EXP_BLUETOOTH.equals(exp)) {
                        onExperimentStop();
                    }
                });
            });
        } else {
            //TODO - remove
            long t = 1000 * 60; // one minute
            CountDownTimer timer = new CountDownTimer(t, t) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    onExperimentStop();
                }
            };
            timer.start();
        }
    }

    @Override
    public void onExperimentStop() {
        if (experiment != null) {
            experiment.finish();
            //TODO - remove
            if (AdbBroadcast.EXP_IDLE_SERVER.equals(expType) || AdbBroadcast.EXP_IDLE.equals(expType)) {
                controller.stop();
                adbBroadcast.propertiesManager.close();
                //closes activity
                helper.post(this::finishAndRemoveTask);
            } else {
                controller.toggleHardware(result -> {
                    controller.stop();
                    adbBroadcast.propertiesManager.close();
                    //closes activity
                    helper.post(this::finishAndRemoveTask);
                });
            }
            experiment = null;
        }
    }
}
