/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.LinkPromise;
import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.outcome.Error;
import org.hyrax.link.misc.outcome.Success;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.util.Collections;

/**
 * Asynchronous link control, with promises
 */
class PromiseCtl<F extends LinkFeatures> implements LinkController<F> {
    private LinkPromise<F> linkPromise;

    PromiseCtl(@NonNull LinkPromise<F> linkPromise) {
        this.linkPromise = linkPromise;
    }

    @Override
    public Link<F> getLink() {
        return linkPromise;
    }

    @Override
    public void toggleHardware(@NonNull ActionResult result) {
        if (linkPromise.isEnabled())
            linkPromise.disable()
                    .done(success -> result.onResult(new Success(success)))
                    .fail(error -> result.onResult(new Error(error, false)));
        else
            linkPromise.enable()
                    .done(success -> result.onResult(new Success(success)))
                    .fail(error -> result.onResult(new Error(error, false)));
    }

    @Override
    public void toggleDiscovery(@NonNull DiscoveryProperties properties, @NonNull ActionResult result) {
        if (linkPromise.isDiscovering(properties.getIdentifier()))
            linkPromise.cancelDiscover(properties.getIdentifier())
                    .done(scanId -> result.onResult(new Success(scanId)))
                    .fail(error -> result.onResult(new Error(error, properties.getIdentifier())));
        else
            linkPromise.discover(properties)
                    .done(devices -> result.onResult(new Success(devices)))
                    .fail(error -> result.onResult(new Error(error, Collections.EMPTY_LIST)));
    }

    @Override
    public void toggleVisibility(@NonNull VisibilityProperties properties,
                                 @NonNull ActionResult result) {
        if (linkPromise.isVisible(properties.getIdentifier()))
            linkPromise.cancelVisible(properties.getIdentifier())
                    .done(advertiserId -> result.onResult(new Success(advertiserId)))
                    .fail(error -> result.onResult(new Error(error, properties.getIdentifier())));
        else
            linkPromise.setVisible(properties)
                    .done(advertiserId -> result.onResult(new Success(advertiserId)))
                    .fail(error -> result.onResult(new Error(error, properties.getIdentifier())));
    }

    @Override
    public void toggleConnection(@NonNull Device device, @NonNull ConnectionProperties properties,
                                 @NonNull ActionResult result) {
        if (linkPromise.isConnected(device))
            linkPromise.disconnect(device)
                    .done(dev -> result.onResult(new Success(dev)))
                    .fail(error -> result.onResult(new Error(error, device)));
        else
            linkPromise.connect(device, properties)
                    .done(dev -> result.onResult(new Success(dev)))
                    .fail(error -> result.onResult(new Error(error, device)));
    }

    @Override
    public void disableConnection(@NonNull Device device, ActionResult result) {
        linkPromise.disconnect(device)
                .done(dev -> result.onResult(new Success(dev)))
                .fail(error -> result.onResult(new Error(error, device)));
    }

    @Override
    public void toggleAccept(@NonNull ServerProperties properties, @NonNull ActionResult result) {
        if (linkPromise.isAccepting(properties.getIdentifier()))
            linkPromise.deny(properties.getIdentifier())
                    .done(serverId -> result.onResult(new Success(serverId)))
                    .fail(error -> result.onResult(new Error(error, properties.getIdentifier())));
        else
            linkPromise.accept(properties)
                    .done(serverId -> result.onResult(new Success(serverId)))
                    .fail(error -> result.onResult(new Error(error, properties.getIdentifier())));
    }
}
