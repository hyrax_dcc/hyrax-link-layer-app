/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkService;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityComm.Mode;

/**
 * Activity helper class, that abstracts some general execution.
 */
public class LinkHelper<F extends LinkFeatures> implements LinkController<F> {
    private final Context appContext;
    final ActivityComm activityComm;
    final Technology technology;
    private final LinkLog listenersEvents;
    private final ArrayList<Device> devicesDiscovered;
    @Nullable
    private String logFilePath;

    //transitions of modes
    private static final Map<Mode, Mode> transitionMode = new HashMap<Mode, Mode>() {{
        put(Mode.SYNC, Mode.ASYNC);
        put(Mode.ASYNC, Mode.PROMISE);
        put(Mode.PROMISE, Mode.SYNC);
    }};

    private Mode execMode;
    private LinkController<F> controller;

    /**
     * Constructor.
     *
     * @param appContext   activity context
     * @param activityComm communication ui handler (Communication with MAIN thread)
     */
    public LinkHelper(@NonNull Context appContext,
                      @NonNull ActivityComm activityComm, @NonNull Technology technology) {
        this.appContext = appContext;
        this.activityComm = activityComm;
        this.technology = technology;
        this.listenersEvents = new LinkLog(this);
        this.devicesDiscovered = new ArrayList<>();
        this.execMode = Mode.SYNC;
    }

    /**
     * Activates the logger.
     * This should be called before start() method.
     * The log will go to public download folder
     *
     * @param logFilePath log file path.
     */
    public void enableLog(@NonNull String logFilePath) {
        this.logFilePath = logFilePath;
    }

    /**
     * Returns the activity communication object.
     *
     * @return activity communication object.
     */
    @NonNull
    public ActivityComm getActivityComm() {
        return activityComm;
    }

    /**
     * Follows activity flow.
     */
    public void start() {
        if (!LinkService.isEnabled()) {
            if (logFilePath != null)
                LogHelper.newInstance(logFilePath);
            //start log battery, cpu, memory stats - every 200 milliseconds
            LogHelper.logStats(appContext, 200);

            LinkService.boot(appContext);
            //default mode is asynchronous
            swapMode(Mode.ASYNC);
            //verifies if the current link is supported
            if (!controller.getLink().isSupported())
                activityComm.error(String.format("Tech Link %s not supported", technology));
            //check is the hardware already enabled
            if (controller.getLink().isEnabled())
                activityComm.toggleButton(ActivityComm.Button.HARDWARE,
                        ActivityComm.Button.State.ON, 0);
            //check if the hardware has any active connection
            if (controller.getLink().getConnected().size() > 0)
                activityComm.toggleButton(ActivityComm.Button.CONNECTION,
                        ActivityComm.Button.State.ON, controller.getLink().getConnected().size());
        }
    }

    /**
     * Follows activity flow.
     */
    public void stop() {
        LinkService.shutdown();
        LogHelper.destroy();
    }

    /**
     * Changes execution mode SYNC -> ASYNC -> PROMISE
     */
    public void toggleMode() {
        Mode mode = transitionMode.get(execMode);
        if (mode == null)
            throw new RuntimeException("Undefined execution mode " + execMode);
        swapMode(mode);
    }

    /**
     * Changes the execution model given a mode.
     *
     * @param mode new execution model
     */
    private void swapMode(@NonNull Mode mode) {
        switch (mode) {
            case SYNC:
                execMode = Mode.SYNC;
                controller = new SyncCtl<>(LinkService.getLinkSync(technology));
                activityComm.toastLong("Exec mode is now synchronous");
                break;
            case ASYNC:
                execMode = Mode.ASYNC;
                controller = new AsyncCtl<>(LinkService.getLinkAsync(technology));
                activityComm.toastLong("Exec mode is now asynchronous (default)");
                break;
            case PROMISE:
                execMode = Mode.PROMISE;
                controller = new PromiseCtl<>(LinkService.getLinkPromise(technology));
                activityComm.toastLong("Exec mode is now asynchronous through promises");
                break;
            default:
                throw new RuntimeException("Undefined mode " + mode);
        }
        activityComm.toggleButtonExec(mode);
        //listen all link layer events
        controller.getLink().removeListener(listenersEvents);
        controller.getLink().listen(listenersEvents);
    }


    @Override
    public Link<F> getLink() {
        return controller.getLink();
    }

    @Override
    public void toggleHardware(@NonNull ActionResult result) {
        LogHelper.log(LogHelper.Type.HARDWARE_CALL_BEGIN);
        controller.toggleHardware(outcome -> {
            LogHelper.log(LogHelper.Type.HARDWARE, outcome.isSuccessful());
            result.onResult(outcome);
        });
        LogHelper.log(LogHelper.Type.HARDWARE_CALL_END);
    }

    @Override
    public void toggleDiscovery(@NonNull DiscoveryProperties properties,
                                @NonNull ActionResult result) {
        if (!controller.getLink().isDiscovering(properties.getIdentifier()))
            controller.getLink().listenOnce(
                    LinkEvent.LINK_DISCOVERY_ON,
                    properties.getIdentifier(),
                    (eventType, outcome) -> LogHelper.log(LogHelper.Type.DISCOVERING, outcome.isSuccessful())
            );
        LogHelper.log(LogHelper.Type.DISCOVERY_CALL_BEGIN);
        controller.toggleDiscovery(properties, outcome -> {
            LogHelper.log(LogHelper.Type.DISCOVERY_DONE, outcome.getOutcome());

            Object data = outcome.getOutcome();
            if (data instanceof Collection) {
                devicesDiscovered.clear();
                devicesDiscovered.addAll((Collection<Device>) outcome.getOutcome());
            }
            result.onResult(outcome);
        });
        LogHelper.log(LogHelper.Type.DISCOVERY_CALL_END);
    }

    @Override
    public void toggleVisibility(@NonNull VisibilityProperties properties,
                                 @NonNull ActionResult result) {
        if (!controller.getLink().isVisible(properties.getIdentifier()))
            controller.getLink().listenOnce(
                    LinkEvent.LINK_VISIBILITY_DONE,
                    properties.getIdentifier(),
                    (eventType, outcome) -> LogHelper.log(LogHelper.Type.VISIBILITY_DONE)
            );
        LogHelper.log(LogHelper.Type.VISIBILITY_CALL_BEGIN);
        controller.toggleVisibility(properties, outcome -> {
            LogHelper.log(LogHelper.Type.VISIBLE, outcome.isSuccessful());
            result.onResult(outcome);
        });
        LogHelper.log(LogHelper.Type.VISIBILITY_CALL_END);
    }

    @Override
    public void toggleConnection(@NonNull Device device, @NonNull ConnectionProperties properties,
                                 @NonNull ActionResult result) {
        LogHelper.log(LogHelper.Type.CONNECTION_CALL_BEGIN);
        controller.toggleConnection(device, properties, outcome -> {
            LogHelper.log(LogHelper.Type.CONNECTED, outcome.getOutcome());
            result.onResult(outcome);
        });
        LogHelper.log(LogHelper.Type.CONNECTION_CALL_END);
    }

    @Override
    public void disableConnection(@NonNull Device device, ActionResult result) {
        controller.disableConnection(device, result);
    }

    @Override
    public void toggleAccept(@NonNull ServerProperties properties, @NonNull ActionResult result) {
        LogHelper.log(LogHelper.Type.ACCEPT_CALL_BEGIN);
        controller.toggleAccept(properties, outcome -> {
            LogHelper.log(LogHelper.Type.ACCEPTING, outcome.getOutcome());
            result.onResult(outcome);
        });
        LogHelper.log(LogHelper.Type.ACCEPT_CALL_END);
    }

    /**
     * Returns the discovered devices.
     *
     * @return array list of devices
     */
    public Collection<Device> getDevicesDiscovered() {
        return devicesDiscovered;
    }
}
