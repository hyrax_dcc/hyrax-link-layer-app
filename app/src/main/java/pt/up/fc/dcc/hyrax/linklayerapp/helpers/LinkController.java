/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.hyrax.link.Device;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

/**
 * Interface that abstracts link interactions.
 */
public interface LinkController<F extends LinkFeatures> {

    /**
     * Interface result.
     */
    interface ActionResult {
        /**
         * Method triggered when result is available
         *
         * @param outcome outcome object
         */
        void onResult(@NonNull Outcome outcome);
    }

    /**
     * Returns the link object
     *
     * @return link object
     */
    Link<F> getLink();

    /**
     * Toggles hardware state ON/OFF.
     */
    default void toggleHardware() {
        toggleHardware(outcome -> Log.d("Hardware", "Default"));
    }

    /**
     * Toggles hardware state ON/OFF.
     *
     * @param result callback result
     */
    void toggleHardware(@NonNull ActionResult result);

    /**
     * Toggles discovery/scanner states ON/OFF.
     *
     * @param properties discovery properties
     */
    default void toggleDiscovery(@NonNull DiscoveryProperties properties) {
        toggleDiscovery(properties, (outcome -> Log.d("Discovery", "Default")));
    }

    /**
     * Toggles discovery/scanner states ON/OFF.
     *
     * @param properties properties discovery properties
     * @param result     callback result
     */
    void toggleDiscovery(@NonNull DiscoveryProperties properties, @NonNull ActionResult result);

    /**
     * Toggles visibility/advertiser states ON/OFF.
     *
     * @param properties discovery properties
     */
    default void toggleVisibility(@NonNull VisibilityProperties properties) {
        toggleVisibility(properties, (outcome -> Log.d("Visibility", "Default")));
    }

    /**
     * Toggles visibility/advertiser states ON/OFF.
     *
     * @param properties discovery properties
     * @param result     callback result
     */
    void toggleVisibility(@NonNull VisibilityProperties properties, @NonNull ActionResult result);

    /**
     * Toggles device connection states ON/OFF.
     *
     * @param device     device to connect to/disconnect from
     * @param properties connection properties
     */
    default void toggleConnection(@NonNull Device device, @NonNull ConnectionProperties properties) {
        toggleConnection(device, properties, outcome ->
                Log.d("Connection", "Default -> " + device));
    }

    /**
     * Toggles device connection states ON/OFF.
     *
     * @param device     device to connect to/disconnect from
     * @param properties connection properties
     * @param result     callback result
     */
    void toggleConnection(@NonNull Device device,
                          @NonNull ConnectionProperties properties,
                          @NonNull ActionResult result);

    /**
     * Disconnects from a device.
     *
     * @param device device to disconnect from
     */
    default void disableConnection(@NonNull Device device) {
        disableConnection(device, outcome ->
                Log.d("Disconnect", "Default -> " + device));
    }

    /**
     * Disconnects from a device.
     *
     * @param device device to disconnect from
     * @param result callback result
     */
    void disableConnection(@NonNull Device device, ActionResult result);

    /**
     * Toggles acceptance of remote connections states ON/OFF.
     *
     * @param properties server properties
     */
    default void toggleAccept(@NonNull ServerProperties properties) {
        toggleAccept(properties, outcome -> Log.d("Accept", "Default"));
    }

    /**
     * Toggles acceptance of remote connections states ON/OFF.
     *
     * @param properties server properties
     * @param result     callback result
     */
    void toggleAccept(@NonNull ServerProperties properties, @NonNull ActionResult result);
}
