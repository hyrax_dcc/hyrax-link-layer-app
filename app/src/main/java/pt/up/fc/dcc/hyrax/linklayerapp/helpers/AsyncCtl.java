/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.LinkAsync;
import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

/**
 * Asynchronous link control.
 */
class AsyncCtl<F extends LinkFeatures> implements LinkController<F> {
    private final LinkAsync<F> linkAsync;

    /**
     * Constructor
     *
     * @param linkAsync object link async
     */
    AsyncCtl(@NonNull LinkAsync<F> linkAsync) {
        this.linkAsync = linkAsync;
    }

    @Override
    public Link<F> getLink() {
        return linkAsync;
    }

    @Override
    public void toggleHardware(@NonNull ActionResult result) {
        if (linkAsync.isEnabled())
            linkAsync.disable((eventType, outcome) -> result.onResult(outcome));
        else
            linkAsync.enable(((eventType, outcome) -> result.onResult(outcome)));
    }

    @Override
    public void toggleDiscovery(@NonNull DiscoveryProperties properties,
                                @NonNull ActionResult result) {
        if (linkAsync.isDiscovering(properties.getIdentifier()))
            linkAsync.cancelDiscover(properties.getIdentifier(),
                    ((eventType, outcome) -> result.onResult(outcome)));
        else
            linkAsync.discover(properties, ((eventType, outcome) -> result.onResult(outcome)));
    }

    @Override
    public void toggleVisibility(@NonNull VisibilityProperties properties,
                                 @NonNull ActionResult result) {
        if (linkAsync.isVisible(properties.getIdentifier()))
            linkAsync.cancelVisible(properties.getIdentifier(),
                    (eventType, outcome) -> result.onResult(outcome));
        else
            linkAsync.setVisible(properties, (eventType, outcome) -> result.onResult(outcome));
    }

    @Override
    public void toggleConnection(@NonNull Device device, @NonNull ConnectionProperties properties,
                                 @NonNull ActionResult result) {
        if (linkAsync.isConnected(device))
            linkAsync.disconnect(device, (eventType, outcome) -> result.onResult(outcome));
        else
            linkAsync.connect(device, properties, (eventType, outcome) -> result.onResult(outcome));
    }

    @Override
    public void disableConnection(@NonNull Device device, ActionResult result) {
        linkAsync.disconnect(device, (eventType, outcome) -> result.onResult(outcome));
    }

    @Override
    public void toggleAccept(@NonNull ServerProperties properties, @NonNull ActionResult result) {
        if (linkAsync.isAccepting(properties.getIdentifier()))
            linkAsync.deny(properties.getIdentifier(),
                    (eventType, outcome) -> result.onResult(outcome));
        else
            linkAsync.accept(properties, (eventType, outcome) -> result.onResult(outcome));

    }
}
