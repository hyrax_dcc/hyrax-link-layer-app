/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.support.annotation.NonNull;
import android.util.Log;

import junit.framework.Assert;

import org.hyrax.link.Device;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.ServerInfo;
import org.hyrax.link.misc.exception.LinkLayerRuntimeException;

import java.util.Collection;
import java.util.Objects;

/**
 * Class used to log every link layer event.
 */
public class LinkLog implements LinkListener {
    private final LinkHelper helper;

    /**
     * Constructor.
     *
     * @param helper activity helper object
     */
    LinkLog(@NonNull LinkHelper helper) {
        this.helper = helper;
    }

    @Override
    public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome outcome) {
        Objects.requireNonNull(eventType);
        Objects.requireNonNull(outcome);
        Objects.requireNonNull(outcome.getOutcome());
        Objects.requireNonNull(outcome.getError());
        switch (eventType) {
            case LINK_HARDWARE_ON:
                Assert.assertTrue(outcome.getOutcome() instanceof Boolean);
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.HARDWARE,
                        ActivityComm.Button.State.ON, 0));
                break;
            case LINK_HARDWARE_OFF:
                Assert.assertTrue(outcome.getOutcome() instanceof Boolean);
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.HARDWARE,
                        ActivityComm.Button.State.OFF, 0));
                break;
            case LINK_DISCOVERY_ON:
                Assert.assertTrue(outcome.getOutcome() instanceof String);
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.DISCOVERY,
                        ActivityComm.Button.State.ON,
                        helper.getLink().getScanners().size()));
                break;
            case LINK_DISCOVERY_FOUND:
                Assert.assertTrue(outcome.getOutcome() instanceof Collection);
                //TODO - perform a deep validation inside collection
                break;
            case LINK_DISCOVERY_DONE:
                Assert.assertTrue(outcome.getOutcome() instanceof Collection);
                //TODO - perform a deep validation inside collection
                break;
            case LINK_DISCOVERY_OFF:
                Assert.assertTrue(outcome.getOutcome() instanceof String);
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.DISCOVERY,
                        ActivityComm.Button.State.OFF,
                        helper.getLink().getScanners().size()));
                break;
            case LINK_VISIBILITY_ON:
                Assert.assertTrue(outcome.getOutcome() instanceof String);
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.VISIBILITY,
                        ActivityComm.Button.State.ON,
                        helper.getLink().getAdvertisers().size()));
                break;
            case LINK_VISIBILITY_DONE:
                Assert.assertTrue(outcome.getOutcome() instanceof String);
                break;
            case LINK_VISIBILITY_OFF:
                Assert.assertTrue(outcome.getOutcome() instanceof String);
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.VISIBILITY,
                        ActivityComm.Button.State.OFF,
                        helper.getLink().getAdvertisers().size()));
                break;
            case LINK_CONNECTION_NEW:
                Assert.assertTrue(outcome.getOutcome() instanceof Device);
                //TODO - perform a deep validation inside Device
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.CONNECTION,
                        ActivityComm.Button.State.ON,
                        helper.getLink().getConnected().size()));
                break;
            case LINK_CONNECTION_LOST:
                Assert.assertTrue(outcome.getOutcome() instanceof Device);
                //TODO - perform a deep validation inside Device
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.CONNECTION,
                        ActivityComm.Button.State.OFF,
                        helper.getLink().getConnected().size()));
                break;
            case LINK_CONNECTION_SERVER_ON:
                Assert.assertTrue(outcome.getOutcome() instanceof String);
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.ACCEPTANCE,
                        ActivityComm.Button.State.ON,
                        helper.getLink().getAccepted().size()));
                break;
            case LINK_CONNECTION_SERVER_INFO:
                Assert.assertTrue(outcome.getOutcome() instanceof ServerInfo);
                //TODO - perform a deep validation inside Device
                break;
            case LINK_CONNECTION_SERVER_NEW_CLIENT:
                Assert.assertTrue(outcome.getOutcome() instanceof Device);
                break;
            case LINK_CONNECTION_SERVER_OFF:
                Assert.assertTrue(outcome.getOutcome() instanceof String);
                outcome.ifSuccess(argument -> helper.activityComm.toggleButton(
                        ActivityComm.Button.ACCEPTANCE,
                        ActivityComm.Button.State.OFF,
                        helper.getLink().getAccepted().size()));
                break;
            case LINK_DETACH:
                Assert.assertTrue(outcome.getOutcome() instanceof Boolean);
                break;
            default:
                throw new LinkLayerRuntimeException("Undefined event " + eventType);
        }
        //notifies the link layer events
        notifyOutcome(eventType, outcome);
    }

    /**
     * Sends to screen to result of link layer actions.
     *
     * @param event   link layer event
     * @param outcome link layer outcome object
     */
    private void notifyOutcome(@NonNull LinkEvent event, @NonNull Outcome outcome) {
        outcome.ifSuccess(argument -> {
            String out = String.format("%s -> %s: %s",
                    helper.technology, event, argument.toString());
            Log.d("LinkEvent", out);
            helper.activityComm.debug(out);
        });
        outcome.ifError(argument -> {
            String out = String.format("%s -> %s FAILURE (Rs: %s :: %s)",
                    helper.technology, event, outcome.getError().getReason(),
                    outcome.getError().getMessage());
            Log.e("LinkEvent", out);
            helper.activityComm.error(out);
        });
    }
}
