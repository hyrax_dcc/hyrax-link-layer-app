/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.socket;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import org.hyrax.link.misc.Observer;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityComm;

/**
 * Wifi client socket.
 */
public class WifiSocketComm implements Runnable, SocketComm {
    private final ActivityComm activityComm;
    private final String host;
    private final int port;
    private final AtomicBoolean running;
    private Socket socket;
    private final ExecutorService executorService;
    private Observer<Boolean> connectListener = (Observer<Boolean>) object -> {
    };
    @NonNull
    private final SparseArray<Observer<Type>> msgListeners;

    private int connectionAttempts;
    private int connectionAttemptWaitInterval;
    private int msgId;

    /**
     * Constructor.
     *
     * @param activityComm activity communication object
     * @param host         remote host
     * @param port         remote port
     */
    public WifiSocketComm(@NonNull ActivityComm activityComm, @NonNull String host, int port) {
        this.activityComm = activityComm;
        this.host = host;
        this.port = port;
        this.running = new AtomicBoolean(false);
        this.executorService = Executors.newSingleThreadExecutor();
        this.msgListeners = new SparseArray<>();
        this.connectionAttempts = 5;
        this.connectionAttemptWaitInterval = 200;
        this.msgId = 0;
    }

    /**
     * Constructor.
     *
     * @param activityComm activity communication object
     * @param socket       socket object from server
     */
    WifiSocketComm(@NonNull ActivityComm activityComm, @NonNull Socket socket) {
        this(activityComm, "<from_server>", 0);
        this.socket = socket;
    }

    /**
     * Changes the number of connection attempts.
     *
     * @param attempts connection attempts
     */
    public void setConnectionAttempts(int attempts) {
        this.connectionAttempts = attempts;
    }

    /**
     * Changes the interval (in milliseconds) between each connection.
     *
     * @param interval connection interval
     */
    public void setConnectionAttemptWaitInterval(int interval) {
        this.connectionAttemptWaitInterval = interval;
    }

    @Override
    public void connect(@NonNull Observer<Boolean> listener) {
        if (running.compareAndSet(false, true)) {
            this.connectListener = listener;
            new Thread(this).start();
            return;
        }
        throw new RuntimeException("Client: already running");
    }

    @Override
    public void disconnect() {
        if (running.compareAndSet(true, false)) {
            try {
                executorService.shutdownNow();
                socket.close();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Performs the connection.
     *
     * @return socket object
     */
    @NonNull
    private Socket performConnect() throws ConnectException {
        int attempts = connectionAttempts;
        while (attempts-- > 0) {
            try {
                Socket socket = new Socket();
                socket.connect(new InetSocketAddress(host, port), connectionAttemptWaitInterval);
                return socket;
            } catch (IOException e) {
                e.printStackTrace();
                if (!(e instanceof SocketTimeoutException)) {
                    try {
                        Thread.sleep(connectionAttemptWaitInterval);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        throw new ConnectException("Connection failed!!");
    }

    @Override
    public void run() {
        try {
            if (socket == null)
                socket = performConnect();
            connectListener.onEvent(true);
            activityComm.info("Connected to " + host);

            DataInputStream in = new DataInputStream(socket.getInputStream());
            boolean run = true;
            while (run) {
                try {
                    //read message identifier
                    int id = in.readInt();
                    //read message type and transform to type enum object
                    Type t = Type.toType(in.readByte());
                    Objects.requireNonNull(t);
                    //reads the payload size
                    int s = in.readInt();
                    //reads the  remaining message
                    onMessage(id, t, s, in);
                } catch (EOFException e) {
                    e.printStackTrace();
                    run = false;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (running.get())
                activityComm.error("Client: " + e.getMessage());
            if (e instanceof ConnectException)
                connectListener.onEvent(false);

        } finally {
            running.set(false);
            activityComm.warning("Client Disconnected");
        }
    }

    /**
     * Process a received message
     *
     * @param msgId       message identifier
     * @param type        message type
     * @param payloadSize the remaining message size
     * @param inputStream input stream object
     * @throws IOException throws is any read fails
     */
    private void onMessage(int msgId, Type type, int payloadSize, @NonNull InputStream inputStream) throws IOException {
        activityComm.debug(String.format(Locale.ENGLISH,
                "Rcv Message (Id: %d) of Type: %s payload size: %s",
                msgId, type, SocketComm.sizeToString(payloadSize)));
        switch (type) {
            case PING:
                read(inputStream, payloadSize);
                Objects.requireNonNull(socket);
                write(socket.getOutputStream(), msgId, Type.PONG.getId(), 0);
                break;

            case PONG:
                Observer<Type> evt = msgListeners.get(msgId, tp -> {
                });
                evt.onEvent(type);
                msgListeners.delete(msgId);
                break;
            default:
                throw new RuntimeException("Undefined action " + type);
        }
    }


    @Override
    public void sendMessage(@NonNull Type type, int payloadSize) {
        final long start_ts = System.currentTimeMillis();
        sendMessage(type, payloadSize, tp -> {
            long t = System.currentTimeMillis() - start_ts;
            activityComm.info(String.format(Locale.ENGLISH,
                    "Msg of type: %s RTT: %s Speed: %s",
                    tp,
                    SocketComm.timeToString(t),
                    SocketComm.speedToString(SocketComm.getSpeedPerSecond(payloadSize, t))));
        });
    }

    @Override
    public void sendPingMultiple(int number) {
        if (number == 0)
            return;
        final long start_ts = System.currentTimeMillis();
        sendMessage(Type.PING, 0, tp -> {
            long t = System.currentTimeMillis() - start_ts;
            activityComm.info(String.format(Locale.ENGLISH,
                    "Msg of type: %s RTT: %s Speed: %s",
                    tp,
                    SocketComm.timeToString(t),
                    SocketComm.speedToString(SocketComm.getSpeedPerSecond(0, t))));
            sendPingMultiple(number - 1);
        });
    }

    @Override
    public void sendMessage(@NonNull Type type, int payloadSize, @NonNull Observer<Type> listener) {
        executorService.submit(() -> {
            try {
                activityComm.debug(String.format(Locale.ENGLISH,
                        "Sending message (Id: %d) of Type: %s with size: %s",
                        msgId, type, SocketComm.sizeToString(payloadSize)));
                Objects.requireNonNull(socket);
                final int id = msgId;
                //adds a listener for response
                msgListeners.append(id, listener);

                write(socket.getOutputStream(), msgId, type.getId(), payloadSize);

                //increments message identifier
                msgId += 1;
            } catch (IOException e) {
                e.printStackTrace();
                activityComm.error("Error sending message " + e.getMessage());
            }
        });
    }
}
