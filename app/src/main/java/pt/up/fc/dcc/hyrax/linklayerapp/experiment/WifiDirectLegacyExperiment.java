/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.experiment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.LinkService;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.outcome.Error;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.WifiLink;
import org.hyrax.link.wifi.direct.WifiDirectCredentials;
import org.hyrax.link.wifi.types.ScanDataWifi;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiScanRecord;

import java.util.Collection;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkController;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.SocketComm;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketComm;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketServer;

/**
 * Wifi Direct legacy experiment.
 */
public class WifiDirectLegacyExperiment extends Experiment<WifiDirectLink> {
    private static final String SERVICE_NAME = "service_ap";
    private static final String KEY_SSID = "ssid";
    private static final String KEY_PASS = "pass";

    private final LinkHelper<WifiLink> wifiLinkHelper;
    @Nullable
    private WifiSocketServer socketServer;
    @Nullable
    private WifiSocketComm socketComm;
    @Nullable
    private WifiScanRecord wifiScanRecord;

    /**
     * Constructor.
     *
     * @param linkHelper link helper object
     * @param server     <tt>true</tt> if server, <tt>false</tt> otherwise
     */
    public WifiDirectLegacyExperiment(@NonNull LinkHelper<WifiDirectLink> linkHelper,
                                      @NonNull LinkHelper<WifiLink> wifiLinkHelper,
                                      boolean server) {
        super(linkHelper, server);
        this.wifiLinkHelper = wifiLinkHelper;
        if (server)
            this.socketServer = new WifiSocketServer(linkHelper.getActivityComm());
    }


    @Override
    ServerProperties getServerProperties() {
        return linkHelper.getLink().getFeatures().newServerBuilder().build();
    }

    private UUID visUUID;

    @Override
    VisibilityProperties getVisibilityProperties() {
        if (visUUID == null)
            visUUID = UUID.randomUUID();

        WifiDirectCredentials credentials = linkHelper.getLink().getFeatures()
                .getWifiP2pCredentials(getServerProperties().getIdentifier());
        Objects.requireNonNull(credentials);

        WifiAdvertiseData data = new WifiAdvertiseData();
        data.addData(KEY_SSID, credentials.getSSID());
        data.addData(KEY_PASS, credentials.getPassword());

        Objects.requireNonNull(socketServer);
        return linkHelper.getLink().getFeatures()
                .newVisibilityBuilder(
                        SERVICE_NAME,
                        socketServer.getListenPort(),
                        data)
                .setAdvertiseId(visUUID.toString())
                .build();
    }

    @Override
    DiscoveryProperties getDiscoveryProperties() {
        return linkHelper.getLink().getFeatures()
                .newDiscoveryBuilder(SERVICE_NAME)
                .setStopRule(devices -> devices.size() > 0)
                .build();
    }

    @Override
    ConnectionProperties getConnectionProperties(@NonNull Device device) {
        ScanDataWifi dataWifi = (ScanDataWifi) device.getScanData().iterator().next();
        this.wifiScanRecord = dataWifi.getOriginalObject();

        String ssid = wifiScanRecord.getExtraData().get(KEY_SSID);
        String pass = wifiScanRecord.getExtraData().get(KEY_PASS);

        Objects.requireNonNull(ssid);
        Objects.requireNonNull(pass);
        Device wifiDevice = wifiLinkHelper.getLink().getFeatures().toWifiNode(device, ssid);
        return wifiLinkHelper.getLink().getFeatures()
                .newConnectionBuilder(wifiDevice)
                .setPassword(pass)
                .removeWhenDisconnect(true)
                .build();
    }

    @Override
    SocketComm getSocketObject(@NonNull Device device) {
        Objects.requireNonNull(wifiScanRecord);
        String host = Constants.DEFAULT_GO_IP;
        int port = wifiScanRecord.getPort();
        this.socketComm = new WifiSocketComm(linkHelper.getActivityComm(), host, port);
        return socketComm;
    }

    @Override
    public void finish() {
        if (socketComm != null)
            socketComm.disconnect();
        if (socketServer != null)
            socketServer.stop();

        if (!server) {
            Collection<Device> devices = wifiLinkHelper.getLink().getConnected();
            for (Device device : devices)
                wifiLinkHelper.disableConnection(device);
        }
    }

    @Override
    void connect(@NonNull Device device, @NonNull ConnectionProperties connectionProperties,
                 @NonNull LinkController.ActionResult result) {
        Objects.requireNonNull(wifiScanRecord);
        String ssid = wifiScanRecord.getExtraData().get(KEY_SSID);
        Device wifiDevice = wifiLinkHelper.getLink().getFeatures().toWifiNode(device, ssid);
        wifiLinkHelper.toggleConnection(wifiDevice, connectionProperties, result);
    }

    @Override
    void executeServer(@NonNull DoneCallback doneCallback) {
        Objects.requireNonNull(socketServer);
        socketServer.setReadyListener(ready -> {
            if (!ready) {
                doneCallback.onDone(new Error<>(LinkException.build(
                        new Exception("Error starting server")), ""));
                return;
            }
            super.executeServer(doneCallback);
        });
        socketServer.start();
    }
}
