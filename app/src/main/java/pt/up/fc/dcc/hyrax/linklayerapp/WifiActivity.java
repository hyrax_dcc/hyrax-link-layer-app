/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.hyrax.link.Device;
import org.hyrax.link.misc.ScanData;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.wifi.types.ScanDataWifi;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiAdvertiseSettings;
import org.hyrax.link.wifi.WifiLink;
import org.hyrax.link.wifi.types.WifiConnectionSettings;
import org.hyrax.link.wifi.types.WifiScanFilter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;

import pt.up.fc.dcc.hyrax.linklayerapp.experiment.AdbBroadcast;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.AdbBroadcastListener;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.Experiment;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.WifiExperiment;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.HandlerHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkUiActionsListener;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.PermissionManager;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketComm;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketServer;

public class WifiActivity extends AppCompatActivity implements LinkUiActionsListener, AdbBroadcastListener {
    //activity helper
    private HandlerHelper helper;
    //Bluetooth link controller
    private LinkHelper<WifiLink> controller;
    //adb commands receiver
    private AdbBroadcast adbBroadcast;
    private WifiSocketServer wifiSocketServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.helper = new HandlerHelper(this);
        this.controller = new LinkHelper<>(this, helper, Technology.WIFI);
        this.adbBroadcast = new AdbBroadcast(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_wifi, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_WRITE_STORAGE,
                granted -> {
                    if (!granted) {
                        helper.error("Need write permissions to log");
                        helper.toastLong("Permissions are needed in order to log traces");
                    }
                });

        boolean toStart = getIntent().getBooleanExtra(
                MainActivity.START_LINK_SERVICE, false);
        if (toStart) {
            //starts wifi socket server
            wifiSocketServer = new WifiSocketServer(helper);
            wifiSocketServer.start();
            //enables logger
            controller.enableLog(AdbBroadcast.BASE_PATH + "app.log");
            //starts the controller
            controller.start();
        }
        //TODO - remove
        String exp = getIntent().getStringExtra(MainActivity.EXPERIMENT);
        String log = getIntent().getStringExtra(MainActivity.LOG_FILE_NAME);
        if (exp != null && log != null) {
            onExperimentStart(exp, log);
        }
        //register broadcast receiver
        registerReceiver(adbBroadcast, adbBroadcast.getIntentFilters());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        helper.getPermissionManager().onPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (wifiSocketServer != null)
            wifiSocketServer.stop();

        controller.stop();
        //unregisters broadcast receiver
        unregisterReceiver(adbBroadcast);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller.stop();
    }

    @Override
    public void onActionAbout(MenuItem menuItem) {
        ActivityHelper.showDialogWithView(this, R.layout.about_wifi);
    }

    @Override
    public void onActionStop(MenuItem menuItem) {
        //TODO - to remove if
        if (experiment != null) {
            onExperimentStop();
            return;
        }
        finishAndRemoveTask();
    }

    @Override
    public void onActionAuto(View view) {
        String[] options = {"Ping", "Ping Multiple"};
        if (controller.getLink().getConnected().size() == 0) {
            helper.error("Not connected to Wifi");
            return;
        }
        Collection<Device> devices = controller.getDevicesDiscovered();
        if (devices.size() == 0) {
            helper.error("No devices discovered");
            return;
        }

        Device device = devices.iterator().next();
        Collection<ScanData> scanData = device.getScanData();
        if (scanData.size() == 0) {
            helper.error("No service data available");
            return;
        }

        ActivityHelper.showOptionsDialog(this, "Select Action", options,
                result -> {
                    switch (result) {
                        case 0:
                            ScanDataWifi dataWifi = (ScanDataWifi) scanData.iterator().next();
                            String host = dataWifi.getOriginalObject().getHost();
                            int port = dataWifi.getOriginalObject().getPort();

                            ActivityHelper.showDialogWithView(WifiActivity.this,
                                    R.layout.ping_select, "Enter ping payload size",
                                    dialogView -> {
                                        EditText text = dialogView.findViewById(R.id.payload_size);
                                        String value = text.getText().toString();
                                        WifiSocketComm socketComm = new WifiSocketComm(helper, host, port);
                                        socketComm.connect(success -> {
                                            if (!success) {
                                                helper.error("Error connection to " + host);
                                                return;
                                            }
                                            socketComm.sendPing(Integer.valueOf(value));
                                        });
                                    });
                            break;
                        case 1:
                            dataWifi = (ScanDataWifi) scanData.iterator().next();
                            host = dataWifi.getOriginalObject().getHost();
                            port = dataWifi.getOriginalObject().getPort();

                            ActivityHelper.showDialogWithView(WifiActivity.this,
                                    R.layout.ping_select, "Enter the number of pings",
                                    dialogView -> {
                                        EditText text = dialogView.findViewById(R.id.payload_size);
                                        String value = text.getText().toString();
                                        WifiSocketComm socketComm = new WifiSocketComm(helper, host, port);
                                        socketComm.connect(success -> {
                                            if (! (boolean) success) {
                                                helper.error("Error connection to " + host);
                                                return;
                                            }
                                            socketComm.sendPingMultiple(Integer.valueOf(value));
                                        });
                                    });
                            break;
                        default:
                            throw new RuntimeException("Undefined test " + result);
                    }
                });
    }

    @Override
    public void onActionExecMode(View view) {
        controller.toggleMode();
    }

    @Override
    public void onActionHardware(View view) {
        controller.toggleHardware();
    }

    @Override
    public void onActionDiscovery(View view) {
        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_LOCATION,
                granted -> {
                    if (!granted) {
                        helper.error("No enough permissions to continue");
                        return;
                    }
                    String[] options = {"Scan Wifi Networks", "Discover Services"};
                    ActivityHelper.showOptionsDialog(this, "Select Type", options,
                            result -> {
                                DiscoveryProperties<Void, WifiScanFilter> properties;
                                switch (result) {
                                    case 0:
                                        //wifi networks scan
                                        properties = controller.getLink().getFeatures()
                                                .newDiscoveryBuilder().build();
                                        break;
                                    case 1:
                                        //wifi scan services
                                        properties = controller.getLink().getFeatures()
                                                .newDiscoveryBuilder("service_example")
                                                .build();
                                        break;
                                    default:
                                        throw new RuntimeException("Undefined option " + result);
                                }
                                controller.toggleDiscovery(properties);
                            });
                });
    }

    @Override
    public void onActionVisibility(View view) {
        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_LOCATION,
                granted -> {
                    if (!granted) {
                        helper.error("No enough permissions to continue");
                        return;
                    }
                    String[] options = {"Visible Wifi - ERROR", "Advertise Service"};
                    ActivityHelper.showOptionsDialog(this, "Select Type", options,
                            result -> {
                                VisibilityProperties<WifiAdvertiseSettings, WifiAdvertiseData> properties;
                                switch (result) {
                                    case 0:
                                        //default ERROR
                                        properties = controller.getLink().getFeatures()
                                                .newVisibilityBuilder().build();
                                        break;
                                    case 1:
                                        properties = controller.getLink().getFeatures()
                                                .newVisibilityBuilder("service_example",
                                                        wifiSocketServer.getListenPort()).build();
                                        break;
                                    default:
                                        throw new RuntimeException("Undefined option " + result);
                                }
                                controller.toggleVisibility(properties);
                            });
                });
    }

    @Override
    public void onActionConnection(View view) {
        //merge discovered devices with connected
        HashSet<Device> devicesHash = new HashSet<>(controller.getDevicesDiscovered());
        devicesHash.addAll(controller.getLink().getConnected());

        Device[] devices = devicesHash.toArray(new Device[devicesHash.size()]);
        if (devices.length == 0) {
            helper.warning("No devices discovered. Empty list devices!!");
            return;
        }
        final String[] devicesOptions = ActivityHelper.devicesToStr(devices);
        ActivityHelper.showOptionsDialog(this, "Select a device", devicesOptions,
                result -> {
                    Device dev = devices[result];
                    ConnectionProperties<WifiConnectionSettings> properties =
                            controller.getLink().getFeatures()
                                    .newConnectionBuilder(dev).build();
                    controller.toggleConnection(dev, properties);
                });
    }

    @Override
    public void onActionAcceptance(View view) {
        ServerProperties serverProperties = controller.getLink().getFeatures().newServerBuilder().build();
        controller.toggleAccept(serverProperties);
    }

    @Nullable
    private Experiment experiment;

    @Override
    public void onExperimentStart(@NonNull String exp, @NonNull String logFileName) {
        helper.info(String.format(Locale.ENGLISH, "Starting experiment %s and logging to %s", exp, logFileName));

        //startup
        controller.enableLog(AdbBroadcast.BASE_PATH + logFileName);
        controller.start();
        //TODO - remove
        boolean perform = false;

        switch (exp) {
            case AdbBroadcast.EXP_IDLE_WIFI_SERVER:
                experiment = new WifiExperiment(controller, true);
                controller.toggleHardware();
                break;
            case AdbBroadcast.EXP_IDLE_WIFI:
                experiment = new WifiExperiment(controller, false);
                controller.toggleHardware();
                break;
            case AdbBroadcast.EXP_WIFI_SERVER:
                experiment = new WifiExperiment(controller, true);
                perform = true;
                break;
            case AdbBroadcast.EXP_WIFI:
                experiment = new WifiExperiment(controller, false);
                perform = true;
                break;
            default:
                throw new RuntimeException("Undefined experiment " + exp);
        }
        if (perform) {
            experiment.perform(outcome -> {
                outcome.ifError(error -> {
                    helper.error(outcome.getError().getMessage());
                    adbBroadcast.propertiesManager.setProperties(
                            AdbBroadcast.BARRIER_DONE, Boolean.toString(false));
                });
                outcome.ifSuccess(arg -> {
                    helper.success("Experiment successfully done!!");
                    adbBroadcast.propertiesManager.setProperties(
                            AdbBroadcast.BARRIER_DONE, Boolean.toString(true));
                    //TODO - to remove
                    if (AdbBroadcast.EXP_WIFI.equals(exp)) {
                        onExperimentStop();
                    }
                });
            });
        } else {
            //TODO - remove
            long t = 1000 * 60; // one minute
            CountDownTimer timer = new CountDownTimer(t, t) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    onExperimentStop();
                }
            };
            timer.start();
        }
    }

    @Override
    public void onExperimentStop() {
        if (experiment != null)
            experiment.finish();

        controller.toggleHardware(result -> {
            controller.stop();
            adbBroadcast.propertiesManager.close();
            //closes activity
            helper.post(this::finishAndRemoveTask);
        });
    }
}
