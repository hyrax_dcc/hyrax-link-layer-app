/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Pair;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import pt.up.fc.dcc.hyrax.linklayerapp.R;

/**
 * This handler acts as a bridge between the link layer and the UI (main thread).
 * <br/>
 * Essentially it changes the UI depending on the type of events that receives.
 */
public class HandlerHelper extends Handler implements ActivityComm {
    private final Activity activity;
    //keeps logs
    private final ArrayList<Pair<String, Color>> logs;
    private final PermissionManager permissionManager;

    /**
     * Constructor.
     *
     * @param activity application activity
     */
    public HandlerHelper(@NonNull Activity activity) {
        super(activity.getMainLooper());
        this.activity = activity;
        this.logs = new ArrayList<>();
        this.permissionManager = new PermissionManager(activity);
    }

    @Override
    public void handleMessage(Message msg) {
        //handle incoming messages
    }

    @Override
    public void message(@NonNull String message, @NonNull Color color) {
        logs.add(new Pair<>(message, color));
        post(() -> {
            logMessage(activity.findViewById(R.id.debug), message, color);
            ((ScrollView) activity.findViewById(R.id.scroll_view)).fullScroll(View.FOCUS_DOWN);
        });
    }

    /**
     * Logs a messages to a text view.
     *
     * @param view    text view object
     * @param message string message
     * @param color   message color
     */
    @MainThread
    private void logMessage(@NonNull TextView view, @NonNull String message, @NonNull Color color) {
        String msg = String.format("<font color=\"%s\">%s</font><br/>", color.hex, message);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            view.append(Html.fromHtml(msg, Html.FROM_HTML_MODE_LEGACY));
        else
            view.append(Html.fromHtml(msg));
    }

    @Override
    public void toastLong(@NonNull String message) {
        post(() -> Toast.makeText(activity, message, Toast.LENGTH_LONG).show());
    }

    @Override
    public void toastShort(@NonNull String message) {
        post(() -> Toast.makeText(activity, message, Toast.LENGTH_SHORT).show());
    }

    @Override
    public void toggleButton(@NonNull Button button, @NonNull Button.State state, int count) {
        switch (button) {
            case HARDWARE:
                setButtonStatus(R.id.hw_state, state, count);
                break;
            case DISCOVERY:
                setButtonStatus(R.id.discovery_state, state, count);
                break;
            case VISIBILITY:
                setButtonStatus(R.id.visibility_state, state, count);
                break;
            case CONNECTION:
                setButtonStatus(R.id.connection_state, state, count);
                break;
            case ACCEPTANCE:
                setButtonStatus(R.id.router_state, state, count);
                break;
        }
    }

    @Override
    public void toggleButtonExec(@NonNull Mode mode) {
        switch (mode) {
            case SYNC:
                setButtonStatus(R.id.exec_mode, "Sync", R.color.white,
                        R.mipmap.ic_sync_black_24dp);
                break;
            case PROMISE:
            case ASYNC:
                setButtonStatus(R.id.exec_mode, "Async", R.color.white,
                        R.mipmap.ic_sync_disabled_black_24dp);
                break;
            default:
                throw new RuntimeException("Undefined mode " + mode);
        }
    }

    /**
     * Sets the button status.
     *
     * @param id    button resource id
     * @param state new button state
     * @param count an integer count e.g. number of connections
     */
    private void setButtonStatus(int id, @NonNull Button.State state, int count) {
        String append = (count > 0) ? String.format(Locale.ENGLISH, " (%d)", count) : "";
        if (count > 0 || state == Button.State.ON)
            setButtonStatus(id, "On" + append, R.color.green);
        else
            setButtonStatus(id, "Off" + append, R.color.red);
    }

    /**
     * Sets button status.
     *
     * @param id    button resource id
     * @param text  new text
     * @param color new text color
     */
    private void setButtonStatus(@IdRes int id, @NonNull String text, @ColorRes int color) {
        post(() -> {
            //Gets button object
            android.widget.Button btn = activity.findViewById(id);
            //sets button object
            btn.setText(text);
            //sets button color
            btn.setTextColor(ContextCompat.getColor(activity, color));
        });
    }

    /**
     * Sets button status.
     *
     * @param id    button resource id
     * @param text  new text
     * @param color new text color
     * @param icon  new left icon
     */
    private void setButtonStatus(@IdRes int id, @NonNull String text,
                                 @ColorRes int color, @DrawableRes int icon) {
        post(() -> {
            //Gets button object
            android.widget.Button btn = activity.findViewById(id);
            //sets button object
            btn.setText(text);
            //sets button color
            btn.setTextColor(ContextCompat.getColor(activity, color));
            //sets button left icon
            btn.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);
        });
    }

    /**
     * Returns the permission manager object.
     *
     * @return permission manager object
     */
    @NonNull
    public PermissionManager getPermissionManager() {
        return permissionManager;
    }
}
