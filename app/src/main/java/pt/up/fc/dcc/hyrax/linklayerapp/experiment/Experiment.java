/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.experiment;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.Observer;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.outcome.Error;
import org.hyrax.link.misc.outcome.Success;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.util.Collection;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkController;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LogHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.SocketComm;

/**
 * Abstract experiment class.
 */
public abstract class Experiment<F extends LinkFeatures> {
    /**
     * Done callback.
     */
    public interface DoneCallback {
        /**
         * Triggered when experiment is done.
         *
         * @param outcome outcome object telling the experiment has successfully conclude or not
         */
        void onDone(@NonNull Outcome<?> outcome);
    }

    final LinkHelper<F> linkHelper;
    final boolean server;
    private int transferSize;

    /**
     * Constructor.
     *
     * @param linkHelper link helper object
     * @param server     <tt>true</tt> if server, <tt>false</tt> otherwise
     */
    Experiment(@NonNull LinkHelper<F> linkHelper, boolean server) {
        this.linkHelper = linkHelper;
        this.server = server;
        this.transferSize = 10000000;//10MB
    }

    /**
     * Changes the size of transference.
     *
     * @param size new transference size
     */
    void setTransferSize(int size) {
        this.transferSize = size;
    }

    /**
     * Returns custom server properties.
     *
     * @return server properties object
     */
    abstract ServerProperties getServerProperties();

    /**
     * Returns custom visibility properties.
     *
     * @return visibility properties object
     */
    abstract VisibilityProperties getVisibilityProperties();

    /**
     * Returns custom discovery properties.
     *
     * @return visibility discovery object
     */
    abstract DiscoveryProperties getDiscoveryProperties();

    /**
     * Returns custom connection properties.
     *
     * @param device device object to connect to
     * @return connection properties
     */
    abstract ConnectionProperties getConnectionProperties(@NonNull Device device);

    /**
     * Returns the socket object in order to perform communication.
     *
     * @param device remote device
     * @return socket comm object
     */
    abstract SocketComm getSocketObject(@NonNull Device device);

    /**
     * Performs the experiment.
     *
     * @param doneCallback action done object
     */
    public void perform(@NonNull DoneCallback doneCallback) {
        if (linkHelper.getLink().isEnabled()) {
            doneCallback.onDone(new Error<>(
                    LinkException.build(new Exception(
                            "Please turn off " + linkHelper.getLink().getTechnology() + " before starting")), ""));
            return;
        }
        //first thing - enables the hardware
        linkHelper.toggleHardware(outcome -> {
            //is the enabling fails - ERROR
            outcome.ifError(arg -> doneCallback.onDone(outcome));
            outcome.ifSuccess(hwOut -> {
                outcome.ifError(error -> doneCallback.onDone(outcome));
                outcome.ifSuccess(s -> {
                    if (server) executeServer(doneCallback);
                    else executeClient(doneCallback);
                });
            });
        });
    }

    /**
     * Called when experiment finish.
     */
    public abstract void finish();

    /**
     * Executes experiment - client side.
     *
     * @param doneCallback call when it done
     */
    private void executeClient(@NonNull DoneCallback doneCallback) {
        discoverAndConnect(call -> {
            //if discovery and connect fails - ERROR
            call.ifError(error -> doneCallback.onDone(call));
            call.ifSuccess(connDevice -> {
                SocketComm socketComm = getSocketObject((Device) connDevice);

                socketComm.connect(success -> {
                    //if connection fails - ERROR
                    if (!success)
                        doneCallback.onDone(new Error<>(LinkException.build(
                                new Exception("Socket connection failed")), ""));
                    else
                        executeCommunication(socketComm, 21, done ->
                                doneCallback.onDone(new Success<>("Success!!")));
                });//closes communication
            });//closes connection success
        });
    }

    /**
     * Performs discovery and connect right after.
     *
     * @param result method result callback
     */
    void discoverAndConnect(@NonNull LinkController.ActionResult result) {
        DiscoveryProperties discoveryProperties = getDiscoveryProperties();
        Link link = linkHelper.getLink();
        //if the hardware is discovering - ERROR
        if (link.isDiscovering(discoveryProperties.getIdentifier())) {
            result.onResult(new Error<>(
                    LinkException.build(new Exception("Already discovering")), ""));
            return;
        }

        linkHelper.toggleDiscovery(discoveryProperties, disOut -> {
            //if discovery fails -> ERROR
            disOut.ifError(error -> result.onResult(disOut));
            disOut.ifSuccess(rawDevices -> {
                Collection<Device> devices = (Collection<Device>) rawDevices;
                //if no devices discovered - ERROR
                if (devices.size() == 0) {
                    result.onResult(new Error<>(
                            LinkException.build(new Exception("No devices discovered")), ""));
                    return;
                }
                //get the first device
                Device device = devices.iterator().next();
                ConnectionProperties connectionProperties = getConnectionProperties(device);
                connect(device, connectionProperties, result::onResult);
            });//closes discovery success
        });//closes discovery
    }

    /**
     * Performs a few communication tests.
     *
     * @param socketComm    socket comm object
     * @param numberOfPings number of pings
     * @param listener      call when it done
     */
    private void executeCommunication(@NonNull SocketComm socketComm,
                                      int numberOfPings,
                                      @NonNull Observer<Boolean> listener) {

        if (numberOfPings == 0) {
            listener.onEvent(true);
            return;
        }

        int size = (numberOfPings == 1) ? transferSize : 0;
        LogHelper.log(LogHelper.Type.PING_REQ, size);
        socketComm.sendMessage(SocketComm.Type.PING, size, type -> {
            LogHelper.log(LogHelper.Type.PING_RESP, size);
            executeCommunication(socketComm, numberOfPings - 1, listener);
        });
    }

    /**
     * Connects to a remote device.
     *
     * @param device               remote device
     * @param connectionProperties connection properties object
     * @param result               action result object
     */
    void connect(@NonNull Device device, @NonNull ConnectionProperties connectionProperties,
                 @NonNull LinkController.ActionResult result) {
        linkHelper.toggleConnection(device, connectionProperties, result);
    }

    /**
     * Executes experiment - server side.
     *
     * @param doneCallback call when it done
     */
    void executeServer(@NonNull DoneCallback doneCallback) {
        ServerProperties serverProperties = getServerProperties();
        String advId = getVisibilityProperties().getIdentifier();
        Link link = linkHelper.getLink();
        //if the hardware is accepting connections - ERROR
        if (link.isAccepting(serverProperties.getIdentifier())) {
            doneCallback.onDone(new Error<>(
                    LinkException.build(new Exception("Already accepting connections")), ""));
            return;
        }
        //if the hardware is visible - ERROR
        if (link.isVisible(advId)) {
            doneCallback.onDone(new Error<>(
                    LinkException.build(new Exception("Already visible")), ""));
            return;
        }

        linkHelper.toggleAccept(serverProperties, acceptOut -> {
            //if acceptance fails - ERROR
            acceptOut.ifError(arg -> doneCallback.onDone(acceptOut));
            acceptOut.ifSuccess(accept ->
                    linkHelper.toggleVisibility(getVisibilityProperties(), visOut -> {
                        //if visibility fails - ERROR
                        visOut.ifError(error -> doneCallback.onDone(visOut));
                        visOut.ifSuccess(id -> doneCallback.onDone(new Success<>("Success!")));
                    })//closes visibility
            );//closes accept success
        });//closes accept
    }
}
