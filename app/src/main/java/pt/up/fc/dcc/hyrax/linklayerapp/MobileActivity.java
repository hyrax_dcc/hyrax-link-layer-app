/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.mobile.MobileLink;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.HandlerHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkUiActionsListener;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.PermissionManager;

public class MobileActivity extends AppCompatActivity implements LinkUiActionsListener {
    //activity helper
    private HandlerHelper helper;
    //Bluetooth link controller
    private LinkHelper<MobileLink> controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.helper = new HandlerHelper(this);
        this.controller = new LinkHelper<>(this, helper, Technology.MOBILE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mobile, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_WRITE_STORAGE,
                granted -> {
                    if (!granted) {
                        helper.error("Need write permissions to log");
                        helper.toastLong("Permissions are needed in order to log traces");
                    }
                });

        boolean toStart = getIntent().getBooleanExtra(
                MainActivity.START_LINK_SERVICE, false);
        if (toStart) {
            //starts the controller
            controller.start();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        helper.getPermissionManager().onPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onStop() {
        super.onStop();
        controller.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller.stop();
    }

    @Override
    public void onActionAbout(MenuItem menuItem) {
        ActivityHelper.showDialogWithView(this, R.layout.about_mobile);
    }

    @Override
    public void onActionStop(MenuItem menuItem) {
        finishAndRemoveTask();
    }

    @Override
    public void onActionAuto(View view) {
        helper.error("Not implemented!");
    }

    @Override
    public void onActionExecMode(View view) {
        controller.toggleMode();
    }

    @Override
    public void onActionHardware(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("The activation/deactivation of deactivation hardware must be done manually.")
                .setTitle("Enable/Disable Mobile Data")
                .setCancelable(false)
                .setPositiveButton("Go Settings",
                        (dialog, id) -> {
                            Intent i = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                            startActivity(i);
                        }
                )
                .setNegativeButton("Cancel",
                        (dialog, id) -> dialog.dismiss()
                );
        builder.create().show();
    }

    @Override
    public void onActionDiscovery(View view) {
        DiscoveryProperties properties = controller.getLink().getFeatures()
                .newDiscoveryBuilder().build();
        controller.toggleDiscovery(properties);
    }

    @Override
    public void onActionVisibility(View view) {
        VisibilityProperties properties = controller.getLink().getFeatures()
                .newVisibilityBuilder().build();
        controller.toggleVisibility(properties);
    }

    @Override
    public void onActionConnection(View view) {
        helper.error("No devices discovered");
    }

    @Override
    public void onActionAcceptance(View view) {
        ServerProperties properties = controller.getLink().getFeatures()
                .newServerBuilder().build();
        controller.toggleAccept(properties);
    }
}
