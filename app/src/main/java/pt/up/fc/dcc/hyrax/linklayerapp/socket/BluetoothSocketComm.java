/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.socket;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;

import org.hyrax.link.bluetooth.legacy.comm.BluetoothComm;
import org.hyrax.link.misc.Observer;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityComm;

/**
 * Bluetooth communication client.
 */
public class BluetoothSocketComm implements SocketComm {
    @NonNull
    private final ActivityComm activityComm;
    @NonNull
    private final BluetoothComm bluetoothComm;
    @NonNull
    private static final Map<BluetoothComm, BluetoothSocketComm> instances = new HashMap<>();
    @NonNull
    private final SparseArray<Observer<Type>> listeners;
    //current message identifier
    private int msgId;
    //connection ready object
    private final Object READY_LOCK = new Object();
    @Nullable
    private Observer<Boolean> readyListener;
    private boolean isReady;

    /**
     * Constructor.
     * Each bluetooth can have only one listener then this is why we need to keep this static.
     *
     * @param activityComm  activity communication handler
     * @param bluetoothComm bluetooth socket object
     */
    private BluetoothSocketComm(@NonNull ActivityComm activityComm, @NonNull BluetoothComm bluetoothComm) {
        this.activityComm = activityComm;
        this.bluetoothComm = bluetoothComm;
        this.listeners = new SparseArray<>();
        this.msgId = 0;
        this.isReady = false;

        //listens when the input stream is ready or if the socket has been closed
        this.bluetoothComm.setCommListener(new BluetoothComm.CommListener() {
            @Override
            public void onInputStreamReady(@NonNull InputStream inputStream) throws IOException {
                synchronized (READY_LOCK) {
                    isReady = true;
                    if (readyListener != null)
                        readyListener.onEvent(true);
                }
                DataInputStream in = new DataInputStream(inputStream);
                boolean run = true;
                while (run) {
                    try {
                        //read message identifier
                        int id = in.readInt();
                        //read message type and transform to type enum object
                        Type t = Type.toType(in.readByte());
                        Objects.requireNonNull(t);
                        //reads the payload size
                        int s = in.readInt();
                        //reads the  remaining message
                        onMessage(id, t, s, in);
                    } catch (EOFException e) {
                        e.printStackTrace();
                        run = false;
                    }
                }
            }

            @Override
            public void onStop() {
                instances.remove(bluetoothComm);
            }
        });
    }

    /**
     * Process a received message
     *
     * @param msgId       message identifier
     * @param type        message type
     * @param payloadSize the remaining message size
     * @param inputStream input stream object
     * @throws IOException throws is any read fails
     */
    private void onMessage(int msgId, Type type, int payloadSize, @NonNull InputStream inputStream) throws IOException {
        activityComm.debug(String.format(Locale.ENGLISH,
                "Rcv Message (Id: %d) of Type: %s payload size: %s",
                msgId, type, SocketComm.sizeToString(payloadSize)));
        switch (type) {
            case PING:
                read(inputStream, payloadSize);
                Objects.requireNonNull(bluetoothComm.getOutputStream());
                write(bluetoothComm.getOutputStream(), msgId, Type.PONG.getId(), 0);
                break;

            case PONG:
                Observer<Type> evt = listeners.get(msgId, tp -> {
                });
                evt.onEvent(type);
                listeners.delete(msgId);
                break;
            default:
                throw new RuntimeException("Undefined action " + type);
        }
    }

    /**
     * Gets an instance of BluetoothSocketComm given a bluetooth comm.
     *
     * @param activityComm  activity communication handler
     * @param bluetoothComm bluetooth socket object
     * @return BluetoothSocketComm object
     */
    public static BluetoothSocketComm getInstance(@NonNull ActivityComm activityComm,
                                                  @NonNull BluetoothComm bluetoothComm) {
        BluetoothSocketComm client = instances.get(bluetoothComm);
        if (client == null) {
            client = new BluetoothSocketComm(activityComm, bluetoothComm);
            instances.put(bluetoothComm, client);
        }
        return client;
    }

    @Override
    public void connect(@NonNull Observer<Boolean> listener) {
        synchronized (READY_LOCK) {
            if (isReady)
                listener.onEvent(true);
            readyListener = listener;
        }
    }

    @Override
    public void disconnect() {

    }

    @Override
    public void sendPingMultiple(int number) {
        if (number == 0)
            return;
        final long start_ts = System.currentTimeMillis();
        sendMessage(Type.PING, 0, tp -> {
            long t = System.currentTimeMillis() - start_ts;
            activityComm.info(String.format(Locale.ENGLISH,
                    "Msg of type: %s RTT: %s Speed: %s",
                    tp,
                    SocketComm.timeToString(t),
                    SocketComm.speedToString(SocketComm.getSpeedPerSecond(0, t))));
            sendPingMultiple(number - 1);
        });
    }

    @Override
    public void sendMessage(@NonNull Type type, int payloadSize) {
        final long start_ts = System.currentTimeMillis();
        sendMessage(type, payloadSize, tp -> {
            long t = System.currentTimeMillis() - start_ts;
            activityComm.info(String.format(Locale.ENGLISH,
                    "Msg of type: %s RTT: %s Speed: %s",
                    tp,
                    SocketComm.timeToString(t),
                    SocketComm.speedToString(SocketComm.getSpeedPerSecond(payloadSize, t))));
        });
    }

    @Override
    public void sendMessage(@NonNull Type type, int payloadSize, @NonNull Observer<Type> listener) {
        new Thread(() -> {
            try {
                activityComm.debug(String.format(Locale.ENGLISH,
                        "Sending message (Id: %d) of Type: %s with size: %s",
                        msgId, type, SocketComm.sizeToString(payloadSize)));
                Objects.requireNonNull(bluetoothComm.getOutputStream());

                final int id = msgId;
                //adds a listener for response
                listeners.append(id, listener);

                write(bluetoothComm.getOutputStream(), msgId, type.getId(), payloadSize);
                //increments message identifier
                msgId += 1;
            } catch (IOException e) {
                e.printStackTrace();
                activityComm.error("Error sending message " + e.getMessage());
            }
        }).start();
    }
}
