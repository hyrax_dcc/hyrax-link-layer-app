/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Logger class implementation.
 */
public class LogHelper {
    //log helper unique instance
    private static LogHelper logHelper;
    //java logger object
    private final Logger logger;
    //file to log
    private final FileHandler fileHandler;
    //executor thread that takes care of log stuff burden
    private final ScheduledExecutorService executor;

    /**
     * Log action classification
     */
    public enum Type {
        BATTERY,
        MEMORY,
        CPU_PID_STAT,
        CPU_STAT,

        EVENT_TRIGGER,

        HARDWARE_CALL_BEGIN,
        HARDWARE,
        HARDWARE_CALL_END,

        DISCOVERY_CALL_BEGIN,
        DISCOVERING,
        DISCOVERY_DONE,
        DISCOVERY_CALL_END,

        VISIBILITY_CALL_BEGIN,
        VISIBLE,
        VISIBILITY_DONE,
        VISIBILITY_CALL_END,

        CONNECTION_CALL_BEGIN,
        CONNECTED,
        CONNECTION_CALL_END,

        ACCEPT_CALL_BEGIN,
        ACCEPTING,
        ACCEPT_CALL_END,

        PING_REQ,
        PING_RESP,
    }

    /**
     * Singleton constructor.
     *
     * @param filePath log file path
     */
    private LogHelper(@NonNull String filePath) throws IOException {
        this.logger = Logger.getLogger("LogHelper");

        //file handler responsible for writing to a file
        this.fileHandler = new FileHandler(filePath);
        //write format - as txt custom
        fileHandler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                // new Date(record.getMillis()),
                return String.format(Locale.ENGLISH, "%d %s%s\n",
                        record.getMillis(),
                        record.getMessage(),
                        paramsToString(record.getParameters()));
            }

            /**
             * Transforms an array of objects to string.
             * @param params objects array
             * @return string format
             */
            private String paramsToString(Object[] params) {
                StringBuilder argsBuilder = new StringBuilder();
                if (params != null && params.length > 0) {
                    argsBuilder.append(' ');
                    argsBuilder.append(params[0].toString());
                    for (int i = 1; i < params.length; i++) {
                        argsBuilder.append(", ");
                        argsBuilder.append(params[i].toString());
                    }
                }
                return argsBuilder.toString();
            }
        });
        logger.addHandler(fileHandler);
        this.executor = Executors.newSingleThreadScheduledExecutor();
    }

    /**
     * Creates a new instance if logger object.
     * Note: Only ONE instance allowed. If you try to get more than one instance it will throw an
     * RuntimeException
     *
     * @param filePath file log path
     */
    public static void newInstance(@NonNull String filePath) {
        if (logHelper == null) {
            try {
                logHelper = new LogHelper(filePath);
                return;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        throw new RuntimeException("LogHelper already instantiated");
    }

    /**
     * Logs a message of a certain type with specific arguments
     *
     * @param type log type - taxonomy
     * @param data log extra data
     */
    public static void log(@NonNull Type type, Object... data) {
        exec(() -> logHelper.logger.log(Level.INFO, type.name(), data));
    }

    /**
     * Closes the logger and forces to write everything from memory to file disk.
     */
    public static void destroy() {
        exec(() -> {
            //closes the executor service and waits for all work to be done
            logHelper.executor.shutdown();
            //flushes the logs to file
            logHelper.fileHandler.flush();
            logHelper.fileHandler.close();
            logHelper.logger.removeHandler(logHelper.fileHandler);
            //destroy
            logHelper = null;
        });
    }

    /**
     * Logs all meaningful statistics about the current process, cpu, memory and
     * also battery consumptions, every {@param period} fixed time interval.
     *
     * @param context application context
     * @param period  fixed delay interval period in milliseconds.
     */
    public static void logStats(@NonNull Context context, long period) {
        //get current process identifier
        final int processPid = android.os.Process.myPid();
        Log.w("My Process Id", "" + processPid);
        exec(() -> logHelper.executor.scheduleWithFixedDelay(
                () -> {
                    //logs battery
                    logBatteryStats(context);
                    //logs cpu
                    logCpuStats(processPid);
                    //logs memory
                    logMemoryStats(processPid);

                }, 0, period, TimeUnit.MILLISECONDS));
    }

    /**
     * Reads the battery stats.
     *
     * @param context application context
     * @link https://source.android.com/devices/tech/power/device
     */
    private static void logBatteryStats(@NonNull Context context) {
        BatteryManager batteryManager = (BatteryManager) context.getSystemService(
                Context.BATTERY_SERVICE);
        Objects.requireNonNull(batteryManager);
        //current in microamperes,
        int currentNow = batteryManager
                .getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW);
        //capacity in microampere-hours
        int chargeCounter = batteryManager
                .getIntProperty(BatteryManager.BATTERY_PROPERTY_CHARGE_COUNTER);
        //energy in nanowatt-hours
        long energyCounter = batteryManager
                .getLongProperty(BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER);
        //gets battery voltage
        Intent intent = context.registerReceiver(
                null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        Objects.requireNonNull(intent);
        long voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
        //log
        log(Type.BATTERY, currentNow, chargeCounter, energyCounter, voltage);
    }

    /**
     * Reads the cpu usage.
     * More details in @link http://man7.org/linux/man-pages/man5/proc.5.html
     *
     * @param pid process identifier
     */
    private static void logCpuStats(int pid) {
        try (BufferedReader r1 = new BufferedReader(new FileReader("/proc/" + pid + "/stat"));
             BufferedReader r2 = new BufferedReader(new FileReader("/proc/stat"))) {
            log(Type.CPU_PID_STAT, r1.readLine());
            log(Type.CPU_STAT, r2.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads the memory usage.
     * More details in @link http://man7.org/linux/man-pages/man5/proc.5.html
     *
     * @param pid process identifier
     */
    private static void logMemoryStats(int pid) {
        try (BufferedReader r = new BufferedReader(new FileReader("/proc/" + pid + "/statm"))) {
            log(Type.MEMORY, r.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Executes a runnable on current thread.
     * This allows to execute abstract function by checking some parameters first
     *
     * @param runnable run object
     */
    private static void exec(@NonNull Runnable runnable) {
        if (logHelper != null)
            runnable.run();
    }
}
