/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.experiment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.outcome.Error;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.types.WifiScanFilter;

import java.util.Collection;
import java.util.Objects;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.SocketComm;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketComm;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketServer;

/**
 * Wifi Direct experiment.
 */
public class WifiDirectExperiment extends Experiment<WifiDirectLink> {
    @Nullable
    private WifiSocketServer socketServer;
    @Nullable
    private WifiSocketComm socketComm;

    /**
     * Constructor.
     *
     * @param linkHelper link helper object
     * @param server     <tt>true</tt> if server, <tt>false</tt> otherwise
     */
    public WifiDirectExperiment(@NonNull LinkHelper<WifiDirectLink> linkHelper, boolean server) {
        super(linkHelper, server);
        if (server)
            this.socketServer = new WifiSocketServer(linkHelper.getActivityComm(),
                    WifiSocketServer.DEFAULT_PORT);
    }


    @Override
    ServerProperties getServerProperties() {
        return linkHelper.getLink().getFeatures().newServerBuilder().build();
    }

    @Override
    VisibilityProperties getVisibilityProperties() {
        return linkHelper.getLink().getFeatures()
                .newVisibilityBuilder().build();
    }

    @Override
    DiscoveryProperties getDiscoveryProperties() {
        return linkHelper.getLink().getFeatures()
                .newDiscoveryBuilder()
                .setScannerFilter(WifiScanFilter.newBuilder()
                        .setFilter(device -> device.getName().startsWith("Android_"))
                        .build())
                .setStopRule(devices -> devices.size() > 0)
                .build();
    }

    @Override
    ConnectionProperties getConnectionProperties(@NonNull Device device) {
        return linkHelper.getLink().getFeatures()
                .newConnectionBuilder(device).build();
    }

    @Override
    SocketComm getSocketObject(@NonNull Device device) {
        this.socketComm = new WifiSocketComm(linkHelper.getActivityComm(),
                Constants.DEFAULT_GO_IP, WifiSocketServer.DEFAULT_PORT);
        socketComm.setConnectionAttemptWaitInterval(500);
        return socketComm;
    }

    @Override
    public void finish() {
        if (socketComm != null)
            socketComm.disconnect();
        if (socketServer != null)
            socketServer.stop();

        Collection<Device> devices = linkHelper.getLink().getConnected();
        for (Device device : devices)
            linkHelper.toggleConnection(device, getConnectionProperties(device));
    }

    @Override
    void executeServer(@NonNull DoneCallback doneCallback) {
        Objects.requireNonNull(socketServer);
        socketServer.setReadyListener(ready -> {
            if (!ready) {
                doneCallback.onDone(new Error<>(LinkException.build(
                        new Exception("Error starting server")), ""));
                return;
            }
            super.executeServer(doneCallback);
        });
        socketServer.start();
    }
}
