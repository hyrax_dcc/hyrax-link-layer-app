/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.hyrax.link.Device;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.WifiLink;
import org.hyrax.link.wifi.direct.WifiDirectCredentials;
import org.hyrax.link.wifi.types.ScanDataWifi;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiAdvertiseSettings;
import org.hyrax.link.wifi.types.WifiConnectionSettings;
import org.hyrax.link.wifi.types.WifiScanFilter;

import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import pt.up.fc.dcc.hyrax.linklayerapp.experiment.AdbBroadcast;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.AdbBroadcastListener;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.Experiment;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.WifiDirectExperiment;
import pt.up.fc.dcc.hyrax.linklayerapp.experiment.WifiDirectLegacyExperiment;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.ActivityHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.HandlerHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkUiActionsListener;
import pt.up.fc.dcc.hyrax.linklayerapp.helpers.PermissionManager;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketComm;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.WifiSocketServer;

public class WifiDirectActivity extends AppCompatActivity implements LinkUiActionsListener, AdbBroadcastListener {
    //activity helper
    private HandlerHelper helper;
    //Bluetooth link controller
    private LinkHelper<WifiDirectLink> controller;
    private LinkHelper<WifiLink> wifiController;
    //adb commands receiver
    private AdbBroadcast adbBroadcast;
    private WifiSocketServer wifiSocketServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_p2p);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.helper = new HandlerHelper(this);
        this.controller = new LinkHelper<>(this, helper, Technology.WIFI_DIRECT);
        this.wifiController = new LinkHelper<>(this, helper, Technology.WIFI);
        this.adbBroadcast = new AdbBroadcast(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_wifi_p2p, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_WRITE_STORAGE,
                granted -> {
                    if (!granted) {
                        helper.error("Need write permissions to log");
                        helper.toastLong("Permissions are needed in order to log traces");
                    }
                });

        boolean toStart = getIntent().getBooleanExtra(
                MainActivity.START_LINK_SERVICE, false);
        if (toStart) {
            //starts wifi socket server
            wifiSocketServer = new WifiSocketServer(helper, WifiSocketServer.DEFAULT_PORT);
            wifiSocketServer.start();
            //enables logger
            controller.enableLog(AdbBroadcast.BASE_PATH + "app.log");
            //starts the controller
            controller.start();
            //
            wifiController.toggleMode();
        }
        //TODO - remove
        String exp = getIntent().getStringExtra(MainActivity.EXPERIMENT);
        String log = getIntent().getStringExtra(MainActivity.LOG_FILE_NAME);
        if (exp != null && log != null) {
            onExperimentStart(exp, log);
        }
        //register broadcast receiver
        registerReceiver(adbBroadcast, adbBroadcast.getIntentFilters());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        helper.getPermissionManager().onPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (wifiSocketServer != null)
            wifiSocketServer.stop();

        controller.stop();
        wifiController.stop();
        //unregisters broadcast receiver
        unregisterReceiver(adbBroadcast);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller.stop();
        wifiController.stop();
    }

    @Override
    public void onActionAbout(MenuItem menuItem) {
        ActivityHelper.showDialogWithView(this, R.layout.about_wifi_p2p);
    }

    @Override
    public void onActionStop(MenuItem menuItem) {
        //TODO - to remove if
        if (experiment != null) {
            onExperimentStop();
            return;
        }
        finishAndRemoveTask();
    }

    @Override
    public void onActionAuto(View view) {
        String[] options = {"Ping - Default Port Go", "Ping Multiple - GO"};
        int size1 = controller.getLink().getConnected().size();
        int size2 = wifiController.getLink().getConnected().size();
        if (size1 == 0 && size2 == 0) {
            helper.error("Not connected to Wifi");
            return;
        }

        ActivityHelper.showOptionsDialog(this, "Select Action", options,
                result -> {
                    switch (result) {
                        case 0:
                            ActivityHelper.showDialogWithView(WifiDirectActivity.this,
                                    R.layout.ping_select, "Enter ping payload size",
                                    dialogView -> {
                                        EditText text = dialogView.findViewById(R.id.payload_size);
                                        String value = text.getText().toString();
                                        WifiSocketComm socketComm = new WifiSocketComm(helper,
                                                Constants.DEFAULT_GO_IP, WifiSocketServer.DEFAULT_PORT);
                                        socketComm.connect(success -> {
                                            if (!success) {
                                                helper.error("Error connection to " + Constants.DEFAULT_GO_IP);
                                                return;
                                            }
                                            socketComm.sendPing(Integer.valueOf(value));
                                        });
                                    });
                            break;
                        case 1:
                            ActivityHelper.showDialogWithView(WifiDirectActivity.this,
                                    R.layout.ping_select, "Enter the number of pings",
                                    dialogView -> {
                                        EditText text = dialogView.findViewById(R.id.payload_size);
                                        String value = text.getText().toString();
                                        WifiSocketComm socketComm = new WifiSocketComm(helper,
                                                Constants.DEFAULT_GO_IP, WifiSocketServer.DEFAULT_PORT);
                                        socketComm.connect(success -> {
                                            if (!success) {
                                                helper.error("Error connection to " + Constants.DEFAULT_GO_IP);
                                                return;
                                            }
                                            socketComm.sendPingMultiple(Integer.valueOf(value));
                                        });
                                    });
                            break;
                        default:
                            throw new RuntimeException("Undefined test " + result);
                    }
                });
    }

    @Override
    public void onActionExecMode(View view) {
        controller.toggleMode();
    }

    @Override
    public void onActionHardware(View view) {
        controller.toggleHardware();
    }

    @Override
    public void onActionDiscovery(View view) {
        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_LOCATION,
                granted -> {
                    if (!granted) {
                        helper.error("No enough permissions to continue");
                        return;
                    }
                    String[] options = {"Default", "Simple - Service", "Aps - Service"};
                    ActivityHelper.showOptionsDialog(this, "Select Type", options,
                            result -> {
                                DiscoveryProperties<Void, WifiScanFilter> properties;
                                switch (result) {
                                    case 0:
                                        //wifi networks scan
                                        properties = controller.getLink().getFeatures()
                                                .newDiscoveryBuilder().build();
                                        break;
                                    case 1:
                                        //wifi scan services
                                        properties = controller.getLink().getFeatures()
                                                .newDiscoveryBuilder("service_example")
                                                .build();
                                        break;
                                    case 2:
                                        properties = controller.getLink().getFeatures()
                                                .newDiscoveryBuilder("service_ap")
                                                .build();
                                        break;
                                    default:
                                        throw new RuntimeException("Undefined option " + result);
                                }
                                controller.toggleDiscovery(properties);
                            });
                });
    }

    @Override
    public void onActionVisibility(View view) {
        helper.getPermissionManager().requestPermission(PermissionManager.PERMISSION_LOCATION,
                granted -> {
                    if (!granted) {
                        helper.error("No enough permissions to continue");
                        return;
                    }
                    String[] options = {"Default", "Advertise Service", "Advertise Service - Force Scan",
                            "Advertise Ap"};
                    ActivityHelper.showOptionsDialog(this, "Select Type", options,
                            result -> {
                                VisibilityProperties<WifiAdvertiseSettings, WifiAdvertiseData> properties;
                                switch (result) {
                                    case 0:
                                        properties = controller.getLink().getFeatures()
                                                .newVisibilityBuilder().build();
                                        break;
                                    case 1:
                                        WifiAdvertiseData data = new WifiAdvertiseData();
                                        data.addData("key1", "val1");
                                        data.addData("key2", "val2");
                                        properties = controller.getLink().getFeatures()
                                                .newVisibilityBuilder(
                                                        "service_example",
                                                        wifiSocketServer.getListenPort(),
                                                        data)
                                                .build();
                                        break;
                                    case 2:
                                        data = new WifiAdvertiseData();
                                        data.addData("key1", "val1");
                                        data.addData("key2", "val2");
                                        properties = controller.getLink().getFeatures()
                                                .newVisibilityBuilder(
                                                        "service_example",
                                                        wifiSocketServer.getListenPort(),
                                                        false,
                                                        data
                                                ).build();
                                        break;
                                    case 3:
                                        ServerProperties serverProperties = controller.getLink()
                                                .getFeatures().newServerBuilder().build();
                                        if (!controller.getLink().isAccepting(serverProperties.getIdentifier())) {
                                            helper.error("Please create a group first");
                                            return;
                                        }
                                        WifiDirectCredentials credentials = controller
                                                .getLink()
                                                .getFeatures()
                                                .getWifiP2pCredentials(serverProperties.getIdentifier());
                                        Objects.requireNonNull(credentials);
                                        data = new WifiAdvertiseData();
                                        data.addData("SSID", credentials.getSSID());
                                        data.addData("PASS", credentials.getPassword());
                                        properties = controller.getLink().getFeatures()
                                                .newVisibilityBuilder(
                                                        "service_ap",
                                                        wifiSocketServer.getListenPort(),
                                                        false,
                                                        data
                                                ).build();
                                        break;

                                    default:
                                        throw new RuntimeException("Undefined option " + result);
                                }
                                controller.toggleVisibility(properties);
                            });
                });
    }

    @Override
    public void onActionConnection(View view) {
        //merge discovered devices with connected
        HashSet<Device> devicesHash = new HashSet<>(controller.getDevicesDiscovered());
        devicesHash.addAll(controller.getLink().getConnected());

        Device[] devices = devicesHash.toArray(new Device[devicesHash.size()]);
        if (devices.length == 0) {
            helper.warning("No devices discovered. Empty list devices!!");
            return;
        }

        final String[] devicesOptions = ActivityHelper.devicesToStr(devices);
        ActivityHelper.showOptionsDialog(this, "Select a device", devicesOptions,
                result -> {
                    String[] options = {"Default", "Legacy Wifi"};
                    Device dev = devices[result];
                    ActivityHelper.showOptionsDialog(this, "Select type", options,
                            which -> {
                                switch (which) {
                                    case 0:
                                        ConnectionProperties<WifiConnectionSettings> properties =
                                                controller.getLink().getFeatures()
                                                        .newConnectionBuilder(dev).build();
                                        controller.toggleConnection(dev, properties);
                                        break;
                                    case 1:
                                        if (dev.getScanData().size() == 0) {
                                            helper.error("Service ap data - unavailable");
                                            return;
                                        }
                                        ScanDataWifi dataWifi = (ScanDataWifi)
                                                dev.getScanData().iterator().next();
                                        Map<String, String> data = dataWifi.getOriginalObject().getExtraData();
                                        String ssid = data.get("SSID");
                                        String pass = data.get("PASS");

                                        Objects.requireNonNull(ssid);
                                        Objects.requireNonNull(pass);
                                        Device connDevice = wifiController.getLink()
                                                .getFeatures().toWifiNode(dev, ssid);
                                        ConnectionProperties<WifiConnectionSettings> legacyProperties =
                                                wifiController.getLink().getFeatures()
                                                        .newConnectionBuilder(connDevice)
                                                        .removeWhenDisconnect(true)
                                                        .setPassword(pass).build();

                                        wifiController.toggleConnection(connDevice, legacyProperties);
                                        break;
                                    default:
                                        throw new RuntimeException("Undefined type " + which);
                                }
                            });//close which type
                });//closes select device
    }

    @Override
    public void onActionAcceptance(View view) {
        ServerProperties serverProperties = controller.getLink().getFeatures().newServerBuilder().build();
        controller.toggleAccept(serverProperties);
    }

    @Nullable
    private Experiment experiment;

    @Override
    public void onExperimentStart(@NonNull String exp, @NonNull String logFileName) {
        helper.info(String.format(Locale.ENGLISH, "Starting experiment %s and logging to %s", exp, logFileName));

        //startup
        controller.enableLog(AdbBroadcast.BASE_PATH + logFileName);
        controller.start();

        switch (exp) {
            case AdbBroadcast.EXP_WIFI_DIRECT_SERVER:
                experiment = new WifiDirectExperiment(controller, true);
                break;
            case AdbBroadcast.EXP_WIFI_DIRECT:
                experiment = new WifiDirectExperiment(controller, false);
                break;
            case AdbBroadcast.EXP_WIFI_DIRECT_LEGACY_SERVER:
                wifiController.toggleMode();
                experiment = new WifiDirectLegacyExperiment(controller, wifiController, true);
                break;
            case AdbBroadcast.EXP_WIFI_DIRECT_LEGACY:
                wifiController.toggleMode();
                experiment = new WifiDirectLegacyExperiment(controller, wifiController, false);
                break;
            default:
                throw new RuntimeException("Undefined experiment " + exp);
        }
        experiment.perform(outcome -> {
            outcome.ifError(error -> {
                helper.error(outcome.getError().getMessage());
                adbBroadcast.propertiesManager.setProperties(
                        AdbBroadcast.BARRIER_DONE, Boolean.toString(false));
            });
            outcome.ifSuccess(arg -> {
                helper.success("Experiment successfully done!!");
                adbBroadcast.propertiesManager.setProperties(
                        AdbBroadcast.BARRIER_DONE, Boolean.toString(true));
                //TODO - to remove
                if (AdbBroadcast.EXP_WIFI_DIRECT.equals(exp) || AdbBroadcast.EXP_WIFI_DIRECT_LEGACY.equals(exp)) {
                    onExperimentStop();
                }
            });
        });
    }

    @Override
    public void onExperimentStop() {
        if (experiment != null)
            experiment.finish();

        controller.toggleHardware(result -> {
            controller.stop();
            adbBroadcast.propertiesManager.close();
            //closes activity
            helper.post(this::finishAndRemoveTask);
        });
    }
}