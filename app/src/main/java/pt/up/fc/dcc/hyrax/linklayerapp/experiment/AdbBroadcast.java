/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.experiment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;
import java.util.Properties;

/**
 * Class responsible for listen intents from command line (adb).
 */
public class AdbBroadcast extends BroadcastReceiver {
    public static final String BASE_PATH = "/storage/emulated/0/Download/";
    //barrier file name
    private static final String BARRIER_FILE = BASE_PATH + "file.barrier";
    //barrier keys
    public static final String BARRIER_DONE = "done";

    //intent arguments
    private static final String ARG_EXP = "ARG_EXP";
    private static final String ARG_LOG_FILE_NAME = "ARG_LOG_FILE_NAME";

    //intent values
    public static final String EXP_IDLE_SERVER = "EXP_IDLE_SERVER";
    public static final String EXP_IDLE = "EXP_IDLE";
    public static final String EXP_IDLE_BLUETOOTH_SERVER = "EXP_IDLE_BLUETOOTH_SERVER";
    public static final String EXP_IDLE_BLUETOOTH = "EXP_IDLE_BLUETOOTH";
    public static final String EXP_BLUETOOTH_SERVER = "EXP_BLUETOOTH_SERVER";
    public static final String EXP_BLUETOOTH = "EXP_BLUETOOTH";
    public static final String EXP_BLUETOOTH_LE_SERVER = "EXP_BLUETOOTH_LE_SERVER";
    public static final String EXP_BLUETOOTH_LE = "EXP_BLUETOOTH_LE";
    public static final String EXP_IDLE_WIFI_SERVER = "EXP_IDLE_WIFI_SERVER";
    public static final String EXP_IDLE_WIFI = "EXP_IDLE_WIFI";
    public static final String EXP_WIFI_SERVER = "EXP_WIFI_SERVER";
    public static final String EXP_WIFI = "EXP_WIFI";
    public static final String EXP_WIFI_DIRECT_SERVER = "EXP_WIFI_DIRECT_SERVER";
    public static final String EXP_WIFI_DIRECT = "EXP_WIFI_DIRECT";
    public static final String EXP_WIFI_DIRECT_LEGACY_SERVER = "EXP_WIFI_DIRECT_LEGACY_SERVER";
    public static final String EXP_WIFI_DIRECT_LEGACY = "EXP_WIFI_DIRECT_LEGACY";

    /**
     * Intents action definitions
     */
    public enum IntAction {
        EXP_START("android.hyrax.exp_start"),
        EXP_STOP("android.hyrax.exp_stop");

        private String ref;

        /**
         * Constructor.
         *
         * @param ref string identifier
         */
        IntAction(@NonNull String ref) {
            this.ref = ref;
        }

        /**
         * Returns an IntAction object given a string.
         *
         * @param ref string reference
         * @return IntAction object or null of no action found
         */
        @Nullable
        public static IntAction toIntAction(@NonNull String ref) {
            for (IntAction action : IntAction.values())
                if (action.ref.equals(ref))
                    return action;
            return null;
        }
    }


    private final AdbBroadcastListener listener;
    public final PropertiesManager propertiesManager;

    /**
     * Constructor.
     *
     * @param listener adb listener object
     */
    public AdbBroadcast(@NonNull AdbBroadcastListener listener) {
        this.listener = listener;
        PropertiesManager.clean();
        this.propertiesManager = new PropertiesManager();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Objects.requireNonNull(intent.getAction());
        final IntAction action = IntAction.toIntAction(intent.getAction());
        if (action == null)
            throw new RuntimeException("Undefined action " + intent.getAction());
        switch (action) {
            case EXP_START:
                listener.onExperimentStart(
                        getArgString(intent, ARG_EXP),
                        getArgString(intent, ARG_LOG_FILE_NAME));
                break;

            case EXP_STOP:
                listener.onExperimentStop();
                break;

            default:
                throw new RuntimeException("Undefined action " + action);
        }
    }

    /**
     * Extracts and return a string from intent.
     *
     * @param intent intent object
     * @param key    key to retrieve
     * @return string value
     */
    @NonNull
    private String getArgString(@NonNull Intent intent, @NonNull String key) {
        String arg = intent.getStringExtra(key);
        Objects.requireNonNull(arg);
        return arg;
    }


    /**
     * Extracts and return a string from intent. If not exists return the default value.
     *
     * @param intent       intent object
     * @param key          key to retrieve
     * @param defaultValue default value to return in case the key does not exists
     * @return string value
     */
    @NonNull
    private String getArgString(@NonNull Intent intent, @NonNull String key, @NonNull String defaultValue) {
        String arg = intent.getStringExtra(key);
        return (arg == null) ? defaultValue : arg;
    }

    /**
     * Returns the intent filters allowed.
     *
     * @return intent filter object
     */
    public IntentFilter getIntentFilters() {
        IntentFilter filter = new IntentFilter();
        for (IntAction action : IntAction.values())
            filter.addAction(action.ref);
        return filter;
    }

    /**
     * File properties manager.
     */
    public static class PropertiesManager {
        private final FileOutputStream outputStream;

        /**
         * Constructor
         */
        private PropertiesManager() {
            //this.outputStream = new FileOutputStream(BARRIER_FILE, true);
            try {
                this.outputStream = new FileOutputStream(BARRIER_FILE);//no append
            } catch (FileNotFoundException e) {
                throw new RuntimeException("Error open barrier file");
            }
        }

        /*public synchronized static void reset() {
            propertiesManager.clean(true);
        }*/

        /**
         * Changes a list of properties.
         *
         * @param pairs properties (key, value) paris to be set
         */
        public synchronized void setProperties(String... pairs) {
            if (pairs.length % 2 != 0)
                throw new RuntimeException("The argument arity must be pair");

            Properties properties = new Properties();
            for (int i = 0; i < pairs.length; i += 2)
                properties.setProperty(pairs[i], pairs[i + 1]);
            store(properties);
        }

        /**
         * Stores the object properties on file.
         *
         * @param properties object properties
         */
        private void store(@NonNull Properties properties) {
            try {
                properties.store(outputStream, null);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Error storing properties values");
            }
        }

        /**
         * Closes the file output stream.
         */
        public void close() {
            try {
                outputStream.close();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }

        /**
         * Cleans the properties file.
         */
        private static void clean() {
            try (PrintWriter writer = new PrintWriter(BARRIER_FILE)) {
                writer.print("");
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
