/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.helpers;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.LinkSync;
import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Synchronous link control.
 * The action must run on different thread, otherwise it will block the current.
 */
class SyncCtl<F extends LinkFeatures> implements LinkController<F> {
    private final LinkSync<F> linkSync;
    private final Executor executor;

    /**
     * Constructor.
     *
     * @param linkSync object link sync
     */
    SyncCtl(@NonNull LinkSync<F> linkSync) {
        this.linkSync = linkSync;
        this.executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public Link<F> getLink() {
        return linkSync;
    }

    @Override
    public void toggleHardware(@NonNull ActionResult result) {
        executor.execute(() -> {
            if (linkSync.isEnabled())
                result.onResult(linkSync.disable());
            else
                result.onResult(linkSync.enable());
        });
    }

    @Override
    public void toggleDiscovery(@NonNull DiscoveryProperties properties, @NonNull ActionResult result) {
        executor.execute(() -> {
            if (linkSync.isDiscovering(properties.getIdentifier()))
                result.onResult(linkSync.cancelDiscover(properties.getIdentifier()));
            else
                result.onResult(linkSync.discover(properties));
        });
    }

    @Override
    public void toggleVisibility(@NonNull VisibilityProperties properties,
                                 @NonNull ActionResult result) {
        executor.execute(() -> {
            if (linkSync.isVisible(properties.getIdentifier()))
                result.onResult(linkSync.cancelVisible(properties.getIdentifier()));
            else
                result.onResult(linkSync.setVisible(properties));
        });
    }

    @Override
    public void toggleConnection(@NonNull Device device, @NonNull ConnectionProperties properties,
                                 @NonNull ActionResult result) {
        executor.execute(() -> {
            if (linkSync.isConnected(device))
                result.onResult(linkSync.disconnect(device));
            else
                result.onResult(linkSync.connect(device, properties));
        });
    }

    @Override
    public void disableConnection(@NonNull Device device, ActionResult result) {
        result.onResult(linkSync.disconnect(device));
    }

    @Override
    public void toggleAccept(@NonNull ServerProperties properties, @NonNull ActionResult result) {
        executor.execute(() -> {
            if (linkSync.isAccepting(properties.getIdentifier()))
                result.onResult(linkSync.deny(properties.getIdentifier()));
            else
                result.onResult(linkSync.accept(properties));
        });
    }
}
