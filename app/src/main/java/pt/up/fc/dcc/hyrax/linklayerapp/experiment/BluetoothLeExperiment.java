/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package pt.up.fc.dcc.hyrax.linklayerapp.experiment;

import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.bluetooth.BluetoothLeLink;
import org.hyrax.link.bluetooth.types.BluetoothLeServerSettings;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import pt.up.fc.dcc.hyrax.linklayerapp.helpers.LinkHelper;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.BluetoothLeClient;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.BluetoothLeServer;
import pt.up.fc.dcc.hyrax.linklayerapp.socket.SocketComm;

/**
 * Bluetooth le experiment.
 */
public class BluetoothLeExperiment extends Experiment<BluetoothLeLink> {
    @Nullable
    private BluetoothLeClient leClient;

    /**
     * Constructor.
     *
     * @param linkHelper link helper object
     * @param server     <tt>true</tt> if server, <tt>false</tt> otherwise
     */
    public BluetoothLeExperiment(@NonNull LinkHelper<BluetoothLeLink> linkHelper, boolean server) {
        super(linkHelper, server);
        setTransferSize(300000);//300KB
    }

    @Override
    ServerProperties getServerProperties() {
        BluetoothLeServer leServer = new BluetoothLeServer(linkHelper);
        return linkHelper.getLink()
                .getFeatures().newServerBuilder()
                .setSettings(BluetoothLeServerSettings.newBuilder()
                        .setGattService(leServer.getGattService())
                        .setServerCallback(leServer)
                        .build()
                )
                .build();
    }

    @Override
    VisibilityProperties getVisibilityProperties() {
        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_LOW)
                .build();
        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .setIncludeTxPowerLevel(false)
                .addServiceUuid(new ParcelUuid(BluetoothLeServer.SERVICE_ID))
                .build();
        return linkHelper.getLink().getFeatures()
                .newVisibilityBuilder(settings, data).build();
    }

    @Override
    DiscoveryProperties getDiscoveryProperties() {
        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build();
        ArrayList<ScanFilter> filter = new ArrayList<ScanFilter>() {{
            add(new ScanFilter.Builder()
                    .setDeviceName("Nexus 9")
                    .build());
        }};
        return linkHelper.getLink().getFeatures()
                .newDiscoveryBuilder(settings, filter)
                .stopAfterTimeoutExpiration(true)
                .setStopRule(devices -> devices.size() > 0)
                .build();
    }

    @Override
    ConnectionProperties getConnectionProperties(@NonNull Device device) {
        leClient = new BluetoothLeClient(linkHelper);
        return linkHelper.getLink().getFeatures()
                .newConnectionBuilder(device)
                .setSettings(leClient)
                .build();
    }

    @Override
    SocketComm getSocketObject(@NonNull Device device) {
        Objects.requireNonNull(leClient);
        return leClient;
    }

    @Override
    public void finish() {
        if (leClient != null)
            leClient.disconnect();
        if (server) {
            linkHelper.toggleAccept(getServerProperties());
            linkHelper.toggleVisibility(getVisibilityProperties());
        } else {
            Collection<Device> connections = linkHelper.getLink().getConnected();
            for (Device device : connections)
                linkHelper.disableConnection(device);
        }
    }
}
